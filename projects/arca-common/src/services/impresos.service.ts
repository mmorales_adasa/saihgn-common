import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { constants } from '../config/constants';
import { ArcaServiceOptions } from './arca.options.service';
import { AuditoriaService } from './auditoria.service';
@Injectable()
export class ImpresosService {
    private options: ArcaServiceOptions;
    private nameToken = "authToken";
    tipos: any = [
        { id: constants.TIPO_IMPRESO.INFORME_ANALISIS, name: "Informe de análisis", prefix_file: "Boletin_Resultados_" },
        { id: constants.TIPO_IMPRESO.INFORME_ANALISIS_MODIFICADO, name: "Informe de análisis modificado", prefix_file: "Informe_de_Analisis_Modificado_" },
        //{ id: constants.TIPO_IMPRESO.INFORME_LABORATORIO, name: "Informe de laboratorio" },
        //{ id: constants.TIPO_IMPRESO.INFORME_LABORATORIO_MODIFICADO, name: "Informe de laboratorio modificado" }
    ];
    estados: any = [
        { id: constants.ESTADO_IMPRESO.PENDIENTE_VALIDACION, name: "Pendiente de validación", color: "#aacdf3" },
        { id: constants.ESTADO_IMPRESO.VALIDADO_RESP_TECNICO, name: "Validado por Resp.Técnico", color: "yellow" },
        { id: constants.ESTADO_IMPRESO.VALIDADO_RESP_CALIDAD, name: "Validado por Resp.Calidad", color: "orange" },
        { id: constants.ESTADO_IMPRESO.VALIDADO_DIREC_CALIDAD, name: "Validado por Director calidad", color: "#80e998" }
    ];
    constructor(private _http: Http, options : ArcaServiceOptions, public _auditoriaService: AuditoriaService) {
        this.options = options;
    }
    public selectImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/selectImpreso';
        return this.callPost(url, params);
    }
    public getHistorialImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/getHistorialImpreso';
        return this.callPost(url, params);
    }
    public insertarEstado(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/insertarEstado';
        return this.callPost(url, params);
    }
    public updateImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/updateImpreso';
        return this.callPost(url, params);
    }
    public deleteImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/deleteImpreso';
        return this.callPost(url, params);
    }
    public generarImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/insertarImpreso';
        return this.callPost(url, params);
    }
    public reGenerarImpreso(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/regenerarImpreso';
        return this.callPost(url, params);
    }
    public previsualizar(params): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/previsualizar';
        return this.callPostBlob(url, params);
    }
    public download(codImpreso): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresos/download?codImpreso=' + codImpreso;
        return this._http.get(this.getUrlToken(url), this.getHeadersForBLob()).pipe(
            map(res => {
                return res;
            })
        )
    }
    public getHeadersForBLob() {
        const headers = new Headers();
        var ro = new RequestOptions({ headers: headers });
        ro.responseType = ResponseContentType.Blob;
        return ro;
    }
    public getImpresosGenerados(codAnalisis): Observable<any> {
        var url = this.options.laboratorioEndPoint + '/impresosGenerados/' + codAnalisis;
        return this._http.get(url).pipe(
            map(res => res.json())
        );
    }
    public getTiposImpreso(): any {
        return this.tipos;
    }
    public getEstados(): any {
        return this.estados;
    }
    public getEstadoById(id): any {
        var elemento = this.estados.find(item => item.id === id);
        return elemento;
    }
    public getTipoImpreso(id): any {
        var elemento = this.tipos.find(item => item.id === id);
        return elemento;
    }
    public comprobacionImprimible(analisis) {
        return analisis.COMPROBACION_METODO == 0
            && analisis.COMPROBACION_TIPO_VALIDACION == 0
            && analisis.COMPROBACION_VALOR == 0
            && analisis.COMPROBACION_RESULTADOFINAL == 0;
    }
    public comprobacionRegenerar(analisis) {
        var allow=true;
        var position = analisis.LIST_IMPRESOS.findIndex(item => parseInt(item.COD_TIPO_IMPRESOS) === constants.TIPO_IMPRESO.INFORME_ANALISIS);
        if (position >= 0) {
            if (analisis.LIST_IMPRESOS[position].FIRMADO == 1) {
                allow = false;
            }
        }
        return allow;
    }
    public existeTipoInforme(analisis, tipo) {
        var position = analisis.LIST_IMPRESOS.findIndex(item => parseInt(item.COD_TIPO_IMPRESOS) === tipo);
        return position >= 0;
    }    
    public comprobacionGenerarNuevo(analisis) {
        var position = analisis.LIST_IMPRESOS.findIndex(item => parseInt(item.COD_TIPO_IMPRESOS) === constants.TIPO_IMPRESO.INFORME_ANALISIS);
        return position < 0;
    }
    public callPost(url, params): Observable<any> {
        var profile = this._auditoriaService.getUserProfile();
        if (!params) {
            params = {};
        }
        params[this.nameToken] = profile.token;
        return this._http.post(url, params).pipe(
            map(res => { return res.json() })
        )
    }    
    public callPostBlob(url, params): Observable<any> {
        var profile = this._auditoriaService.getUserProfile();
        if (!params) {
            params = {};
        }
        params[this.nameToken] = profile.token;
        return this._http.post(url, params, this.getHeadersForBLob()).pipe(
            map(res => { return res})
        )
    } 
    public getUrlToken(url): any{
        var profile = this._auditoriaService.getUserProfile();
        if (url.includes('?')){
            url += "&" + this.nameToken + "=" + profile.token;
        }else{
            url += "?" + this.nameToken + "=" +  profile.token;
        }  
        return url;
    }
    public callGet(url): Observable<any> {
        this.getUrlToken(url);    
        return this._http.get(url).pipe(
            map(res => { return res.json() })
        )
    }    
}