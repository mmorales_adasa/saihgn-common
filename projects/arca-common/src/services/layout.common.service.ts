import { Injectable, Output, EventEmitter} from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class LayoutCommonService {    
    
    // Search Data

    private readonly searchDataSubjectSource$ = new Subject<object>();

    public get searchDataNewData(): Observable<object> {
        return this.searchDataSubjectSource$.asObservable();
    }

    public searchData(data: any) {
        this.searchDataSubjectSource$.next(data);
    }

    // Search Data for Charts

    private readonly searchDataForChartsSubjectSource$ = new Subject<object>();

    public get searchDataForChartsNewData(): Observable<object> {
        return this.searchDataForChartsSubjectSource$.asObservable();
    }

    public searchDataForCharts(data: any) {
        this.searchDataForChartsSubjectSource$.next(data);
    }

    // Open filter

    private readonly openFilterSubjectSource$ = new Subject<object>();
    private readonly closeFilterSubjectSource$ = new Subject<object>();

    public get openFilterNewData(): Observable<object> {
        return this.openFilterSubjectSource$.asObservable();
    }

    public get closeFilterNewData(): Observable<object> {
        return this.closeFilterSubjectSource$.asObservable();
    }

    public openFilter() {
        this.openFilterSubjectSource$.next({});
    }

    public closeFilter() {
        this.closeFilterSubjectSource$.next({});
    }

}