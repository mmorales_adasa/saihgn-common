import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { constants } from '../config/constants';
import { ArcaServiceOptions } from './arca.options.service';
import { AuditoriaService } from './auditoria.service';
@Injectable()
export class CrudService {
    private baseUrl;
    private nameToken = "authToken";
    constructor(private _http: Http,  public _auditoriaService: AuditoriaService) {        
    }
    public setBaseUrl(url){
        this.baseUrl = url;
    }
    public select(params) {
        var url = this.baseUrl + '/select';
        return this.callPost(url, params);
    }
    public insert(params) {
        var url = this.baseUrl  + '/insert';
        return this.callPost(url, params);
    }
    public update(params) {
        var url = this.baseUrl  + '/update';
        return this.callPost(url, params);
    }
    public delete(params) {
        var url = this.baseUrl + '/delete';
        return this.callPost(url, params);
    }  
    public callPost(url, params): Observable<any> {
        var profile = this._auditoriaService.getUserProfile();
        if (!params) {
            params = {};
        }
        params[this.nameToken] = profile.token;
        return this._http.post(url, params).pipe(
            map(res => { return res != null ? res.json() : null })
        )
    }
}
