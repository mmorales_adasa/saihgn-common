import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { constants } from '../config/constants';
import { ArcaServiceOptions } from './arca.options.service';
export const AuditoriaConstants = {
    LOGIN: 'LOGIN',
    LOGIN_KO: 'LOGIN_KO',
    LOGOUT: 'LOGOUT',
    SEARCH: 'SEARCH',
    INSERT: 'INSERT',
    UPDATE: 'UPDATE',
    DELETE: 'DELETE',
    EXPORT_XLS: 'EXPORT_XLS',
    EXPORT_PDF: 'EXPORT_PDF',
};
@Injectable()
export class AuditoriaService {
    private options: ArcaServiceOptions;
    constructor(private _http: Http, options: ArcaServiceOptions) {
        this.options = options;
    }
    public removeProfile(): any {
        localStorage.removeItem('profile');
    }
    public setUserProfile(profile): any {
        localStorage.profile = JSON.stringify(profile);
    }
    public getUserProfile(): any {
        if (localStorage.profile) {
            return JSON.parse(localStorage.profile);
        }
        return null;
    }
    public login(params): Observable<any> {
        var url = this.options.auditoriaEndPoint + '/security/login';
        return this._http.post(url, params).pipe(
            map(res => { return res.json() })
        )
    }
}
