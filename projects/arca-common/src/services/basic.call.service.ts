import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Http, RequestOptions, Headers } from '@angular/http';
import { AuditoriaService } from './auditoria.service';
import { map, catchError } from 'rxjs/operators';
import { HttpEvent } from '@angular/common/http'
@Injectable()
export class BasicCallService {
    nameToken = "authToken";
    constructor(public _http: Http, public _auditoriaService: AuditoriaService) {}
    public execute(url, params): Observable<any> {
        return this.callPost(url, params);
    }
    public callPost(url, params): Observable<any> {
        var profile = this._auditoriaService.getUserProfile();
        var token = "";
        if (profile && profile.token) {
            token = profile.token;
        }
        if (!params) {
            params = {};
        }
        params[this.nameToken] = token;
        return this._http.post(url, { data: params }, this.getHeaders()).pipe(
            map(res => { return res.json() }),
            catchError(
                (error: any, caught: Observable<HttpEvent<any>>) => {
                    if (error.status === 401) {
                        window.location.href = '/';
                    }
                    throw error;
                }
            )
        );
    }
    private getHeaders() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append("Accept", 'application/json');
        return new RequestOptions({ headers: headers });
    }
}
