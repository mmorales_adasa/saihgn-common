import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
@Injectable()
export class GenericTableCommonService {
    // Search Data
    private readonly loadTableSubjectSource$ = new Subject<object>();
    public get loadTableNewCall(): Observable<object> {
        return this.loadTableSubjectSource$.asObservable();
    }
    public loadTable(data: GenericTableExternalFilter) {
        this.loadTableSubjectSource$.next(data);
    }
    // Generic table component. Desencadenante:
    private readonly afterLoadTableSubjectSource$ = new Subject<object>();
    public get afterLoadTableNewCall(): Observable<object> {
        return this.afterLoadTableSubjectSource$.asObservable();
    }
    public afterLoadTable(data: GenericTableExternalFilter) {
        this.afterLoadTableSubjectSource$.next(data);
    }
    // Generic filter. Search. Desencadenante:
    private readonly searchFromFilterSubjectSource$ = new Subject<object>();
    public get searchFromFilterNewCall(): Observable<object> {
        return this.searchFromFilterSubjectSource$.asObservable();
    }
    public searchFromFilter(data: SaerchFromFilterObject) {
        this.searchFromFilterSubjectSource$.next(data);
    }
    // Generic filter. Open AE form. Desencadenante:
    private readonly openGenericAbeFormSubjectSource$ = new Subject<object>();
    public get openGenericAbeFormNewCall(): Observable<object> {
        return this.openGenericAbeFormSubjectSource$.asObservable();
    }
    public openGenericAbeForm(data: OpenGenericAbeFormObject) {
        this.openGenericAbeFormSubjectSource$.next(data);
    }
    private readonly afterUpdateFromAbeFormSubjectSource$ = new Subject<object>();
    public get afterUpdateFromAbeFormNewCall(): Observable<object> {
        return this.afterUpdateFromAbeFormSubjectSource$.asObservable();
    }
    public afterUpdateFromAbeForm(data: OpenGenericAbeFormObject) {
        this.afterUpdateFromAbeFormSubjectSource$.next(data);
    }
    // On click in table cell. Desencadenante:
    private readonly clickOnTableCellSubjectSource$ = new Subject<object>();
    public get clickOnTableCellNewCall(): Observable<object> {
        return this.clickOnTableCellSubjectSource$.asObservable();
    }
    public clickOnTableCell(data: ClickOnTableCellObject) {
        this.clickOnTableCellSubjectSource$.next(data);
    }
    // Utilidad para cualquier evento que solo necesite un ID
    private readonly simpleEventSubjectSource$ = new Subject<string>();
    public get simpleEventNewCall(): Observable<string> {
        return this.simpleEventSubjectSource$.asObservable();
    }
    public simpleEvent(data: string) {
        this.simpleEventSubjectSource$.next(data);
    }
}
export class GenericTableExternalFilter {
    public parent: string;
    public filter: any;
    public id: string;
    public data: any[];
    constructor(parent: string, filter: any) {
        this.parent = parent;
        this.filter = filter;
    }
}
export class GenericTableEditionObject {
    public tableId: string;
    public pk: any;
    constructor(tableId: string, pk: any) {
        this.tableId = tableId;
        this.pk = pk;
    }
}
export class OpenGenericAbeFormObject {
    public id: string;
    public model: any;
    public mode: string;
    constructor(id, mode: string, model) {
        this.id = id;
        this.model = model;
        this.mode = mode;
    }
}
export class GenericTableNewObject {
    public tableId: string;
    constructor(tableId: string) {
        this.tableId = tableId;
    }
}
export class SaerchFromFilterObject {
    public id: string;
    public filterSelection: any[];
    constructor(id: string, filterSelection: any) {
        this.id = id;
        this.filterSelection = filterSelection;
    }
}
export class ClickOnTableCellObject {
    public id: string;
    public elementCod: string;
    public elementValue: any;
    constructor(id: string, elementCod: string, elementValue: any) {
        this.id = id;
        this.elementCod = elementCod;
        this.elementValue = elementValue;
    }
}
