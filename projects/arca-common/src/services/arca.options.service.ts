export class ArcaServiceOptions {
	public auditoriaEndPoint : string;	
	public laboratorioEndPoint : string;
	public auditaActivated : boolean = false;
}