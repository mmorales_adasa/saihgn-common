import { Injectable } from '@angular/core';
@Injectable()
export class UtilitiesService {
    constructor() {}    
    public translateFlagtoText(value): any {
        if (value===1 || value===true) {
            return "Sí";
        } else {
            return "No"
        }
    }
}