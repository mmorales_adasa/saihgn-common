import  moment from 'moment';
export const DateUtils = {
    translateMonth(label) {
        var month = label.match(/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec/g);    
        if (!month) return label; 
        var translation = month[0];    
        switch(month[0]) {    
            case 'Jan': translation = 'Ene' ; break;
            case 'Feb': translation = 'Feb' ; break;
            case 'Mar': translation = 'Mar' ; break;
            case 'Apr': translation = 'Abr' ; break;
            case 'May': translation = 'May' ; break;
            case 'Jun': translation = 'Jun' ; break;
            case 'Jul': translation = 'Jul' ; break;
            case 'Aug': translation = 'Ago' ; break;
            case 'Sep': translation = 'Sep' ; break;
            case 'Oct': translation = 'Oct' ; break;
            case 'Nov': translation = 'Nov' ; break;
            case 'Dec': translation = 'Dic' ; break;
        }    
        return label.replace(month, translation, 'g');
    },
    stringDate_DD_MM_YYYY_toIntegerDate_YYYYMMDD(s){
        var ss = s.split('/');
        return Number(ss[2] + ss[1] + ss[0]);
    },
    momentToIntegerDate_YYYYMMDD(mFecha : moment.Moment){
        return Number(mFecha.format('YYYYMMDD'));
    },
    integerDate_YYYYMMDD_toStringDate_DD_MM_YYYY(iFecha){
        var sFecha = '' + iFecha;
        var year = sFecha.substr(0, 4);
        var month = sFecha.substr(4, 2);
        var day = sFecha.substr(6, 2);
        return day + '/' + month + '/' + year;
    },
    date_toStringDate_DD_MM_YYYY(date){
        var m = moment(date);
        return m.format('DD/MM/YYYY');
    },
    getAllDatesInRange(iniMoment_, endMoment){
        var result : Date[] = [];
        var iniMoment = moment(iniMoment_.toDate())
        if(iniMoment > endMoment) return result;
        result.push(iniMoment.toDate());
        var newMoment = iniMoment.add('days', 1);
        while(newMoment <= endMoment){
            result.push(newMoment.toDate());
            newMoment = newMoment.add('days', 1);
        }
        return result;
    },
    getAllYearsInRange(iniYear, endYear){
        var result : Date[] = [];
        if(iniYear > endYear) return result;
        result.push(iniYear);
        var newMoment = iniYear +1;
        while(newMoment <= endYear){
            result.push(newMoment);
            newMoment = newMoment +1;
        }
        return result;
    },
    date_toStringDate_MMM_YYYY(date){
        var m = moment(date);
        return m.format('MMM YYYY');
    },    
    getAllMonthsInRange(iniMoment_, endMoment){
        var result : Date[] = [];
        var iniMoment = moment(iniMoment_.toDate())
        if(iniMoment > endMoment) return result;
        result.push(iniMoment.toDate());
        var newMoment = iniMoment.add('months', 1);
        while(newMoment <= endMoment){
            result.push(newMoment.toDate());
            newMoment = newMoment.add('months', 1);
        }
        return result;
    },        
}