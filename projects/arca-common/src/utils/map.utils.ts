import * as ol from 'openlayers';
import { ListUtils } from './list.utils';
import { GenericTableCommonService, ClickOnTableCellObject } from '../services/generic.table.common.service';
export const MapUtils = {
    getMapInstance(id) : any {
         var map = new ol.Map({
            target: id,
            view: new ol.View({
                center: ol.proj.fromLonLat([-0.876566, 41.6563497]), // Zaragoza
                zoom: 6 // 5
            }),
            layers: [
                new ol.layer.Tile({
                    preload: 4,
                    source: new ol.source.OSM(),
                }),
            ],
            loadTilesWhileAnimating: true,
        });  
        return map; 
    },
    addOnClickFunction(map : ol.Map, _genericTableCommonService : GenericTableCommonService, componentId : string){
        map.on("click", function(e) {
            map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                var o = new ClickOnTableCellObject(componentId, 'COD_ESTACION', feature['c']);
                _genericTableCommonService.clickOnTableCell(o);
            })
        });
    },
    resetMap(map : ol.Map){
        var layersToRemove = [];
        var firstLayer = true;
        map.getLayers().forEach(function (layer) {
            if(!firstLayer) {
                layersToRemove.push(layer);
            } else {
                firstLayer = false;
            }
        });
        var len = layersToRemove.length;
        for(var i = 0; i < len; i++) {
            map.removeLayer(layersToRemove[i]);
        }
    },
    addPlacesToMap(map : ol.Map, _places : MapPlaceModel[]){
        var places = [];
        for(var place of _places){
            places.push([place.lon, place.lat, place.icon, place.color, place.name]);
        }
        var vectorSource = new ol.source.Vector({});
        for (var i = 0; i < places.length; i++) {
            var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([Number(places[i][0]), Number(places[i][1])], 'EPSG:4326', 'EPSG:3857')),
            });
            iconFeature.setId(places[i][4]);
            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [0.5, 0.5],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    src: '' + places[i][2],
                    color: '' + places[i][3],
                    crossOrigin: 'anonymous',   
                }),
                text: new ol.style.Text({
                    font: 'bold 14px Calibri,sans-serif',
                    fill: new ol.style.Fill({ color: 'blue' }),
                    // get the text from the feature - `this` is ol.Feature
                    // and show only under certain resolution
                    //text: places[i][0],
                    text: places[i][4],
                    textAlign: 'center',
                    offsetY: -22,
                  })
            });
            iconStyle.getImage()
            iconFeature.setStyle(iconStyle);
            vectorSource.addFeature(iconFeature);
        }
        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            updateWhileAnimating: true,
            updateWhileInteracting: true,
        });
        map.removeLayer(vectorLayer);
        map.getLayers().push(vectorLayer);
        // Centramos mapa
        if (vectorLayer.getSource().getFeatures().length > 1) {
            map.getView().fit(vectorSource.getExtent());
            map.getView().setZoom(map.getView().getZoom() + 2); // Aumentamos un poco de zoom si hay varias estaciones
        } else {
            // si solo hay una estación, ponemos poco zoom;
            map.getView().fit(vectorSource.getExtent());
            map.getView().setZoom(16);
        }
    },
    dataArrayToMapPlaceModelArray(data : any[], codAttr, lonAttr, latAttr){
        var places : MapPlaceModel[] = [];
        var placeKeyList = ListUtils.getMasterComboFromObjectList(data, codAttr).keyList;
        for(var placeKey of placeKeyList){
            var place = data.filter((m : any) => m[codAttr] == placeKey)[0];
            places.push(new MapPlaceModel(placeKey, place[lonAttr], place[latAttr]));
        }
        return places;
    },
}

export class MapPlaceModel {
    name : string;
    lon : number;
    lat: number;
    icon: string;
    color: string;
    constructor(name, lon, lat){
        //this.name = name.split(' ')[0];
        this.name = name;
        this.lon = lon;
        this.lat = lat;
        this.icon = 'http://maps.google.com/mapfiles/ms/micons/blue.png';
        this.color = 'blue';
    }
}