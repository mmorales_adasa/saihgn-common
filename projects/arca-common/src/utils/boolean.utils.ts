export const BooleanUtils = {
    isTrue(value){
        return (value === true) || (value === '1') || (value === 1) 
    },
}