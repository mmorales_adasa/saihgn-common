export const ListUtils = {
    DEFAULT_TOTAL_LABEL: 'Total',
    getMasterComboFromObjectList(data: any[], attribute: string): any {
        var keyList = [];
        var multiselectList = [];
        if (data) {
            data.forEach(function (element) {
                if (element && !keyList.includes(element[attribute])) {
                    if (typeof element == 'string') {
                        keyList.push(element);
                        multiselectList.push({ cod: element, desc: element });
                    } else {
                        keyList.push(element[attribute]);
                        var descElement = '' + element[attribute];
                        // CASOS ESPECIALES
                        if (attribute == 'DESC_MES') descElement = descElement.split(',')[1];
                        var multiselectObject = { cod: element[attribute], desc: descElement };
                        multiselectList.push(multiselectObject);
                    }
                }
            });
        }
        // Sort
        multiselectList = multiselectList.sort((a, b) => (a.cod > b.cod) ? 1 : ((b.cod > a.cod) ? -1 : 0));
        return { keyList: keyList, multiselectList: multiselectList };
    },
    getMasterComboFromObjectListWithDesc(data: any[], codAttr: string, descAttr: string): any {
        var keyList = []; 
        var multiselectList = [];
        if (data) {
            data.forEach(function (element) {
                if (!keyList.includes(element[codAttr])) {
                    keyList.push(element[codAttr]);
                    var multiselectObject = { cod: element[codAttr], desc: element[descAttr] };
                    multiselectList.push(multiselectObject);
                }
            });
        }
        return { keyList: keyList, multiselectList: multiselectList };
    },
    getMasterComboFromObjectListWithFields(data: any[], codAttr: string, morefields: string[]) {
        var result = [];
        var keyList = []; if (data) {
            data.forEach(function (element) {
                if (!keyList.includes(element[codAttr])) {
                    var o = {};
                    o[codAttr] = element[codAttr];
                    for (var field of morefields) o[field] = element[field];
                    keyList.push(element[codAttr]);
                    result.push(o);
                }
            });
        }
        return result;
    },
    filterObjectList(data: any[], attribute: string, values: string[]): any[] {
        var result = [];
        if (values.length == 0) return data;
        if (data) {
            result = data.filter((m: any) => m && values.includes('' + m[attribute]));
        }
        return result;
    },
    filterObjectListLike(data: any[], attribute: string, value: string): any[] {
        var result = data;
        if (value != '') {
            result = data.filter((m: any) => ('' + m[attribute]).toLowerCase().indexOf(value.toLowerCase()) != -1);
        }
        return result;
    },
    getNumberOfResults(data: any): number {
        return data.filter((m: any) => !m['SUBTOTAL']).length;
    },
    groupObjectList(data: any[], displayedAttributes: string[], displayedMetrics: string[]): any[] {
        var result = [];
        var resultKeys = [];
        data.forEach(function (element) {
            // obtenemos key
            var attrKey = ListUtils.generateElementKey(element, displayedAttributes);
            // Comprbamos si ya hemos añadido la fila. Si no está­, la añadimos
            var index = resultKeys.length;
            if (!resultKeys.includes(attrKey)) {
                resultKeys.push(attrKey);
                // clonamos y añadimo
                var resultElement = {};
                for (var key of displayedAttributes) resultElement[key] = element[key];
                result.push(resultElement);
            } else {
                index = resultKeys.indexOf(attrKey);
            }
            // Agrupamos para cada métrica
            for (var metric of displayedMetrics) {
                var currentValue = result[index][metric];
                currentValue = (currentValue ? currentValue : 0);
                result[index][metric] = currentValue + element[metric];
            }
        });
        return result;
    },
    sortObjectList(data: any[], sortElement: string, direction: string) {
        return data.sort((a, b) => {
            const isAsc = direction === 'asc';
            var aa = a[sortElement];
            var bb = b[sortElement];
            return ListUtils.compare(aa, bb, isAsc);
        });
    },
    sortStringList(data: string[], direction: string) {
        return data.sort((a, b) => {
            const isAsc = direction === 'asc';
            return ListUtils.compare(a, b, isAsc);
        });
    },
    sortNumberList(data: number[], direction: string) {
        return data.sort((a, b) => {
            const isAsc = direction === 'asc';
            return ListUtils.compare(a, b, isAsc);
        });
    },
    compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    },
    addSubtotalToObjectList(data: any[], groupByElement: string, lastDisplayedAttribute: string,
        displayedMetrics: string[], subtotalType: string) {
        // Si hemos ordenado por el indicador, no añadimos subtotal, no tiene sentido
        if (displayedMetrics.includes(groupByElement)) return data;
        var result = [];
        var lastKey = '';
        var lastElement = {};
        var lastValues = [];
        var lastValueContador = 0;
        // inicializamos array de acumulados
        for (var i in displayedMetrics) lastValues[i] = 0;
        data.forEach(function (element) {
            var key = element[groupByElement];
            // Comprobamos si hay que insertar subtotal           
            var object = ListUtils.addSubtotalToObjectList_createObject(element, lastKey, lastValues, lastValueContador, groupByElement, displayedMetrics, lastDisplayedAttribute, subtotalType);
            if (lastKey && (key != lastKey)) {
                result.push(object);
                for (var i in displayedMetrics) lastValues[i] = 0;
                lastValueContador = 0;
            }
            // Actualizamos lastKey
            lastKey = key;
            lastElement = element;
            // Actualizamos agregado
            if (!element['SUBTOTAL']) {
                if (displayedMetrics.length > 0) {
                    for (var i in displayedMetrics) {
                        var value = element[displayedMetrics[i]];
                        var value = (value ? value : 0);
                        lastValues[i] += Number(value); // Sumador
                    }
                }
                else lastValueContador++; // Contador
            }
            // Añadimos a resultado
            result.push(element);
        });
        // Añadimos al final 
        var object = ListUtils.addSubtotalToObjectList_createObject(lastElement, lastKey, lastValues, lastValueContador, groupByElement, displayedMetrics, lastDisplayedAttribute, subtotalType);
        if (data.length > 0) result.push(object);
        return result;
    },
    addSubtotalToObjectList_createObject(elementToCopy, lastKey, lastValues, lastValueContador, groupByElement,
        valueElements, lastColumn, subtotalType) {
        var object = {};
        for (var attr in elementToCopy) object[attr] = '';
        object[groupByElement] = lastKey;
        object[subtotalType] = true;
        object['SUBTOTAL'] = true;
        if (valueElements.length > 0) for (var i in valueElements) object[valueElements[i]] = Number(lastValues[i].toFixed(3));
        else object[lastColumn] = lastValueContador;
        return object;
    },
    addTotalToObjectList(data: any[], displayedMetrics: string[], groupByElement: string, lastDisplayedAttribute: string, subtotalType: string) {
        var object = {};
        for (var attr in data[0]) object[attr] = '';
        object[groupByElement] = ListUtils.DEFAULT_TOTAL_LABEL;
        object[subtotalType] = true;
        object['SUBTOTAL'] = true;
        if (displayedMetrics.length > 0) {
            // Suma
            for (var i in displayedMetrics) {
                var valueElement = displayedMetrics[i];
                var total = 0;
                total = data.map(t => ((t[valueElement] && !t.SUBTOTAL) ? t[valueElement] : 0)).reduce((acc, value) => acc + Number(value), 0);
                object[valueElement] = Number(total.toFixed(3));
            }
        } else {
            // Contador
            var total = data.filter(t => !t['SUBTOTAL']).length;
            object[lastDisplayedAttribute] = total;
        }
        var result = data;
        result.push(object);
        return result;
    },
    getStringOfDescs(data: any[], elements: string[], codAttr: string, descAttr: string): string {
        var result = '';
        if (elements) {
            for (var cod of elements) {
                if (result != '') result += ', ';
                result += data.filter((m: any) => m[codAttr] == cod)[0][descAttr];
            }
        }
        return result;
    },
    generateElementKey(element: any, attrs: string[]): string {
        var result: any[] = [];
        for (var attr of attrs) {
            result.push(element[attr]);
        }
        return result.toString();
    },
    possibleObjectListToKeyList(olist: any[], key: string) {
        var result: string[] = [];
        for (var element of olist) {
            if (typeof element == 'object') result.push(element[key]);
            else result.push(element);
        }
        return result;
    },
    convertToListForArray(list, fieldCod, fielDesc) {
        var result = [];
        for(var item of list){
            result.push({COD : item[fieldCod] , DESCR : item[fielDesc]})
        }
        return result;
    }        
}