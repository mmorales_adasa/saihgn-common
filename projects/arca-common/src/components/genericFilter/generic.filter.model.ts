import { MultiselectModel } from "../multiselect/multiselect.model";
import { MultiselectModelMV } from "../multiselect/multiselectMV.model";
import { ListUtils } from "../../utils/list.utils";
import { asLiteral } from "@angular/compiler/src/render3/view/util";
export class GenericFilter {
    id: string;
    COD_PERMISO: string;
    rowArray: string[] = [];
    rowElementMap: any;
    elementMap: any;
    considerEmptyError = true;
    maxElementsInDesc = 3;
    searchValues = {};
    initClosed = false;
    initSearch = false;
    insertable = false;
    dateError = false;
    emptyError = false;
    errorMessage = '';
    dateValidation: boolean = false;
    dateValidation_iniDateId: string;
    dateValidation_endDateId: string;
    dateValidation_elements: string[] = [];
    dateInvalidationIsActive: boolean = false;
    constructor(id: string, COD_PERMISO) {
        this.id = id;
        this.COD_PERMISO = COD_PERMISO;
        this.rowElementMap = {};
        this.elementMap = {};
    }
    addDateValidation(dateValidation_iniDateId: string, dateValidation_endDateId: string, dateValidation_elements: string[]) {
        this.dateValidation = true;
        this.dateValidation_iniDateId = dateValidation_iniDateId;
        this.dateValidation_endDateId = dateValidation_endDateId;
        this.dateValidation_elements = dateValidation_elements;
    }
    addFieldToRowFilterMap(row: number, id: string) {
        var srow = '' + row;
        if (!this.rowArray.includes(srow)) this.rowArray.push(srow);
        if (!this.rowElementMap.hasOwnProperty(srow)) this.rowElementMap[srow] = [];
        if (!this.rowElementMap[srow].includes(id)) this.rowElementMap[srow].push(id);
    }
    addTextField(row: number, id: string, label: string, bsWidth: number, minLength: number = 100) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericElementFilter(id, label, bsWidth, 'TX', null, null, null, false);
        filter.minLength = minLength;
        this.elementMap[id] = filter;
    }
    addDateField(row: number, id: string, label: string, bsWidth: number, minLength: number = 100) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericElementFilter(id, label, bsWidth, 'DT', null, null, null, false);
        filter.minLength = minLength;
        this.elementMap[id] = filter;
    }
    addSelectField(row: number, id: string, label: string, bsWidth: number, data: any[], defaultOption: boolean, returnValueAsMultiselect: boolean, minLength: number = 100) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericElementFilter(id, label, bsWidth, 'SS', data, defaultOption, null, returnValueAsMultiselect);
        filter.minLength = minLength;
        this.elementMap[id] = filter;
    }
    addMultiselectField(row: number, id: string, label: string, bsWidth: number, data: any[], searchOption: boolean, minLength: number = 100) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericElementFilter(id, label, bsWidth, 'MS', data, null, searchOption, true);
        filter.minLength = minLength;
        this.elementMap[id] = filter;       
    }
    addMultiselectFieldVirtualScroll(row: number, id: string, label: string, bsWidth: number, fun: any, searchOption: boolean, minLength: number = 100, disabled : boolean= false) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericElementFilter(id, label, bsWidth, 'MSV', null, null, searchOption, true);
        filter.minLength = minLength;
        filter.functionPagination = fun;
        filter.disabled = disabled;
        this.elementMap[id] = filter;
        return filter;
    }
    addAfeccion(elementId, superElementId, superCod) {
        this.elementMap[elementId].afeccionIdArray.push(superElementId);
        this.elementMap[elementId].afeccionSupercodArray.push(superCod);
        this.elementMap[superElementId].afeccionDependences.push(elementId);

        // Actualizamos lista
        if (this.elementMap[elementId].type != 'MSV') {
            this.updateCombo(elementId, superElementId);
        }
    }
    setValue(elementId, value) {
        this.elementMap[elementId].selectedValue = value;
    }
    getSearchValue(elementId) {
        return this.searchValues[elementId];
    }
    // On Change
    change(elementId) {
        // Comprobamos si afecta a Date Invalidation
        if (this.dateValidation_elements.includes(elementId)) {
            if (this.elementMap[elementId].selectedValue) {
                this.elementMap[this.dateValidation_iniDateId].disabled = true;
                this.elementMap[this.dateValidation_endDateId].disabled = true;
                this.dateInvalidationIsActive = true;
            } else {
                this.elementMap[this.dateValidation_iniDateId].disabled = false;
                this.elementMap[this.dateValidation_endDateId].disabled = false;
                this.dateInvalidationIsActive = false;
            }
        }
        // Comprobamos si hay que haer validación de fechas
        this.dateError = false;
        this.errorMessage = '';
        if (!this.dateInvalidationIsActive && this.dateValidation_elements.concat([this.dateValidation_iniDateId, this.dateValidation_endDateId]).includes(elementId)) {
            if (!this.elementMap[this.dateValidation_iniDateId].selectedValue) this.dateError = false;
            else if (!this.elementMap[this.dateValidation_endDateId].selectedValue) this.dateError = false;
            else this.dateError = this.elementMap[this.dateValidation_endDateId].selectedValue < this.elementMap[this.dateValidation_iniDateId].selectedValue;
            if (this.dateError) {
                this.errorMessage = 'La fecha inicial no puede ser posterior a la fecha final';
            }
        }
        // Comprobamos si afecta a algún otro filtro
        if (this.elementMap[elementId].type != "MSV") {
            for (var elementId_ in this.elementMap) {
                this.updateCombo(elementId_, elementId);
            }
        }
        if (this.elementMap[elementId].type == "MSV") {
            this.elementMap[elementId].msModel.setElements(this.elementMap[elementId].selectedValue); 
            this.updateComboMSV(elementId);
        }
    }
    updateComboMSV(elementId) {
        var list = [];
        list.push({
            field: elementId,
            selectedValues: this.elementMap[elementId].msModel.getSelectedElements()
        });
        for (var item of this.elementMap[elementId].afeccionDependences) {            
            // Otras affeciones 
            var others = this.elementMap[item].afeccionSupercodArray;
            for (var other of others) {
                if (other != elementId){
                    list.push({
                        field : other,
                        selectedValues : this.elementMap[other].msModel.getSelectedElements()
                    });
                }
            }
            this.elementMap[item].msModel.reload(this.elementMap[item], list);
        }
    }
    updateCombo(elementId, superElementId) {
        if (this.elementMap[elementId].afeccionIdArray.includes(superElementId)) {
            var superValues = this.elementMap[superElementId].getValueArray();
            var index = this.elementMap[elementId].afeccionIdArray.indexOf(superElementId);
            var superCod = this.elementMap[elementId].afeccionSupercodArray[index];
            // SELECT
            if (this.elementMap[elementId].type == 'SS') {
                var ssData = [];
                if (superValues && (superValues.length == 0)) {
                    ssData = this.elementMap[elementId].superdata;
                } else if (superValues) {
                    ssData = this.elementMap[elementId].superdata.filter(m => superValues.includes(m[superCod]));
                }
                // Evitamos duplicados
                ssData = ListUtils.getMasterComboFromObjectListWithFields(ssData, 'COD', ['DESCR']);
                this.elementMap[elementId].data = ssData;
                // Opción por defecto;
                if (this.elementMap[elementId].data.length > 0) {
                    this.elementMap[elementId].selectedValue = this.elementMap[elementId].data[0].COD;
                }
            }
            // MULTISELECT
            if (this.elementMap[elementId].type == 'MS') {
                var msData = [];
                if (superValues && (superValues.length == 0))
                    msData = this.elementMap[elementId].superdata;
                else {
                    msData = this.elementMap[elementId].superdata.filter(m => superValues.includes(m[superCod]));
                }
                // Evitamos duplicados
                msData = ListUtils.getMasterComboFromObjectListWithFields(msData, 'COD', ['DESCR']);
                this.elementMap[elementId].msModel.setElements(msData);
            }
        }
    }
    // Clean
    clean() {
        for (var elementId in this.elementMap) {
            this.elementMap[elementId].clean();
        }
        if (this.dateValidation) {
            this.dateInvalidationIsActive = false;
            this.elementMap[this.dateValidation_iniDateId].disabled = false;
            this.elementMap[this.dateValidation_endDateId].disabled = false;
        }
    }
    // Search data
    search(): any {
        var resultSuperObject = {};
        var resultObject = {};
        var summaryIds = [];
        var summary = {};
        this.emptyError = false;
        this.searchValues = {};
        if (!this.dateError) {
            for (var elementId in this.elementMap) {
                if (this.considerElement(elementId)) {
                    var value = this.elementMap[elementId].getValue();
                    this.searchValues[elementId] = value;
                    if (value) {
                        resultObject[elementId] = value;
                        summaryIds.push(elementId);
                        summary[elementId] = this.elementMap[elementId].getSummary(this.maxElementsInDesc);
                    }
                }
            }
            if (this.considerEmptyError && (Object.keys(resultObject).length == 0)) {
                this.emptyError = true;
                this.errorMessage = 'Es necesario seleccionar al menos un filtro';
            } else {
                resultSuperObject['SUMMARY_ID_ARRAY'] = summaryIds;
                resultSuperObject['SUMMARY_MAP'] = summary;
            }
        }
        resultSuperObject['filter'] = resultObject;
        return resultSuperObject;
    }
    considerElement(elementId) {
        if (!this.dateValidation) return true;
        else if (!this.dateInvalidationIsActive) return true;
        else if ([this.dateValidation_iniDateId, this.dateValidation_endDateId].includes(elementId)) return false;
        else return true;
    }
}
export class GenericElementFilter {
    TYPE_SS = 'SS'; // Select
    TYPE_MS = 'MS'; // Multiselect
    TYPE_MSV = 'MSV'; // Multiselect Virt Scrol
    TYPE_DT = 'DT'; // Date
    TYPE_TX = 'TX'; // Texto
    id: string;
    label: string;
    bsWidth: number;
    minLength: number;
    functionPagination: any;
    type: string;
    superdata: any[];
    data: any[];
    defaultOption: boolean;
    returnValueAsMultiselect = false;
    msModel = null;
    selectedValue: any;
    disabled = false;
    dirty = false;
    afeccionIdArray = [];
    afeccionSupercodArray = [];
    afeccionDependences = [];

    constructor(id, label, bsWidth, type, superdata, defaultOption, searchOption, returnValueAsMultiselect) {
        this.id = id;
        this.label = label;
        this.bsWidth = bsWidth;
        this.type = type;
        this.defaultOption = defaultOption;
        this.superdata = superdata;
        this.data = ListUtils.getMasterComboFromObjectListWithFields(superdata, 'COD', ['DESCR']);
        this.returnValueAsMultiselect = returnValueAsMultiselect;
        if (this.type == this.TYPE_MS) {
            this.msModel = new MultiselectModel('COD', 'DESCR', searchOption);
            
            this.msModel.setElements(this.data);
        }
        if (this.type == this.TYPE_MSV) {
            this.msModel = new MultiselectModelMV('COD', 'DESCR', searchOption);
        }
        if (this.type == this.TYPE_SS) {
            if (defaultOption) this.selectedValue = null;
            else this.selectedValue = this.superdata[0] ? this.superdata[0].COD : null;
        }
    }
    getValue(): any {
        if (this.type == this.TYPE_SS) {
            if (!this.returnValueAsMultiselect || !this.selectedValue) return this.selectedValue;
            else return [this.selectedValue];
        }
        if (this.type == this.TYPE_TX) return this.selectedValue;
        if (this.type == this.TYPE_DT) {
            if (!this.selectedValue) return null;
            var date = this.selectedValue.toDate();
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            day = (day < 10) ? '0' + day : '' + day;
            month = (month < 10) ? '0' + month : '' + month;
            return '' + day + '/' + month + '/' + year;
        }
        if (this.type == this.TYPE_MS || this.type == this.TYPE_MSV) {
            var value = this.msModel.getSelectedElements();
            if (!value || (value.length == 0)) value = null;
            return value;
        }
    }
    getValueArray(): any[] {
        if (this.type == this.TYPE_SS) {
            var value = this.getValue();
            return value ? [value] : [];
        }
        if (this.type == this.TYPE_MS || this.type == this.TYPE_MSV) return this.msModel.getSelectedElements();
    }
    getSummary(maxElementsInDesc) {
        var summary = '';
        var value = this.getValue();
        if (value) {
            if (this.type == this.TYPE_TX) summary += value;
            if (this.type == this.TYPE_SS) summary += this.superdata.filter(m => m.COD == value)[0].DESCR;
            if (this.type == this.TYPE_DT) summary += value;
            if (this.type == this.TYPE_MS || this.type == this.TYPE_MSV) {
                var s = this.msModel.getSelectedSummary();
                var ss = s.split(', ');
                if (ss.length <= maxElementsInDesc) summary += s;
                else {
                    for (var i = 0; i < maxElementsInDesc; i++) {
                        if (i != 0) summary += ', ';
                        summary += ss[i];
                    }
                    var restantes = ss.length - maxElementsInDesc;
                    summary += ' (+' + restantes + ')';
                }
            }
        }
        return new GenericElementFilterSummary(this.id, this.label, summary);
    }
    clean() {
        if (this.type == this.TYPE_MS) {
            this.msModel.cleanSelection();
        }
        else if (this.type == this.TYPE_MSV) {
            this.selectedValue = [];
            this.msModel.selectedElements = [];
        }
        else this.selectedValue = null;
    }
}
export class GenericElementFilterSummary {
    id: string;
    label: string;
    value: string;
    constructor(id: string, label: string, value: string) {
        this.id = id;
        this.label = label;
        this.value = value;
    }
}
