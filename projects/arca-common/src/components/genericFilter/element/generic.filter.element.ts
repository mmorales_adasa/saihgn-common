import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { GenericTableCommonService, SaerchFromFilterObject, OpenGenericAbeFormObject } from '../../../services/generic.table.common.service';
import { GenericElementFilter, GenericFilter } from './../generic.filter.model';
//import { AuditoriaConstants, AuditoriaService } from '../../../services/auditoria.service';
import { AuditoriaService }   from  './../../../services/auditoria.service';
import { AuditoriaConstants } from  './../../../services/auditoria.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { find } from 'rxjs/operators';
@Component({
    selector: 'app-generic-filter-element',
    templateUrl: './generic.filter.element.html',
    styleUrls: ['./generic.filter.element.scss', './../../../styles/angular2.multiselect.drowpdown.default.theme.css'],
    providers: [AuditoriaService,]
}) 
export class GenericFilterElement implements OnInit {
    @Input() filter: GenericFilter;
    @Input() elementId: string;
    @Output() onChangeFilter = new EventEmitter<any>();
    filterOpened = true;
    summaryIds = [];
    summary = {};
    defaultSearchSusbscription: Subscription;
    itemList = [];
    loading = false;
    indices: any;
    constructor(
        private _genericTableCommonService: GenericTableCommonService,
        private _auditoriaService: AuditoriaService,
        
    ) { 

        
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        if (this.defaultSearchSusbscription) {
            this.defaultSearchSusbscription.unsubscribe();
        }
    }
    toggleFilter() {
        this.filterOpened = !this.filterOpened;
    }
    search() {
        var filterSelection = this.filter.search();
        if (!this.filter.dateError && !this.filter.emptyError) {
            this.summaryIds = filterSelection['SUMMARY_ID_ARRAY'];
            
            this.summary = filterSelection['SUMMARY_MAP'];
            this._genericTableCommonService.searchFromFilter(new SaerchFromFilterObject(this.filter.id, filterSelection['filter']));
            this.filterOpened = false;
        }
    }
    openAddForm() {
        this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.filter.id, 'I', {}));
    }
    getWidthInput(element) {
        //return element.minLength + "px";
        return "100%";
    }
    getStyleFilterApply() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    getStyleFilterClean() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    applyFilter() {
        this.search();
        for (var row of this.filter.rowArray) {
            for (var item of this.filter.rowElementMap[row]) {
                this.filter.elementMap[item].dirty = false;
            }
        }
    }
    removeFilter() {
        this.filter.elementMap[this.elementId].dirty = true;
    }
    setDirty() {
        this.filter.elementMap[this.elementId].dirty = true;
        this.onChangeFilter.emit({
            element : this.elementId
        });          

    }
    getClassDirty() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return "filter-dirty";
        }
        else {
            return "";
        }
    }
} 
