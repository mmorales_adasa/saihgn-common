import { Component, OnInit, Input, } from '@angular/core';
import { GenericTableCommonService, SaerchFromFilterObject, OpenGenericAbeFormObject } from '../../services/generic.table.common.service';
import { GenericElementFilter, GenericFilter } from './generic.filter.model';
//import { AuditoriaConstants, AuditoriaService } from '../../services/auditoria.service';

import { AuditoriaService } from './../../services/auditoria.service';
import { AuditoriaConstants } from './../../services/auditoria.service';

import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { find } from 'rxjs/operators';
@Component({
    selector: 'app-generic-filter',
    templateUrl: './generic.filter.component.html',
    styleUrls: ['./generic.filter.component.scss', './../../styles/angular2.multiselect.drowpdown.default.theme.css'],
    providers: []
})
export class GenericFilterComponent implements OnInit {
    @Input() filter: GenericFilter;
    @Input() options: any = {
        sections: []
    };

    filterOpened = true;
    summaryIds = [];
    summary = {};
    defaultSearchSusbscription: Subscription;
    itemList = [];
    loading = false;
    indices: any;
    constructor(
        private _genericTableCommonService: GenericTableCommonService,
        private _auditoriaService: AuditoriaService,
        private router: Router
    ) { }
    ngOnInit() {
        if (this.filter.initClosed) this.filterOpened = false;
        if (this.filter.initSearch === true) {
            setTimeout(() => this.search(), 100);
        }
    }
    ngOnDestroy() {
        if (this.defaultSearchSusbscription) {
            this.defaultSearchSusbscription.unsubscribe();
        }
    }
    toggleFilter() {
        this.filterOpened = !this.filterOpened;
    }
    clear() {
        this.filter.clean();
        /*for (var row of this.filter.rowArray){
            for (var item of this.filter.rowElementMap[row]){
                this.filter.elementMap[item].dirty = false;
            }               
        } */
    }
    expandClosePanel(section) {
        section.open = !section.open;
    }
    search() {
        var filterSelection = this.filter.search();
        if (!this.filter.dateError && !this.filter.emptyError) {
            for (var row of this.filter.rowArray) {
                for (var item of this.filter.rowElementMap[row]) {
                    this.filter.elementMap[item].dirty = false;
                }
            }
            this.summaryIds = filterSelection['SUMMARY_ID_ARRAY'];
            this.summary = filterSelection['SUMMARY_MAP'];
            this._genericTableCommonService.searchFromFilter(new SaerchFromFilterObject(this.filter.id, filterSelection['filter']));
            this.filterOpened = false;
        }
    }
    openAddForm() {
        this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.filter.id, 'I', {}));
    }
    getWidthInput(elementId) {
        return this.filter.elementMap[elementId].minLength + "px";

    }/*
    getwidthPanel() {
        return 12 / this.options.sections.length;
    }*/
    getClassDirty(elementId) {
        if (this.filter.elementMap[elementId].dirty) {
            return "filter-dirty";
        }
        else {
            return "";
        }
    }

    onChangeFilter(event){
      //  alert(event); 
    }
} 
