import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, HostListener, ElementRef, } from '@angular/core';
import { GenericTableCommonService, OpenGenericAbeFormObject } from '../../services/generic.table.common.service';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material';
import { BasicCrudModel } from './basic.crud.model';
import { AuditoriaService } from './../../services/auditoria.service';
import { CrudService } from './../../services/crud.service';
import { BasicCallService } from './../../services/basic.call.service';
import { constants as conmmonConstants } from '../../config/constants';



@Component({
    selector: 'app-basic-crud',
    templateUrl: './basic.crud.component.html',
    styleUrls: ['./basic.crud.component.scss'],
    providers: [CrudService, BasicCallService]
})
export class BasicCrudComponent implements OnInit {
    @Input() options: BasicCrudModel;
    @Output() onExecuteAction = new EventEmitter<any>();    
    @Output() onchangeValue = new EventEmitter<any>();    
    boxOpened = false;
    updateError = '';
    mode = '';
    method = '';
    snackBarMessage = '';
    accion = '';
    model: any;
    loading = false;
    asleep = true;


    
    constructor(
        public _basicCallService: BasicCallService,
        public dialog: MatDialog,

    ) { }
    ngOnInit() {
        this.mode = this.options.mode;
        this.model = this.options.rowSelected
        for (var elementId in this.options.elementMap) {
            this.options.elementMap[elementId].validationError = '';
            this.options.elementMap[elementId].setValue((['E', 'D'].includes(this.mode)) ? this.model[elementId] : this.options.elementMap[elementId].defaultValue);

            if (this.mode=='E' && this.options.elementMap[elementId].type ==  conmmonConstants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT){
                
                
                    this.options.elementMap[elementId].value = [{ "COD": this.model[this.options.elementMap[elementId].field_code], "DESCR": this.model[this.options.elementMap[elementId].field_desc] }];
                    
                
            
            }
        }

        


    }/*
    loadOptions() {
        if (this.mode == 'I') this.method = 'insert';
        if (this.mode == 'E') this.method = 'update';
        if (this.mode == 'D') this.method = 'delete';
        if (this.mode == 'I') this.snackBarMessage = 'Registro añadido de forma correcta';
        if (this.mode == 'E') this.snackBarMessage = 'Registro actualizado de forma correcta';
        if (this.mode == 'D') this.snackBarMessage = 'Registro eliminado de forma correcta';
        if (this.mode == 'I') this.accion = AuditoriaConstants.INSERT;
        if (this.mode == 'E') this.accion = AuditoriaConstants.UPDATE;
        if (this.mode == 'D') this.accion = AuditoriaConstants.DELETE;
    }*/
    getDisabled(elementId) {
        return ((this.mode == 'I') && (this.options.elementMap[elementId].insertDisabled || this.options.elementMap[elementId].sequence))
            || ((this.mode == 'E') && this.options.elementMap[elementId].pk)
            || this.options.elementMap[elementId].disabled;
            ;
    }
    cancel() {
        this.onExecuteAction.emit({
            action: "cancel"
        });
    }
    changeSingleSelect(event, elementId) {
        let text = event.target.options[event.target.options.selectedIndex].text;
        var field_in_table = this.options.elementMap[elementId].field_in_table;
        if (this.options.elementMap[field_in_table]) {
        }
        else {
            this.options.elementMap[field_in_table] = {};
        }
        this.options.elementMap[field_in_table].typeData = "TEXT";
        this.options.elementMap[field_in_table].value = text;
        this.options.elementMap[field_in_table].virtual = true;
        
        this.onchangeValue.emit({
            elementId : elementId,
            event: event
        });        

        
    }
    save() {
        this.updateError = '';

        var validationErrors = (this.mode == 'D') ? false : this.options.getValidationErrors();
        if (!validationErrors) {
            this.loading = true;
            var data = this.options.getModel()

            var pkField = this.options.getPrimaryKey();            

            var tmp = Object.assign({}, this.options.serviceParams);
            tmp["model"] = data;
            tmp.mode = this.mode;
            
            //this.mode = 'E';
            var tmp2 = {};
            for (var key in data) {
                tmp2[key] = data[key]["value"];
            }
            
            if (this.options.withPersistence) {
                this._basicCallService.execute(this.options.service, tmp).subscribe((result: any[]) => {
                    if (this.mode === conmmonConstants.BASIC_CRUD_MODE.INSERT){
                        tmp2[pkField] = result; 
                    }
                    this.onExecuteAction.emit({
                        action: "save",
                        data: tmp2
                    });
                });
            } else {
                this.onExecuteAction.emit({
                    action: "save",
                    data: tmp2
                });
            }
        }
    }
    openAssistant(id){
        this.onExecuteAction.emit({
            action: "openAssistant",
            data: id
        });


    }
}