import moment from 'moment';
import { MultiselectModel } from '../multiselect/multiselect.model';
import { ListUtils } from "../../utils/list.utils";
import { constants as conmmonConstants } from '../../config/constants';
//import { constants } from 'os';
export class BasicCrudModel {
    title: string;
    rowArray: string[] = [];
    rowElementMap: any;
    elementMap: any = {};
    mode: any;
    hiddenFields: any = {};
    service: any;
    serviceParams: any;
    withPersistence: boolean = false;
    rowSelected;
    dateValidation: boolean = false;
    dateValidation_iniDateId: string;
    dateValidation_endDateId: string;
    dateValidation_elements: string[] = [];
    dateInvalidationIsActive: boolean = false;
    constructor(conf: any) {
        this.title = conf.title;
        this.service = conf.service;
        this.serviceParams = conf.serviceParams;
        this.rowElementMap = {};
        this.withPersistence = conf.withPersistence;
    }
    addFieldToRowFilterMap(row: number, id: string) {
        var srow = '' + row;
        if (!this.rowArray.includes(srow)) this.rowArray.push(srow);
        if (!this.rowElementMap.hasOwnProperty(srow)) this.rowElementMap[srow] = [];
        this.rowElementMap[srow].push(id);
    }
    /*addHiddenField(id: string, pk: boolean) {
        var filter = new BasicCrudElement(id, pk, false, '', 12, 'TX', null, null, null);
        filter.hidden = true;
        this.elementMap[id] = filter;
    }*/

    addTextField(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue,
            sequence: conf.sequence,
            assistant: conf.assistant ? conf.assistant : false,
            disabled: conf.disabled ? conf.disabled : false,
            password: conf.password ? conf.password : false

        });
        this.elementMap[conf.id] = filter;

    }
    addNumberField(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.NUMBER_FIELD,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue,
            sequence: conf.sequence,
            assistant: conf.assistant ? conf.assistant : false,
            disabled: conf.disabled ? conf.disabled : false

        });
        this.elementMap[conf.id] = filter;

    }
    addCustomAssistant(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue
        });
        this.elementMap[conf.id] = filter;
    }

    addAutoCompleteVirtual(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.AUTO_COMPLETE_VIRTUAL,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue
        });
        this.elementMap[conf.id] = filter;
    }

    addTextAreaField(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.TEXT_AREA,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue
        });
        this.elementMap[conf.id] = filter;
    }
    addDateField(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.DATE_SELECT,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue
        });
        this.elementMap[conf.id] = filter;
    }
    addSelectField(conf: any) {
        var list = ListUtils.convertToListForArray(conf.data, conf.code, conf.desc);
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            field_code: conf.code,
            field_desc: conf.desc,
            field_in_table: conf.field_in_table,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            data: list,
            maxLength: conf.maxLength,
            defaultValue: conf.defaultValue,
            typeData: conf.typeData,
            showEmpty : conf.showEmpty ? conf.showEmpty : false
        });
        
        this.elementMap[conf.id] = filter;
    }
    addHiddenField(conf: any) {
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            mandatory: conf.mandatory,
            label: conf.label,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.HIDDEN,
            data: null,
            maxLength: conf.maxLength,
            typeData: conf.typeData,
            defaultValue: conf.defaultValue,
            sequence: conf.sequence
        });
        this.elementMap[conf.id] = filter;
    }
    addSelectServerPagination(conf: any) {
        //var list = ListUtils.convertToListForArray(conf.data, conf.code, conf.desc);
        this.addFieldToRowFilterMap(conf.row, conf.id);
        var filter = new BasicCrudElement({
            id: conf.id,
            pk: conf.pk,
            field_code: conf.code,
            field_desc: conf.desc,
            field_in_table: conf.field_in_table,
            mandatory: conf.mandatory,
            label: conf.label,
            functionPagination: conf.fun,
            bsWidth: conf.bsWidth, type: conmmonConstants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            data: [],
            maxLength: conf.maxLength,
            defaultValue: conf.defaultValue,
            typeData: conf.typeData,
            settings: {
                text: "",
                singleSelection: true,
                showCheckbox: true,
                badgeShowLimit: 1,
                noDataLabel: 'No existen datos',
                enableCheckAll: true,
                selectAllText: 'Seleccionar todos',
                unSelectAllText: 'Eliminar todos',
                classes: "custom-class-example ",
                enableSearchFilter: true,
                lazyLoading: true,
                primaryKey: 'COD',
                labelKey: 'DESCR'
            }
        });
        this.elementMap[conf.id] = filter;
    }
    setValue(elementId, value) {
        this.elementMap[elementId].value = value;
    }
    setDisabledAtInsertion(id) {
        this.elementMap[id].insertDisabled = true;
    }
    addIntegerValidation(elementId) {
        this.elementMap[elementId].addIntegerValidation();
    }
    addFloatValidation(elementId) {
        this.elementMap[elementId].addFloatValidation();
    }
    setDependency(parentId, childId) {
        this.elementMap[parentId].dependencies.push(childId);
    }
    getValidationErrors(): boolean {
        var result = false;
        for (var elementId in this.elementMap) {
            if (!this.elementMap[elementId].virtual) {
                var validationError = this.elementMap[elementId].loadValidations();
                result = result || validationError;
            }
        }
        return result;
    }
    getPrimaryKey(): any {

        for (var elementId in this.elementMap) {
            if (this.elementMap[elementId].pk) {
                return elementId;
            }
        }
        return null;
    }
    getModel(): any {
        var result = {};
        
        for (var elementId in this.elementMap) {
            if (this.elementMap[elementId].type == conmmonConstants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
                if (this.elementMap[elementId].value) {
                    result[elementId] = {
                        value: this.elementMap[elementId].value[0]["COD"],
                        type: this.elementMap[elementId].typeData
                    };

                    result[this.elementMap[elementId].field_in_table] = {
                        value: this.elementMap[elementId].value[0]["DESCR"],
                        type: this.elementMap[elementId].typeData
                    };
                }


            }
            else {
                if (this.elementMap[elementId].virtual) {
                    result[elementId] = {
                        value: this.elementMap[elementId].value,
                        type: this.elementMap[elementId].typeData
                    }
                }
                else {
                    result[elementId] = {
                        value: this.elementMap[elementId].getValue(),
                        type: this.elementMap[elementId].typeData
                    }
                }
            }
        }

        return result;
    }
    save() {
    }


}
export class BasicCrudElement {
    /*TYPE_SS = 'SS'; // Select
    TYPE_MS = 'MS'; // Multiple Select
    TYPE_DT = 'DT'; // Date
    TYPE_TX = 'TX'; // Input text
    TYPE_TA = 'TA'; // Textarea
    TYPE_MT = 'MT'; // Multiple Text
    TYPE_AC = 'AC'; // Autocomplete
    */
    VALIDATE_MADATORY = 'NN';
    VALIDATE_MAXLENGTH = 'ML';
    VALIDATE_INTEGER = 'I';
    VALIDATE_FLOAT = 'FL';
    VALIDATE_AUTOCOMPLETE = 'AC';
    validations = [];
    hidden: boolean = false;
    id: string;
    label: string;
    bsWidth: number;
    type: string;
    data: any[];
    field_code;
    field_desc;
    field_in_table;
    pk: boolean;
    dependencies: string[] = [];
    msModel: MultiselectModel;
    autocompleteOptions: any[] = null;
    mandatory: boolean;
    maxLength: number;
    validationError = '';
    value: any;
    defaultValue: any;
    insertDisabled = false;
    typeData;
    sequence: boolean = false;
    settingsMSV: any;
    functionPagination;
    public loading: boolean = false;
    filterText: any;
    public ofsset: number = 100;
    position: number = 0;
    firstTime: boolean = true;
    searching: boolean = false;
    assistant: boolean = false;
    disabled: boolean = false;
    showEmpty  : boolean = true;
    password : boolean = false;
    filters: any;

    constructor(conf) {
        this.hidden = false;
        this.id = conf.id;
        this.pk = conf.pk;
        this.mandatory = conf.mandatory;
        this.label = conf.label;
        this.bsWidth = conf.bsWidth;
        this.type = conf.type;
        this.data = conf.data;
        this.field_code = conf.field_code;
        this.settingsMSV = conf.settings;
        this.field_desc = conf.field_desc;
        this.functionPagination = conf.functionPagination;
        this.field_in_table = conf.field_in_table;
        this.typeData = conf.typeData;
        this.assistant = conf.assistant;
        this.maxLength = conf.maxLength;
        this.disabled = conf.disabled;
        this.showEmpty = conf.showEmpty;
        this.password = conf.password;
        if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
            this.value = null;
        }
        if (this.mandatory) this.validations.push(this.VALIDATE_MADATORY);
        if (this.maxLength) this.validations.push(this.VALIDATE_MAXLENGTH);
        /*if (this.type == this.TYPE_AC) {
            this.validations.push(this.VALIDATE_AUTOCOMPLETE);
        }*/
        this.validationError = '';
        this.defaultValue = conf.defaultValue;
        this.sequence = conf.sequence;
    }

    getValue(): any {
        if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.DATE_SELECT) {
            if (!this.value) {
                return null;
            } else {
                //alert(this.value);
                return moment(this.value).format('YYYY-MM-DD hh:mm:ss');
            }
        }
        else if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            return this.value

        }
        else if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.MULTI_SELECT) {
            return this.msModel.getSelectedElements();
        }

        else {
            return this.value;
        }
    }
    setValue(value) {
        if (value === null || value === "") {
            this.value = null;
        } else {
            if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.DATE_SELECT) {
                this.value = moment(value, "YYYY-MM-DD hh:mm:ss").format();
            } else if (this.type == conmmonConstants.TYPE_COMPONENT_FILTER.MULTI_SELECT) {
                this.msModel.selectedElements = value;
            } /*else if (this.type == this.TYPE_MT) {
                this.value = value.slice(0, value.length);
            } */else {
                this.value = value
            }
        }
    }
    addIntegerValidation() {
        this.validations.push(this.VALIDATE_INTEGER);
    }
    addFloatValidation() {
        this.validations.push(this.VALIDATE_FLOAT);
    }
    loadValidations(): boolean {
        var result = false;
        if (this.validations) {
            for (var validation of this.validations) {
                if (validation == this.VALIDATE_MADATORY) {
                    result = this.loadMandatoryValidation();
                } else if (validation == this.VALIDATE_MAXLENGTH) {
                    result = this.loadMaxLengthValidation();
                } else if (validation == this.VALIDATE_INTEGER) {
                    result = this.loadIntegerValidation();
                } else if (validation == this.VALIDATE_FLOAT) {
                    result = this.loadFloatValidation();
                } else if (validation == this.VALIDATE_AUTOCOMPLETE) {
                    result = this.loadAutocompleteValidation();
                }
                if (result) {
                    break;
                }
            }
        }
        return result;
    }
    loadMandatoryValidation(): boolean {
        var notValid = (!this.getValue() && this.getValue() !== false) && !(this.getValue() === 0);
        if (!notValid) {
            if (typeof this.getValue() === 'string' || this.getValue() instanceof String) notValid = !this.getValue().trim()
        }
        this.validationError = notValid ? 'Campo obligatorio' : '';
        return notValid;
    }
    loadMaxLengthValidation(): boolean {
        var result = this.getValue()
            && (this.maxLength)
            && (this.maxLength > 0)
            && (this.getValue().length > this.maxLength);
        this.validationError = result ? 'Longitud máxima (' + this.maxLength + ') superada' : '';
        return result;
    }
    loadIntegerValidation(): boolean {
        var value = this.getValue();
        var result = false;
        if (value) result = !Number.isInteger(Number(value));
        this.validationError = result ? 'El campo debe ser un número entero' : '';
        return result;
    }
    loadFloatValidation(): boolean {
        var value = this.getValue();
        var result = false;
        if (value) result = Number.isNaN(Number.parseFloat(value));
        this.validationError = result ? 'El campo debe ser un número float' : '';
        return result;
    }
    loadAutocompleteValidation(): boolean {
        const value = this.getValue();
        const result = !!value && !this.autocompleteOptions.some(option => option.COD == value);
        this.validationError = result ? 'El valor no es un valor permitido' : '';
        return result;
    }
    filterOptions(): any {
        const value = this.getValue();
        if (!value) {
            return [];
        }
        const search = value.toLocaleUpperCase();
        if (search.length < 3) {
            return this.autocompleteOptions.filter(option => option.COD.toLocaleUpperCase() == search);
        } else {
            const words = search.split(/[ \t]+/);
            return this.autocompleteOptions.filter(option => {
                for (let word of words) {
                    if (option.DESCR.toLocaleUpperCase().indexOf(word) == -1) {
                        return false;
                    }
                }
                return true;
            });
        }
    }
    changeSelectServer(event) {
        var self = this;
        this.loading = true;
        this.filterText = event.target.value;
        self.position = 0;
        if (!this.searching) {
            this.searching = true;
            this.functionPagination({ limit: this.ofsset, offset: this.position, text: this.filterText }).then(newList => {
                this.data = newList;
                self.position = newList.length;
                this.searching = false;
                this.loading = false;
            });
        }

    }
    fetchMoreSelectServer(event) {
        var self = this;
        setTimeout(() => {
            if (this.firstTime || (!self.searching && !self.loading && event.endIndex >= self.position - 1)) {
                self.loading = true;
                this.functionPagination({ limit: this.ofsset, offset: this.position, text: this.filterText }).then(newList => {

                    if (this.data.length == 0) {
                        self.data = self.data.concat(newList);
                        self.loading = false;
                    }
                    else {
                        var tmp = [];
                        for (var item of newList) {
                            if (!this.data.find(row => row['COD'] === item['COD'])) {
                                tmp.push(item);
                            }
                            self.data = self.data.concat(tmp);
                        }
                    }
                    self.firstTime = false;
                    self.position += this.ofsset;
                    self.loading = false;
                });
            }
        }, 200);



    }
    onSearchSelectServer(event: any, element: any) {
        var self = this;
        this.loading = true;
        this.filterText = event.target.value;
        self.position = 0;
        if (!this.searching) {
            this.searching = true;
            element.functionPagination({ limit: this.ofsset, offset: 0, text: this.filterText }).then(newList => {
                this.data = newList;
                self.position = newList.length;
                this.searching = false;
                this.loading = false;
            });
        }

    }
}