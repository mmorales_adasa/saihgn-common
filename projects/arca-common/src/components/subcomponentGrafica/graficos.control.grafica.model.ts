import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { Subscription } from 'rxjs';

import { DatePipe } from '@angular/common'
import { CUSTOM_DATE_FORMATS } from '../../config/constants';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { GenericTableCommonService, GenericTableExternalFilter, SaerchFromFilterObject } from './../../services/generic.table.common.service'
import * as htmlToImage from 'html-to-image';

import { Chart } from 'chart.js';
import { ChartUtils, ChartConfig, ChartConfigUtils } from './../genericTable/chart.utils';
import { ListUtils } from '../../utils/list.utils';
declare  let window: any;
export class GraficosControlGraficaModel{

    public parentId : string;
    public id : string;
    public x_element : string;
    public serie : string;
    public superdata : any[];
    public hasFiltroPatrones = true;

    // Utils
    xArray : string[];
    _datePipe : DatePipe;
    fechaIniString = '';
    fechaFinString = '';
    public sinLimiteInferior = false;

    // Chart
    public chart : Chart;
    public chartCanvas : ElementRef;
    public chartConfig : ChartConfig;
    public chartUtils : ChartUtils = new ChartUtils();

    // Cálculos
    public limiteInferior : number = 0.0;
    public limiteSuperior : number = 100.0;
    public media : number;
    public desviacionEstandar : number;
    public coeficienteVariacion : number;

    // Filtro patrones
    public patronCombo : number[];
    public patronSelected : number;

    // Combos
    public parametroCombo = [];
    public parametroSelected = '';
    public metodoCombo = [];
    public metodoSelected = '';



    constructor(parentId : string, id : string, x_element : string, serie : string){
        this.parentId = parentId;
        this.id = id;
        this.x_element = x_element;
        this.serie = serie;
    }

    // Export chart to png image
    exportChartToPngImage(){
        htmlToImage.toBlob(this.chartCanvas.nativeElement)
        .then(function (blob) {
            window.saveAs(blob, 'grafica.png');
        });
    }

    public preLoad(){         
        // Obtenemos combos
        this.parametroCombo = ListUtils.getMasterComboFromObjectList(this.superdata, 'COD_PARAMETRO').keyList;
        if(this.parametroCombo.length == 0) this.parametroCombo = ListUtils.getMasterComboFromObjectList(this.superdata, 'DESC_PARAMETRO').keyList;
        if(this.parametroCombo.length > 0) this.parametroSelected = this.parametroCombo[0];
        this.metodoCombo = ListUtils.getMasterComboFromObjectList(this.superdata, 'COD_METODO').keyList;
        if(this.metodoCombo.length > 0) this.metodoSelected = this.metodoCombo[0];

        // Obtenemos combo de patrones
        if(this.hasFiltroPatrones){
            this.patronCombo = ListUtils.getMasterComboFromObjectList(this.superdata, 'VALOR_PATRON').keyList;
            this.patronCombo = ListUtils.sortNumberList(this.patronCombo, 'asc');
        }

        this.loadChart();
    }

    public loadChart(){

        var x_element = this.x_element;
        var serie = this.serie;
        var data = this.superdata;

        // Filtramos por parámetro y combo
        data = ListUtils.filterObjectList(data, 'COD_PARAMETRO', [this.parametroSelected]);
        data = ListUtils.filterObjectList(data, 'COD_METODO', [this.metodoSelected]);

        // Si es necesario, filtramos por patron
        if(this.hasFiltroPatrones && this.patronSelected){
            data = ListUtils.filterObjectList(data, 'VALOR_PATRON', [ '' + this.patronSelected]);
        }
        
        // Calculamos media
        var total = data.map(t => t[serie]).reduce((acc, value) => acc + Number(value), 0);
        this.media = total / data.length;

        // Calculamos desviación estándar y coeficiente de variación
        var dstd = 0;
        var values = ListUtils.getMasterComboFromObjectList(data, serie).keyList;
        for(var v of values) dstd += Math.pow(v - this.media, 2);
        this.desviacionEstandar = Math.sqrt(dstd / (values.length-1));
        this.coeficienteVariacion = this.desviacionEstandar / Math.abs(this.media);

        // Ajustamos decimales
        this.media = Number(this.media.toFixed(2));
        this.desviacionEstandar = Number(this.desviacionEstandar.toFixed(2));
        this.coeficienteVariacion = Number(this.coeficienteVariacion.toFixed(2));

        // Generamos array de datos para la gráfica
        var dataForChart = [];
        var limiteInferior = this.limiteInferior;
        var limiteSuperior = this.limiteSuperior;
        var media = this.media;
        data.forEach(function(element) {
            dataForChart.push({
                x: element[x_element],
                limiteInferior: limiteInferior,
                limiteSuperior: limiteSuperior,
                valor: element[serie],
                media: media,
            });
        });
                
        // Generamos configuración de chart
        var li = Number(this.limiteInferior).toFixed(2);
        var ls = Number(this.limiteSuperior).toFixed(2);

        if(!this.sinLimiteInferior){
            var series = ['limiteInferior', 'limiteSuperior', 'valor', 'media'];
            var serieLabels = ['Límite inferior = ' + li, 'Límite superior = ' + ls, '% Recuperación', 'Media = ' + this.media.toFixed(2)];
            this.chartConfig = ChartConfigUtils.generateBarChartConfig('line', ['x'], series, serieLabels);
            this.chartConfig.colors = ['rgba(255, 80, 80, 0.8)', 'rgba(255, 80, 80, 0.8)', 'rgba(100, 100, 255, 0.8)', 'rgba(120, 209, 120, 0.8)' ];
            this.chartConfig.fills = [false, false, false, false];
            this.chartConfig.pointSizes = [0, 0, 3, 0];
            this.chartConfig.borderWidths = [2, 2, 2, 2];
        } else {
            var series = ['limiteSuperior', 'valor', 'media'];
            var serieLabels = ['Límite superior = ' + ls, '% Recuperación', 'Media = ' + this.media.toFixed(2)];
            this.chartConfig = ChartConfigUtils.generateBarChartConfig('line', ['x'], series, serieLabels);
            this.chartConfig.colors = ['rgba(255, 80, 80, 0.8)', 'rgba(100, 100, 255, 0.8)', 'rgba(120, 209, 120, 0.8)' ];
            this.chartConfig.fills = [false, false, false];
            this.chartConfig.pointSizes = [0, 3, 0];
            this.chartConfig.borderWidths = [2, 2, 2];
        }

        // Generamos chart
        this.chart = this.chartUtils.dataToChartObject(this.chartCanvas, this.chartConfig, dataForChart); 
    }


}
