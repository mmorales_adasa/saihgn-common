import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DatePipe } from '@angular/common'
import { CUSTOM_DATE_FORMATS } from '../../config/constants';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { GenericTableCommonService, GenericTableExternalFilter, SaerchFromFilterObject } from './../../services/generic.table.common.service'
import { GraficosControlGraficaModel } from './graficos.control.grafica.model'
import * as htmlToImage from 'html-to-image';

import { Chart } from 'chart.js';
import { ChartUtils, ChartConfig, ChartConfigUtils } from './../genericTable/chart.utils';
import { ListUtils } from '../../utils/list.utils';

declare let window: any;
@Component({
    selector: 'app-graficos-control-grafica',
    templateUrl: './graficos.control.grafica.component.html',
    styleUrls: ['./graficos.control.grafica.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GraficosControlGraficaComponent implements OnInit, OnDestroy {

    public collapsed = false;
    subscription: Subscription;
    // Gráfica
    @Input() tableService: any;
    // Gráfica
    @Input() grafica: GraficosControlGraficaModel;

    // Chart    
    @ViewChild('chartcanvas') chartCanvas: ElementRef;


    constructor(

        private _genericTableCommonService: GenericTableCommonService,
        private _datePipe: DatePipe,
        private _cd: ChangeDetectorRef
    ) {

    }
    ngOnInit() {
        this.subscription = this.tableService.afterLoadTableNewCall.subscribe((filter: GenericTableExternalFilter) => {
            if ((filter.parent == this.grafica.parentId) && (filter.id == this.grafica.id)) {
                this.grafica.chartCanvas = this.chartCanvas;
                this.grafica._datePipe = this._datePipe;
                this.grafica.superdata = filter.data;
                this.grafica.preLoad();
                this._cd.markForCheck();
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    toggle() {
        this.collapsed = !this.collapsed;
    }




}
