import { MultiselectModel } from "../multiselect/multiselect.model";
import { MultiselectModelMV } from "../multiselect/multiselectMV.model";
import { ListUtils } from "../../utils/list.utils";
import { constants } from "../../config/constants";
import { asLiteral } from "@angular/compiler/src/render3/view/util";
import { Self } from "@angular/core";
import moment from 'moment';
export class BasicFilter {
    //id: string;
    rowArray: string[] = [];
    options;
    rowElementMap: any;
    elementMap: any;
    considerEmptyError = true;
    maxElementsInDesc = 3;
    searchValues = {};
    dateError = false;
    emptyError = false;
    errorMessage = '';
    dateValidation: boolean = false;
    dateValidation_iniDateId: string;
    dateValidation_endDateId: string;
    dateValidation_elements: string[] = [];
    dateInvalidationIsActive: boolean = false;
    constructor(conf) {
        this.rowElementMap = {};
        this.elementMap = {};
        this.options = conf;
    }
    public addFilter(conf: any) {
        var self = this;
        this.addFieldToRowFilterMap(conf.group, conf.id);
        if (conf.dataComponent === constants.TYPE_COMPONENT_FILTER.TEXT_FIELD || conf.dataComponent === constants.TYPE_COMPONENT_FILTER.NUMBER_FIELD) {
            var filter = new BasicElementFilter({
                id : conf.id,
                label : conf.label,
                bsWidth : null,
                type : conf.dataComponent,
                superdata : null,
                defaultValue : conf.defaultValue,
                searchOption : null,
                returnValueAsMultiselect : false,
                typeData : conf.dataType ? conf.dataType : constants.TYPE_DATA_FILTER.TYPE_TEXT,
                minLength : conf.width
            });
            filter.minLength = conf.width;
            this.elementMap[conf.id] = filter;
            //conf, id, label, bsWidth, type, superdata, defaultValue, searchOption, returnValueAsMultiselect, typeData
        }
        if (conf.dataComponent === constants.TYPE_COMPONENT_FILTER.DATE_SELECT) {
            var filter = new BasicElementFilter({
                id : conf.id,
                label : conf.label,
                bsWidth : null,
                type : conf.dataComponent,
                superdata : null,
                defaultValue : conf.defaultValue,
                searchOption : null,
                returnValueAsMultiselect : false,
                typeData : conf.dataType ? conf.dataType : constants.TYPE_DATA_FILTER.DATE,
                minLength : conf.width
            });
            filter.minLength = conf.width;
            this.elementMap[conf.id] = filter;
        }
        if (conf.dataComponent === constants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
            if (conf.data) {
                var list = ListUtils.convertToListForArray(conf.data, conf.code, conf.desc);
                var filter = new BasicElementFilter({
                    id : conf.id,
                    label : conf.label,
                    bsWidth : null,
                    type : conf.dataComponent,
                    superdata : list,
                    defaultValue : null,
                    searchOption : conf.searchOption,
                    returnValueAsMultiselect : true,
                    typeData : conf.dataType ? conf.dataType : constants.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING,
                    minLength : conf.width
                });
                filter.minLength = conf.width;
                this.elementMap[conf.id] = filter;
            }
        }
        if (conf.dataComponent === constants.TYPE_COMPONENT_FILTER.MULTI_SELECT) {
            var list = ListUtils.convertToListForArray(conf.data, conf.code, conf.desc);
            var filter = new BasicElementFilter({
                id : conf.id,
                label : conf.label,
                bsWidth : null,
                type : conf.dataComponent,
                superdata : list,
                defaultValue : null,
                searchOption : conf.searchOption,
                returnValueAsMultiselect : true,
                typeData : conf.dataType ? conf.dataType : constants.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING,
                minLength : conf.width                
            });
            filter.minLength = conf.width;
            self.elementMap[conf.id] = filter;
        }
        if (conf.dataComponent === constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            var filter = new BasicElementFilter({
                id : conf.id,
                label : conf.label,
                bsWidth : null,
                type : conf.dataComponent,
                superdata : null,
                defaultValue : null,
                searchOption : conf.searchOption,
                returnValueAsMultiselect : true,
                typeData : conf.dataType ? conf.dataType : constants.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING,
                minLength : conf.width,
                functionPagination : conf.fun,
                disabled : conf.disabled,
            });
            filter.minLength = conf.width;
            filter.functionPagination = conf.fun;
            filter.disabled = conf.disabled;            
            self.elementMap[conf.id] = filter;
        }
        this.considerEmptyError = conf.considerEmptyError;
    }
    addFieldToRowFilterMap(row: number, id: string) {
        var srow = '' + row;
        if (!this.rowArray.includes(srow)) this.rowArray.push(srow);
        if (!this.rowElementMap.hasOwnProperty(srow)) this.rowElementMap[srow] = [];
        if (!this.rowElementMap[srow].includes(id)) this.rowElementMap[srow].push(id);
    }
    addAfeccion(elementId, superElementId, superCod) {
        this.elementMap[elementId].afeccionIdArray.push(superElementId);
        this.elementMap[elementId].afeccionSupercodArray.push(superCod);
        this.elementMap[superElementId].afeccionDependences.push(elementId);
        // Actualizamos lista
        if (this.elementMap[elementId].type != constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            this.updateCombo(elementId, superElementId);
        }
    }
    setValue(elementId, value) {
        this.elementMap[elementId].selectedValue = value;
    }
    getSearchValue(elementId) {
        return this.searchValues[elementId];
    }
    // On Change
    change(elementId) {
        // Comprobamos si afecta a Date Invalidation
        if (this.dateValidation_elements.includes(elementId)) {
            if (this.elementMap[elementId].selectedValue) {
                this.elementMap[this.dateValidation_iniDateId].disabled = true;
                this.elementMap[this.dateValidation_endDateId].disabled = true;
                this.dateInvalidationIsActive = true;
            } else {
                this.elementMap[this.dateValidation_iniDateId].disabled = false;
                this.elementMap[this.dateValidation_endDateId].disabled = false;
                this.dateInvalidationIsActive = false;
            }
        }
        // Comprobamos si hay que haer validación de fechas
        this.dateError = false;
        this.errorMessage = '';
        if (!this.dateInvalidationIsActive && this.dateValidation_elements.concat([this.dateValidation_iniDateId, this.dateValidation_endDateId]).includes(elementId)) {
            if (!this.elementMap[this.dateValidation_iniDateId].selectedValue) this.dateError = false;
            else if (!this.elementMap[this.dateValidation_endDateId].selectedValue) this.dateError = false;
            else this.dateError = this.elementMap[this.dateValidation_endDateId].selectedValue < this.elementMap[this.dateValidation_iniDateId].selectedValue;
            if (this.dateError) {
                this.errorMessage = 'La fecha inicial no puede ser posterior a la fecha final';
            }
        }
        // Comprobamos si afecta a algún otro filtro
        if (this.elementMap[elementId].type != "MSV") {
            for (var elementId_ in this.elementMap) {
                this.updateCombo(elementId_, elementId);
            }
        }
        if (this.elementMap[elementId].type == "MSV") {
            this.elementMap[elementId].msModel.setElements(this.elementMap[elementId].selectedValue);
            this.updateComboMSV(elementId);
        }
    }



    updateComboMSV(elementId) {
        var list = [];
        list.push({
            field: elementId,
            selectedValues: this.elementMap[elementId].msModel.getSelectedElements()
        });
        for (var item of this.elementMap[elementId].afeccionDependences) {
            // Otras affeciones 
            var others = this.elementMap[item].afeccionSupercodArray;
            for (var other of others) {
                if (other != elementId) {
                    list.push({
                        field: other,
                        selectedValues: this.elementMap[other].msModel.getSelectedElements()
                    });
                }
            }
            this.elementMap[item].msModel.reload(this.elementMap[item], list);
        }
    }
    updateCombo(elementId, superElementId) {
        if (this.elementMap[elementId].afeccionIdArray.includes(superElementId)) {
            var superValues = this.elementMap[superElementId].getValueArray();
            var index = this.elementMap[elementId].afeccionIdArray.indexOf(superElementId);
            var superCod = this.elementMap[elementId].afeccionSupercodArray[index];
            // SELECT
            if (this.elementMap[elementId].type == 'SS') {
                var ssData = [];
                if (superValues && (superValues.length == 0)) {
                    ssData = this.elementMap[elementId].superdata;
                } else if (superValues) {
                    ssData = this.elementMap[elementId].superdata.filter(m => superValues.includes(m[superCod]));
                }
                // Evitamos duplicados
                ssData = ListUtils.getMasterComboFromObjectListWithFields(ssData, 'COD', ['DESCR']);
                this.elementMap[elementId].data = ssData;
                // Opción por defecto;
                if (this.elementMap[elementId].data.length > 0) {
                    this.elementMap[elementId].selectedValue = this.elementMap[elementId].data[0].COD;
                }
            }
            // MULTISELECT
            if (this.elementMap[elementId].type == 'MS') {
                var msData = [];
                if (superValues && (superValues.length == 0))
                    msData = this.elementMap[elementId].superdata;
                else {
                    msData = this.elementMap[elementId].superdata.filter(m => superValues.includes(m[superCod]));
                }
                // Evitamos duplicados
                msData = ListUtils.getMasterComboFromObjectListWithFields(msData, 'COD', ['DESCR']);
                this.elementMap[elementId].msModel.setElements(msData);
            }
        }
    }
    // Clean
    clean() {
        for (var elementId in this.elementMap) {
            this.elementMap[elementId].clean();
        }
        if (this.dateValidation) {
            this.dateInvalidationIsActive = false;
            this.elementMap[this.dateValidation_iniDateId].disabled = false;
            this.elementMap[this.dateValidation_endDateId].disabled = false;
        }
    }
    // Search data
    search(): any {
        var resultSuperObject = {};
        var summaryIds = [];
        var summary = {};
        this.emptyError = false;
        this.searchValues = {};
        var filters = [];
        if (!this.dateError) {
            for (var elementId in this.elementMap) {
                if (this.considerElement(elementId)) {
                    var value = this.elementMap[elementId].getValue();
                    this.searchValues[elementId] = value;
                    if (value) {
                        summaryIds.push(elementId);
                        summary[elementId] = this.elementMap[elementId].getSummary(this.maxElementsInDesc);
                        filters.push({
                            id: elementId,
                            type: this.elementMap[elementId].typeData,
                            values: value
                        })
                    }
                }
            }
            if (this.considerEmptyError && (filters.length == 0)) {
                this.emptyError = true;
                this.errorMessage = 'Es necesario seleccionar al menos un filtro';
            } else {
                resultSuperObject['SUMMARY_ID_ARRAY'] = summaryIds;
                resultSuperObject['SUMMARY_MAP'] = summary;
            }
        }
        resultSuperObject['filter'] = filters;
        return resultSuperObject;
    }
    considerElement(elementId) {
        if (!this.dateValidation) return true;
        else if (!this.dateInvalidationIsActive) return true;
        else if ([this.dateValidation_iniDateId, this.dateValidation_endDateId].includes(elementId)) return false;
        else return true;
    }
}
export class BasicElementFilter {
    id: string;
    label: string;
    bsWidth: number;
    minLength: number;
    functionPagination: any;
    type: string;
    superdata: any[];
    data: any[];
    defaultValue: any;
    returnValueAsMultiselect = false;
    msModel = null;
    selectedValue: any;
    disabled = false;
    dirty = false;
    afeccionIdArray = [];
    afeccionSupercodArray = [];
    afeccionDependences = [];
    typeData: any;
    constructor(conf) {
        this.id = conf.id;
        this.label = conf.label;
        this.bsWidth = conf.bsWidth;
        this.type = conf.type;
        this.defaultValue =conf.defaultValue;
        this.superdata = conf.superdata;
        this.typeData = conf.typeData;
        this.disabled = conf.disabled,
        this.functionPagination = conf.functionPagination
        this.data = ListUtils.getMasterComboFromObjectListWithFields(conf.superdata, 'COD', ['DESCR']);
        this.returnValueAsMultiselect = conf.returnValueAsMultiselect;
        if (this.type == constants.TYPE_COMPONENT_FILTER.TEXT_FIELD || this.type == constants.TYPE_COMPONENT_FILTER.NUMBER_FIELD) {
            this.selectedValue = conf.defaultValue;
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.MULTI_SELECT) {
            this.msModel = new MultiselectModel('COD', 'DESCR', conf.searchOption);
            this.msModel.setElements(this.data);
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            this.msModel = new MultiselectModelMV('COD', 'DESCR', conf.searchOption);
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
            if (conf.defaultValue) this.selectedValue = null;
            else this.selectedValue = this.superdata[0] ? this.superdata[0].COD : null;
        }
    }
    getValue(): any {
        if (this.type == constants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
            if (!this.returnValueAsMultiselect || !this.selectedValue) return this.selectedValue;
            else return [this.selectedValue];
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.TEXT_FIELD || this.type == constants.TYPE_COMPONENT_FILTER.NUMBER_FIELD) return this.selectedValue;
        if (this.type == constants.TYPE_COMPONENT_FILTER.DATE_SELECT) {
            if (!this.selectedValue) return null;
            //alert(this.selectedValue);
            //var m = moment(this.selectedValue).format('DD/MM/YYYY');
            var m = moment(this.selectedValue).format('YYYY-MM-DD');
            /*alert(m);
            var date = this.selectedValue.toDate();
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            day = (day < 10) ? '0' + day : '' + day;
            month = (month < 10) ? '0' + month : '' + month;
            return '' + day + '/' + month + '/' + year;
            */ return m;
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.MULTI_SELECT || this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            var value = this.msModel.getSelectedElements();
            if (!value || (value.length == 0)) value = null;
            return value;
        }
    }
    getValueArray(): any[] {
        if (this.type == constants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
            var value = this.getValue();
            return value ? [value] : [];
        }
        if (this.type == constants.TYPE_COMPONENT_FILTER.MULTI_SELECT || this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) return this.msModel.getSelectedElements();
    }
    getSummary(maxElementsInDesc) {
        var summary = '';
        var value = this.getValue();
        if (value) {
            if (this.type == constants.TYPE_COMPONENT_FILTER.TEXT_FIELD || this.type == constants.TYPE_COMPONENT_FILTER.NUMBER_FIELD) summary += value;
            if (this.type == constants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) summary += this.superdata.filter(m => m.COD == value)[0].DESCR;
            if (this.type == constants.TYPE_COMPONENT_FILTER.DATE_SELECT) summary += value;
            if (this.type == constants.TYPE_COMPONENT_FILTER.MULTI_SELECT || this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
                var s = this.msModel.getSelectedSummary();
                var ss = s.split(', ');
                if (ss.length <= maxElementsInDesc) summary += s;
                else {
                    for (var i = 0; i < maxElementsInDesc; i++) {
                        if (i != 0) summary += ', ';
                        summary += ss[i];
                    }
                    var restantes = ss.length - maxElementsInDesc;
                    summary += ' (+' + restantes + ')';
                }
            }
        }
        return new BasicElementFilterSummary(this.id, this.label, summary);
    }
    clean() {
        
        if (this.type == constants.TYPE_COMPONENT_FILTER.MULTI_SELECT) {
            this.msModel.cleanSelection();
        }
        else if (this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            this.selectedValue = [];
            this.msModel.selectedElements = [];
        }
        else this.selectedValue = null;
    }
    refresh(){
        if (this.type == constants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
            this.msModel.onSearch(null, this);            
        }
    }
  
}
export class BasicElementFilterSummary {
    id: string;
    label: string;
    value: string;
    constructor(id: string, label: string, value: string) {
        this.id = id;
        this.label = label;
        this.value = value;
    }
}
