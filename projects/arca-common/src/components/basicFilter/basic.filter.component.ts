import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BasicFilter } from './basic.filter.model';
@Component({
    selector: 'app-basic-filter',
    templateUrl: './basic.filter.component.html',
    styleUrls: ['./basic.filter.component.scss', './../../styles/angular2.multiselect.drowpdown.default.theme.css'],
    providers: []
})
export class BasicFilterComponent implements OnInit {
    @Input() filter: BasicFilter;
    @Output() onSearch = new EventEmitter<any>();
    @Output() onClear = new EventEmitter<any>();
    @Output() onChangeComponent = new EventEmitter<any>();
    filterOpened = true;
    summaryIds = [];
    summary = {};
    itemList = [];
    loading = false;
    indices: any;
    constructor() { }
    ngOnInit() {
        if (this.filter.options.initClosed) {
            this.filterOpened = false};        
            if (this.filter.options.initSearch) {
            setTimeout(() => this.search(), 100);
        }
    }
    ngOnDestroy() {
    }
    toggleFilter() {
        this.filterOpened = !this.filterOpened;
    }
    clear() {
        this.filter.clean();
        this.onClear.emit({});

    }
    expandClosePanel(section) {
        section.open = !section.open;
    }
    search() {
        var filterSelection = this.filter.search();
        if (!this.filter.dateError && !this.filter.emptyError) {
            for (var row of this.filter.rowArray) {
                for (var item of this.filter.rowElementMap[row]) {
                    this.filter.elementMap[item].dirty = false;
                }
            }
            this.summaryIds = filterSelection['SUMMARY_ID_ARRAY'];
            this.summary = filterSelection['SUMMARY_MAP'];
            var s = filterSelection['SUMMARY_TYPE'];

            this.onSearch.emit({filterSelection: filterSelection['filter']});
            if (this.filter.options.autoClose){
                this.filterOpened = false;
            }
        }
    }
    openAddForm() {
        
    }
    getWidthInput(elementId) {
        return this.filter.elementMap[elementId].minLength + "px";
    }
    getClassDirty(elementId) {
        if (this.filter.elementMap[elementId].dirty) {
            return "filter-dirty";
        }
        else {
            return "";
        }
    }
    onChangeFilter(event){
        this.onChangeComponent.emit(event);
    }    
} 
