import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {  BasicFilter } from './../basic.filter.model';
//import { AuditoriaConstants, AuditoriaService } from '../../../services/auditoria.service';
import { AuditoriaService }   from  './../../../services/auditoria.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { find } from 'rxjs/operators';
@Component({
    selector: 'app-basic-filter-element',
    templateUrl: './basic.filter.element.html',
    styleUrls: ['./basic.filter.element.scss', './../../../styles/angular2.multiselect.drowpdown.default.theme.css'],
    providers: [AuditoriaService,]
}) 
export class BasicFilterElement implements OnInit {
    @Input() filter: BasicFilter;
    @Input() elementId: string;
    @Output() onChangeFilter = new EventEmitter<any>();
    filterOpened = true;
    summaryIds = [];
    summary = {};
    itemList = [];
    loading = false;
    indices: any;
    constructor() { 
    }
    ngOnInit() {
    }
    ngOnDestroy() {
    }
    toggleFilter() {
        this.filterOpened = !this.filterOpened;
    }
    search() {
        var filterSelection = this.filter.search();
        if (!this.filter.dateError && !this.filter.emptyError) {
            this.summaryIds = filterSelection['SUMMARY_ID_ARRAY'];
            this.summary = filterSelection['SUMMARY_MAP'];
            this.filterOpened = false;
        }
    }
    openAddForm() {
        //this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.filter.id, 'I', {}));
    }
    getWidthInput(element) {
        //return element.minLength + "px";
        return "100%";
    }
    getStyleFilterApply() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    getStyleFilterClean() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    applyFilter() {
        this.search();
        for (var row of this.filter.rowArray) {
            for (var item of this.filter.rowElementMap[row]) {
                this.filter.elementMap[item].dirty = false;
            }
        }
    }
    removeFilter() {
        this.filter.elementMap[this.elementId].dirty = true;
    }
    setDirty() {
        this.filter.elementMap[this.elementId].dirty = true;
        this.onChangeFilter.emit({
            element : this.elementId,
            value : this.filter.elementMap[this.elementId].selectedValue,
            info : this.filter.elementMap[this.elementId]
        });              
    }
    getClassDirty() {
        if (this.filter.elementMap[this.elementId].dirty) {
            return "filter-dirty";
        }
        else {
            return "";
        }
    }
} 
