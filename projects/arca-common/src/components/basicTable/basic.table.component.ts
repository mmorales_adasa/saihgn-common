import { Component, EventEmitter, OnInit, Output, AfterViewInit, Input, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasicTableModel } from './basic.table.model';
import { MatPaginator, MatPaginatorIntl, MatDialogRef, MatSort, MatDialog, PageEvent, MatTableDataSource } from '@angular/material';
import { MatPaginatorIntlCustom } from './basic.table.custom.paginator';
import { constants } from '../../config/constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { constants as conmmonConstants } from '../../config/constants';
import { BasicCallService } from './../../services/basic.call.service';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-basic-table',
    styleUrls: ['basic.table.component.css'],
    templateUrl: './basic.table.component.html',
    providers: [BasicCallService,
        { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCustom }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class BasicTableComponent implements OnInit, AfterViewInit {
    @Input() table: BasicTableModel;
    @Input() forceRender: number;
    @Output() onPagination = new EventEmitter<any>();
    @Output() onSort = new EventEmitter<any>();
    @Output() afterExecuteCrudAction  = new EventEmitter<any>();
    @Output() changeCrudValue  = new EventEmitter<any>();
    @Output() beforeExecuteCrudAction = new EventEmitter<any>();
    @Output() onAdd = new EventEmitter<any>();
    @Output() onChecked = new EventEmitter<any>();
    @Output() onExecuteTableUtilityAction = new EventEmitter<any>();
    @Output() onExecuteTableRowUtilityAction = new EventEmitter<any>();
    @ViewChild('modalCrud') modalCrud: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    pageEvent: PageEvent;


    constructor(
        private _cd: ChangeDetectorRef,
        public modalService: NgbModal,
        public dialog: MatDialog,
        public _basicCallService: BasicCallService,
    ) { }
    ngOnInit() {
        this.table._cd = this._cd;
    }
    public getServerData(event) {
        var self = this;
        if (this.table.serverPagination) {
            self.onPagination.emit(event);
        }
        this.pageEvent = event;
    }
    ngAfterViewInit() {
        var self = this;
        this.table.getDataSourceChanged().subscribe(data => {
            var self = this;
            self.table.stopLoading();
            if (self.table.serverPagination && self.sort) {
                self.sort.sortChange.subscribe((event) => {
                    self.onSort.emit(event);
                });

                self.table.dataSource = new MatTableDataSource(data);
                setTimeout(function () {
                    if (self.pageEvent) {
                        self.paginator.pageIndex = self.pageEvent.pageIndex;
                        self.paginator = self.table.dataSource.paginator;
                    }
                }, 2000);
            }
            else {
                self.table.dataSource = new MatTableDataSource(data);
                setTimeout(function () {
                    self.table.dataSource.paginator = self.paginator;
                    self.paginator.pageIndex = 0;
                }, 2000);
            }

        });/*
        if (!this.table.serverPagination) {
            setTimeout(function () {
                self.table.dataSource.paginator = self.paginator;
                self.paginator.pageIndex = 0;
            }, 1000);
        }*/
    }
    selectAll(selected) {
        this.table.selectAll(selected);
        this.onChecked.emit({
            type: "ALL",
            action: "CHECKED",
            selected: selected
        });
    }
    selectRow(selected, selectedRow) {
        if (this.table.selectableUnique){
            this.table.selectAll(false);
        }

        this.table.selectRow(selected, selectedRow);
        this.onChecked.emit({
            type: "SINGLE",
            action: "CHECKED",
            row: selectedRow,
            selected: selected
        });
    }
    openEditForm(tableRow, index) {
        this.table.crudModel.mode = constants.BASIC_CRUD_MODE.EDITION;
        this.table.crudModel.rowSelected = tableRow;        
        this.modalService.open(this.modalCrud, { size: 'lg', centered: true });
        this.beforeExecuteCrudAction.emit({
            action: this.table.crudModel.mode            
        });          
    }
    openAddForm() {

        this.table.crudModel.mode = constants.BASIC_CRUD_MODE.INSERT;
        this.modalService.open(this.modalCrud, { size: 'lg', centered: true });
        this.beforeExecuteCrudAction.emit({
            action: this.table.crudModel.mode
        });        
    }
    openDeleteForm(tableRow, index) {
        var self = this;        
        this.table.crudModel.mode = constants.BASIC_CRUD_MODE.DELETE;
        const dialogRef = this.dialog.open(BasicTableDeleteDialog, { width: '400px' });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                var data = this.table.crudModel.getModel()
                for (var key in data) {
                    data[key].value = tableRow[key];
                }
                var tmp = Object.assign({}, this.table.crudModel.serviceParams);
                tmp["model"] = data;
                tmp.mode = constants.BASIC_CRUD_MODE.DELETE;
                
                if (this.table.crudModel.withPersistence) {
                    this._basicCallService.execute(this.table.crudModel.service, tmp).subscribe((result: any[]) => {
                        dialogRef.close();
                        self.updateTableAfterDeletion(tableRow);
                        
                        self.afterExecuteCrudAction.emit({
                            action: this.table.crudModel.mode,
                            data : tmp
                        });
                    });
                }
                else{
                    self.afterExecuteCrudAction.emit({
                        action: this.table.crudModel.mode,
                        data : tableRow
                    });                    
                } 
            }
        });
    }
    onchangeValue(event) {
        var self = this;        self.changeCrudValue.emit({
            data : event,
        });        

    }
    onExecuteActionCrud(event) {
        var self = this;
        if (event.action == "save") {
            if (this.table.crudModel.mode == conmmonConstants.BASIC_CRUD_MODE.EDITION) {                
                if (this.table.crudModel.withPersistence) {
                    this.updateTableAfterEdition(event.data);
                }
                
                self.afterExecuteCrudAction.emit({
                    data : event.data,
                    action: this.table.crudModel.mode
                });

            }
            if (this.table.crudModel.mode == conmmonConstants.BASIC_CRUD_MODE.INSERT) {
                self.afterExecuteCrudAction.emit({
                    data : event.data,
                    action: this.table.crudModel.mode
                });
            }
            this.modalService.dismissAll();
        }
        else if (event.action == "cancel") {
            this.modalService.dismissAll();
        }
        else if (event.action == "openAssistant") {
            self.afterExecuteCrudAction.emit({
                action: "openAssistant",
                data : event.data,
            });
        }
        
    }
    updateTableAfterEdition(model) {
        var pkAttributes = this.table.pkAttributes;
        this.setModelValues(model,
            this.table.dataSource.data.find(function (element) {
                var result = false;
                for (var pk of pkAttributes) {
                    result = (element[pk] == model[pk]);
                    if (!result) return;
                }
                return result;
            })
        );
    }
    updateTableAfterDeletion(model) {
        var pkAttributes = this.table.pkAttributes;
        var data = this.table.dataSource.data.filter(m => {
            var result = false;
            for (var pk of pkAttributes) {
                result = (m[pk] == model[pk]);
                if (!result) break;
            }
            return !result;
        });
        this.table.dataSource.data = data;
    }
    setModelValues(_elementFrom, elementTo) {
        var data = [_elementFrom];
        var elementFrom = data[0];
        for (var key of Object.keys(elementFrom)) {
            elementTo[key] = elementFrom[key];
        }
    }
    /*
    executeSave() {
        
        this.table.crudModel.save();
        this.onExecuteAction.emit({
            action: this.table.crudModel.mode
        });
    }*/

    resetEdition() {
        this.table.dataSource.data.filter(m => m['EDITING']).forEach(function (element) {
            delete element['EDITING'];
        });
    }
    exportTableAsExcel() {
        this.table.exportTableAsExcel();
    }
    clickOnTableElement(elementCod, elementValue) {

    }
    throwTableUtility(id) {
        this.onExecuteTableUtilityAction.emit({
            action: id
        });
    }
    throwTableRowUtility(column, tableRow ) {
        this.onExecuteTableRowUtilityAction.emit({
            action: column,
            row: tableRow            
        });

        //this.resetEdition();
        //this.table.dataSource.data[index]['EDITING'] = true;
        //var id = this.table.tableRowUtilityObjects[column].id;
        // this._BasicTableCommonService.searchFromFilter(new SaerchFromFilterObject2(id, tableRow));
    }
    tableRowUtilityContextMenu(event, row, column) {
        event.preventDefault();
        if (this.table.tableRowUtilityObjects[column].onContextMenu) {
            this.table.tableRowUtilityObjects[column].onContextMenu(event, row, column);
        }
    }
    getAllColumnAttributes(column): any {

        return this.table.attributes.find(({ id }) => id === column);
    }
    getContentFromDataColumn(row, column): string {
        var partes = column.split(".");
        if (partes.length > 1) {
            var tmp = row;
            for (var i = 0; i < partes.length; i++) {
                tmp = tmp[partes[i]];

            }
            return tmp;
        }
        else
            return row[column];
    }

    getClassRowUtility(column){
        var cl = "fa " + this.getAllColumnAttributes(column).attributes.icon;
        if (this.getAllColumnAttributes(column).attributes.color){
            cl += " " + this.getAllColumnAttributes(column).attributes.color;
        }else{
            cl += " default-row-utility";
        }
        return cl;

    }
    getColumnType(_column): string {
        var column = _column.replace('-filter', '');
        if (column.startsWith('ROWUTILITY')) return 'ROWUTILITY';
        else if (column == 'EDIT') return 'EDIT';
        else if (column == 'DELETE') return 'DELETE';
        else if (column == 'SELECT') return 'SELECT';
        else if (column in this.table.checkboxColumns) return 'CHECKBOX';
        else {
            var col = this.table.attributes.find(({ id }) => id === _column);
            if (col && col.type) {
                return col.type;
            }
            else {
                return constants.BASIC_TABLE_TYPE_COLUMNS.DATA;
            }
        }




        /*if (column.startsWith('ROWUTILITY')) return 'ROWUTILITY';
        else if (column == 'EDIT') return 'EDIT';
        else if (column == 'DELETE') return 'DELETE';
        else if (column == 'SELECT') return 'SELECT';
        else if (column == 'OBSERVACIONES_ICON') return 'OBS';
        //else if (this.table.linkColumns.includes(column)) return 'LINK';
        //else if (this.table.colorFlagColumns.includes(column)) return 'COLOR_FLAG';
        else if (column in this.table.checkboxColumns) return 'CHECKBOX';
        else return 'DATA';
        */
    }
    isUtilityColumn(column) {
        var result = ['EDIT', 'DELETE', 'SELECT', 'ROWUTILITY', 'OBSERVACIONES_ICON'].includes(this.getColumnType(column));
        if (!result) result = column.endsWith('_ICON');
        return result;
    }

    isCheckboxChecked(row, column) {
        return this.table.checkboxColumns[column].checked(row[column]);
    }
    isCheckboxDisabled(row, column) {
        const config = this.table.checkboxColumns[column];
        if (typeof (config.disabled) == "function") {
            return config.disabled(row[column]);
        } else if (typeof (config.change) == "function") {
            return false;
        } else {
            return true;
        }
    }
    async onCheckboxChange(event, row, column) {
        event.preventDefault();
        const config = this.table.checkboxColumns[column];
        if (typeof (config.change) == "function") {
            config.change(!this.isCheckboxChecked(row, column), row)
                .then(() => { this._cd.markForCheck(); })
                .catch(() => { });
        }
    }
}

export interface DialogData {
    animal: string;
    name: string;
}
@Component({
    templateUrl: './basic.table.delete.dialog.html',
})
export class BasicTableDeleteDialog {
    constructor(public dialogRef: MatDialogRef<BasicTableDeleteDialog>) { }
    executeDelete(): void { this.dialogRef.close(true); }
    cancel(): void { this.dialogRef.close(false); }
}
