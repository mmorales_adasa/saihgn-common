import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import moment from 'moment';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable()
export class BasicTableExportService {
    constructor() { }
    public exportAsGenericExcelFile(json: any[], displayedColumns : string[], prefix : string, translations : any): void { 
        // Obtenemos cabecera
        var header = {};
        var ii = 0;
        for(var i in displayedColumns){
            if(['EDIT', 'DELETE', 'SELECT'].includes(displayedColumns[i])) continue;
            if(displayedColumns[i].startsWith('ROWUTILITY_')) continue;
            var desc = translations[displayedColumns[i]];
            if(!desc) desc = displayedColumns[i];
            header[ii] = desc;
            ii++; 
        }
        var mhArray = [];   
        var _json : any[] = [];
        json.forEach(function(element) {
            var object = {};
            var ii = 0;
            for(var i in displayedColumns) {
                if(['EDIT', 'DELETE', 'SELECT'].includes(displayedColumns[i])) continue;
                if(displayedColumns[i].startsWith('ROWUTILITY_')) continue;
                object[ii] = element[displayedColumns[i]];
                ii++;
            }
            _json.push(object);
        });
        var jsonWithHeaders = [].concat(mhArray.concat([header])).concat(_json);   
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonWithHeaders, {skipHeader: true});
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        const data: Blob = new Blob([excelBuffer], { type: EXCEL_TYPE });
        FileSaver.saveAs(data, 'arca_export_' + this.dateToString(new Date()) + EXCEL_EXTENSION);
    }
    private dateToString(date : Date){
        let m = moment(date);  
        return m.format('YYYYMMDDHHmmss'); 
    }
}