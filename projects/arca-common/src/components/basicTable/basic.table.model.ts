import { ChangeDetectorRef } from '@angular/core';
import { ListUtils } from '../../utils/list.utils';
import { MatTableDataSource, Sort } from '@angular/material';
import { MatPaginator } from '@angular/material';
import { BasicTableExportService } from './basic.table.export.service';
import { MultiselectModel } from '../multiselect/multiselect.model';
import { BasicCrudModel } from '../basicCrud/basic.crud.model';
import { MultiselectModelMV } from '../multiselect/multiselectMV.model';
import { Subject, Observable } from 'rxjs';

import { constants as conmmonConstants } from '../../config/constants';
//import { constants } from 'os';
//import { constants } from 'os';
export class BasicTableModel {
    public _cd: ChangeDetectorRef;
    public id: string;
    public superdata: any[] = [];
    public title: string = null;
    public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
    public attributes: any = [];
    public displayedAttributes: string[] = [];
    public displayedPageFilters: string[] = [];
    public displayedPageFiltersObjects: any = {};
    public tableIsLoading: boolean = false;
    public tableHasResults: boolean = false;
    public currentSort: Sort;
    public currentTextFilter: string = '';
    public textFilterTimeout: number = 500;
    public filterByGeneralTextTimeout = null;
    public filterByColumnTextTimeout = {};
    public _exportService: BasicTableExportService = new BasicTableExportService();;
    public displayedColumns: string[] = [];
    public displayedColumnsFilters: string[] = [];
    public displayedColumnsFiltersObjects: any = {};
    public lastDisplayedAttributeColumn: string = '';
    public draggedElement = { id: '', type: '' };
    public binDraggedOn = false;
    public tableColumnDraggedOn = null;
    public pagefilterDraggedOn = null;
    public pagefilterDraggedOn_side = null;
    public paginator: MatPaginator;
    public totalElements;
    //public externalFilters: string[] = [];
    //public externalFiltersObjects: {};
    //public linkColumns = [];
    //public colorFlagColumns = [];
    public checkboxColumns = {};
    public editable = false;
    public deletable = false;
    public insertable = false;
    public showHeaderFilters = true;
    public showExportExcel = true;
    public msgNoResultados: string;
    public msgNumResultados: string;
    public pkAttributes: string[] = [];
    public onClickColumns = [];
    tableUtilities: BasicTableUtility[] = [];
    tableRowUtilities: BasicTableUtility[] = [];
    tableRowUtilityObjects = {};
    public selectable = true;
    public selectableAll = true;
    public dragablecolumns = true;
    markDirtyRows: boolean = false;
    selectableUnique: boolean = false;
    selection: any[] = [];
    traslations: any = {};
    functionSearch: any;
    serverPagination: false;
    showMovementOptions: boolean = true;
    pageSize: number = 50;
    loadedConfiguration: boolean = false;
    crudConf: any = {};
    crudModel;
    public dataSourceChanged = new Subject<any>()
    // Generic table component. Desencadenante:
    private readonly afterLoadTableSubjectSource$ = new Subject<object>();


    // Constructor
    constructor(configuration: any) {
        this.serverPagination = configuration.serverPagination;
        this.id = configuration.id;
        for (var column of configuration.columns) {
            this.attributes.push({
                id: column.id,
                type: column.type ? column.type : conmmonConstants.BASIC_TABLE_TYPE_COLUMNS.DATA,
                typeSort: column.typeSort ? column.typeSort : conmmonConstants.TYPE_DATA_FILTER.TYPE_TEXT,
                displayed: column.displayed ? column.displayed : false,
                filter: column.filter ? column.filter : false,
                primaryKey: column.primaryKey ? column.primaryKey : false,
                attributes: column.attributes,
                tooltip : column.tooltip,
                icon : column.icon,                
            });




            if (column.filter) {
                this.displayedPageFilters.push(column.id);
            }
            if (column.displayed) {
                this.displayedAttributes.push(column.id);
            }
            if (column.primaryKey) {
                this.pkAttributes.push(column.id);
            }

            this.traslations[column.id] = column.translation ? column.translation : "";
            this.selectable = configuration.selectable ? configuration.selectable : false;
            this.selectableAll = configuration.selectableAll ? configuration.selectableAll : false;
            this.showHeaderFilters = configuration.showHeaderFilters ? configuration.showHeaderFilters : false;
            this.showMovementOptions = configuration.showMovementOptions ? configuration.showMovementOptions : false;
            this.showExportExcel = configuration.showExportExcel ? configuration.showExportExcel : true;
            this.title = configuration.title ? configuration.title : null;
            this.msgNoResultados = configuration.msgNoResultados ? configuration.msgNoResultados : "No se han encontrado resultados";
            this.msgNumResultados = configuration.msgNumResultados ? configuration.msgNumResultados : "resultados";
            this.pageSize = configuration.pageSize ? configuration.pageSize : 50;
            this.dragablecolumns = configuration.dragablecolumns ? configuration.dragablecolumns : true;
            this.markDirtyRows = configuration.markDirtyRows ? configuration.markDirtyRows : false;
            this.selectableUnique = configuration.selectableUnique ? configuration.selectableUnique : false;
            if (configuration.sort){
                this.currentSort = { active: configuration.sort.active, direction : configuration.sort.direction };
            }
            else{
                this.currentSort = { active: this.displayedAttributes[0], direction: 'asc' };
            }
            if (configuration.utilities) { this.tableUtilities = configuration.utilities ? configuration.utilities : [] };
            
            // CRUD configuration
            this.crudConf.editable  = configuration.crud && configuration.crud.editable ? configuration.crud.editable : false;
            this.crudConf.deletable = configuration.crud && configuration.crud.deletable ? configuration.crud.deletable : false;
            this.crudConf.insertable = configuration.crud && configuration.crud.insertable ? configuration.crud.insertable : false;
            if (configuration.crud && configuration.crud.fields) {
                this.crudModel = new BasicCrudModel({
                    title: configuration.crud && configuration.crud.title ? configuration.crud.title : "Alta / Edicción",                    
                    service: configuration.crud.service,
                    serviceParams: configuration.crud.serviceParams,
                    withPersistence : configuration.crud.service != null ? true  :  false,
                });

                for (var field of configuration.crud.fields) {                
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.TEXT_FIELD) {                        
                        this.crudModel.addTextField(field);
                    }
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.NUMBER_FIELD) {                        
                        this.crudModel.addNumberField(field);
                    }                    
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.TEXT_AREA) {
                        this.crudModel.addTextAreaField(field);
                    }                    
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.SINGLE_SELECT) {
                        this.crudModel.addSelectField(field);
                    }
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.DATE_SELECT) {
                        this.crudModel.addDateField(field);
                    }
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT) {
                        this.crudModel.addCustomAssistant(field); 
                    }                    
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.AUTO_COMPLETE_VIRTUAL) {
                        this.crudModel.addAutoCompleteVirtual(field); 
                    }                    
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT) {
                        this.crudModel.addSelectServerPagination(field); 
                    }     
                    if (field.type == conmmonConstants.TYPE_COMPONENT_FILTER.HIDDEN) {
                        this.crudModel.addHiddenField(field); 
                    }                             

                }
            }


            for (var i in this.displayedPageFilters) {
                this.displayedPageFiltersObjects[this.displayedPageFilters[i]] = new BasicTablePageFilterModel(this.displayedPageFilters[i]);
            }
            this.loadedConfiguration = true;
        }
    }
    public setValueCrud(field : any, value : any) {        
        this.crudModel.setValue(field, value);
    }
    public setDataSourceChanged(data: any) {
        this.dataSourceChanged.next(data);
    }
    public getDataSourceChanged(): Observable<any> {
        return this.dataSourceChanged.asObservable();
    }
    public startLoading() {
        this.tableIsLoading = true;
    }
    public stopLoading() {
        this.tableIsLoading = false;
    }
    public setTotalElements(totalElements) {
        this.totalElements = totalElements;
    }
    public setDataAndLoadTable(data: any[]) {
        this.superdata = data;
        this.currentTextFilter = '';
        
        this.buildTable();
    }
    public buildTable() {
        var data = [].concat(this.superdata);
        this.loadDisplayedColumns();
        //data = this.filterTableByExternalFilters(data);
        data = this.filterTableByExternalText(data);
        this.tableHasResults = (data.length > 0);
        data = this.filterTableByPageFilters(data);
        data = this.filterTableByColumnFilters(data);
        data = this.sortTable(data);

        if (!this.serverPagination) {
            // this.dataSource = new MatTableDataSource(data);
            this.totalElements = data.length;
            this.setDataSourceChanged(data);
        }
        else {
            //this.dataSource = new MatTableDataSource(data);
            this.setDataSourceChanged(data);
        }

        var __cd = this._cd;
        setTimeout(() => {
            if (this._cd && __cd)
                __cd.markForCheck();
           // this.tableIsLoading = false;
        }, 10)
    }
    private loadDisplayedColumns() {
        this.displayedColumns = [].concat(this.displayedAttributes);
        this.displayedColumnsFilters = [];
        var displayedColumnsFiltersObjects_aux = {};
        for (var rowUtility of this.tableRowUtilities) {
            var columnId = 'ROWUTILITY_' + rowUtility.id;
            this.displayedColumns = [columnId].concat(this.displayedColumns);
            this.tableRowUtilityObjects[columnId] = rowUtility;
        }
        if (this.crudConf.editable) this.displayedColumns = ['EDIT'].concat(this.displayedColumns);
        if (this.crudConf.deletable) this.displayedColumns = ['DELETE'].concat(this.displayedColumns);
        if (this.selectable) this.displayedColumns = ['SELECT'].concat(this.displayedColumns);
        // Añadimos ids para los filtros de columna
        for (var i in this.displayedColumns) {
            var id = this.displayedColumns[i];
            var filterId = id + '-filter';
            var movementId = id + '-movement';
            this.displayedColumnsFilters.push(filterId);
            if (this.displayedColumnsFiltersObjects.hasOwnProperty(id)) displayedColumnsFiltersObjects_aux[id] = this.displayedColumnsFiltersObjects[id];
            else displayedColumnsFiltersObjects_aux[id] = new BasicTableColumnFilterModel(id);
        }
        this.displayedColumnsFiltersObjects = displayedColumnsFiltersObjects_aux;
        this.lastDisplayedAttributeColumn = this.displayedAttributes[this.displayedAttributes.length - 1];
        // External filters
        /*
        var externalFiltersObjects_aux = {};
        for (var i in this.externalFilters) {
            var id = this.externalFilters[i];
            if (this.externalFiltersObjects.hasOwnProperty(id)) externalFiltersObjects_aux[id] = this.externalFiltersObjects[id];
            else externalFiltersObjects_aux[id] = new BasicTablePageFilterModel(id);
        }*/
        //this.externalFiltersObjects = externalFiltersObjects_aux;
        // Page filters
        var displayedPageFiltersObjects_aux = {};
        for (var i in this.displayedPageFilters) {
            var id = this.displayedPageFilters[i];
            if (this.displayedPageFiltersObjects.hasOwnProperty(id)) displayedPageFiltersObjects_aux[id] = this.displayedPageFiltersObjects[id];
            else displayedPageFiltersObjects_aux[id] = new BasicTablePageFilterModel(id);
        }
        this.displayedPageFiltersObjects = displayedPageFiltersObjects_aux;
        // Current sort
        if (!this.displayedColumns.includes(this.currentSort.active)) {
            this.currentSort.active = this.displayedColumns[0];
            this.currentSort.direction = 'asc';
        }
    }
    private filterTableByExternalText(data: any[]): any[] {
        if (this.currentTextFilter) {
            var dataSourceAux = new MatTableDataSource(data);
            dataSourceAux.filter = this.currentTextFilter.trim().toLowerCase();
            data = dataSourceAux.filteredData;
        }
        return data;
    }
    /*
    private filterTableByExternalFilters(data: any[]): any[] {
        // Page filters
        for (var i in this.externalFilters) {
            var pageFilter = this.externalFiltersObjects[this.externalFilters[i]];
            // Actualizamos filtro
            pageFilter.load(data, !!this.checkboxColumns[this.externalFilters[i]]);
            // Actualizamos tabla
            data = ListUtils.filterObjectList(data, pageFilter.attribute, [pageFilter.selectedItem]);
        }
        return data;
    }*/
    private filterTableByPageFilters(data: any[]): any[] {
        // Page filters
        for (var i in this.displayedPageFilters) {
            var pageFilter = this.displayedPageFiltersObjects[this.displayedPageFilters[i]];
            // Actualizamos filtro
            pageFilter.load(data, !!this.checkboxColumns[this.displayedPageFilters[i]]);
            // Actualizamos tabla
            var selectedElements = ListUtils.possibleObjectListToKeyList(pageFilter.msModel.selectedElements, pageFilter.msModel.cod);
            data = ListUtils.filterObjectList(data, pageFilter.attribute, selectedElements);
        }
        return data;
    }
    private filterTableByColumnFilters(data: any[]): any[] {
        // Column filters
        for (var i in this.displayedColumns) {
            var columnFilter = this.displayedColumnsFiltersObjects[this.displayedColumns[i]];
            // Actualizamos tabla
            if (columnFilter.value) {
                data = ListUtils.filterObjectListLike(data, columnFilter.attribute, columnFilter.value);
            }
        }
        return data;
    }
    sortObjectList(data: any[], sortElement: string, direction: string, type: any) {
        
        return data.sort((a, b) => {
            const isAsc = direction === 'asc';
            var aa, bb;
            if (type === conmmonConstants.TYPE_DATA_FILTER.TYPE_NUMBER) {
                aa = parseFloat(a[sortElement]);
                bb = parseFloat(b[sortElement]);
            }
            else if (type === conmmonConstants.TYPE_DATA_FILTER.TYPE_INTEGER) {
                aa = parseInt(a[sortElement]);
                bb = parseInt(b[sortElement]);
            }
            else {
                aa = a[sortElement].toUpperCase().trim()
                bb = b[sortElement].toUpperCase().trim();
            }
            return ListUtils.compare(aa, bb, isAsc);
        });
    }
    private sortTable(data: any[]): any[] {
        if (this.currentSort.active) {
            var typeSort = conmmonConstants.TYPE_DATA_FILTER.TYPE_TEXT;
            for (var item of this.attributes){

                if (item.id===this.currentSort.active){
                    typeSort = item.typeSort ? item.typeSort : conmmonConstants.TYPE_DATA_FILTER.TYPE_TEXT;
                }
            }
            data = this.sortObjectList(data, this.currentSort.active, this.currentSort.direction, typeSort);
        }
        return data;
    }
    public sortData(sort: Sort) {
        this.currentSort = sort;
        this.buildTable();
    }    
    filterByColumnText(text: string, attribute: string) {
        if (this.textFilterTimeout <= 0) {
            this.displayedColumnsFiltersObjects[attribute].value = text;
            this.buildTable();
        } else {
            this.filterByColumnTextWithTimeout(text, attribute);
        }
    }
    async filterByColumnTextWithTimeout(text: string, attribute: string) {
        const self = this;
        await this.waitWhileLoading();
        if (typeof (this.filterByColumnTextTimeout[attribute]) != "undefined") {
            clearTimeout(this.filterByColumnTextTimeout[attribute]);
        }
        self.displayedColumnsFiltersObjects[attribute].value = text;
        this.filterByColumnTextTimeout[attribute] = setTimeout(
            function () {
                self.buildTable();
                delete self.filterByColumnTextTimeout[attribute];
            },
            this.textFilterTimeout
        );
    }
    async waitWhileLoading() {
        while (this.tableIsLoading) {
            await new Promise((resolve, reject) => {
                setTimeout(() => resolve(), 100);
            });
        }
    }
    applyFilter() {
        //this.paginator.pageIndex = 0;
        for (let key in this.displayedPageFiltersObjects) {
            this.displayedPageFiltersObjects[key].dirty = false;
        }
        this.buildTable();
    }
    removeFilter(idFilter) {
        this.displayedPageFilters = this.removeElementInArray(this.displayedPageFilters, idFilter);
        this.buildTable();
    }
    getStyleFilterApply(idFilter) {
        if (this.displayedPageFiltersObjects[idFilter].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    filterByPageFilter(selectAll: boolean, deselectAll: boolean, attribute: string) {
        this.displayedPageFiltersObjects[attribute].dirty = true;
        if (selectAll) this.displayedPageFiltersObjects[attribute].msModel.forceSelectAll();
        else if (deselectAll) this.displayedPageFiltersObjects[attribute].msModel.forceDeselectAll();
    }
    public getColumnTranslation(translation) {
        if (this.traslations) {
            return this.traslations[translation];
        }
        else {
            return "";
        }
    }
    public exportTableAsExcel() {
        var prefix = this.id + '.';
        this._exportService.exportAsGenericExcelFile(this.dataSource.data, this.displayedColumns, prefix, this.traslations);
    }
    public dragEnd(type, id) {
    }
    public dragStart(type, id) {
        this.draggedElement = { id: id, type: type };
    }
    drop_pf(targetId, pos) {
        this.pagefilterDraggedOn = null;
        if (this.attributes.find(({ id }) => id === this.draggedElement.id)) {
            var index = this.displayedPageFilters.indexOf(targetId) + pos;
            this.displayedPageFilters = this.insertElementInArray(this.displayedPageFilters, this.draggedElement.id, (index == -1) ? 0 : index);
            this.buildTable();
        }
    }
    drop_tc(targetId) {
        this.tableColumnDraggedOn = null;
        if (this.attributes.find(({ id }) => id === this.draggedElement.id)) {
            var index = this.displayedAttributes.indexOf(targetId);
            this.displayedAttributes = this.insertElementInArray(this.displayedAttributes, this.draggedElement.id, index);
            this.buildTable();
        }
    }
    drop_bin() {
        this.binDraggedOn = false;
        if (this.draggedElement.type == 'pf') {
            this.displayedPageFilters = this.removeElementInArray(this.displayedPageFilters, this.draggedElement.id);
            this.buildTable();
        } else if (this.draggedElement.type == 'tc') {
            if (this.displayedAttributes.includes(this.draggedElement.id) && (this.displayedColumns.length > 1))
                this.displayedAttributes = this.removeElementInArray(this.displayedAttributes, this.draggedElement.id);
            this.buildTable();
        }
    }
    filterByGeneralText(filterValue: string) {
        if (this.textFilterTimeout <= 0) {
            this.currentTextFilter = filterValue;
            this.buildTable();
        } else {
            this.filterByGeneralTextWithTimeout(filterValue);
        }
    }
    async filterByGeneralTextWithTimeout(filterValue: string) {
        const self = this;
        await this.waitWhileLoading();
        if (this.filterByGeneralTextTimeout !== null) {
            clearTimeout(this.filterByGeneralTextTimeout);
        }
        self.currentTextFilter = filterValue;
        this.filterByGeneralTextTimeout = setTimeout(
            function () {
                self.buildTable();
                self.filterByGeneralTextTimeout = null;
            },
            this.textFilterTimeout
        );
    }
    private insertElementInArray(array: string[], elementId: string, index: number): string[] {
        var result = [];
        for (var i = 0; i < array.length; i++) {
            if (i == index) result.push(elementId);
            if (array[i] != elementId) result.push(array[i]);
        }
        if (index == array.length) result.push(elementId);
        return result;
    }
    private removeElementInArray(array: string[], elementId: string) {
        return this.insertElementInArray(array, elementId, -1);
    }
    selectRow(selected, selectedRow) {
        if (selected) {
            if (!this.selection.find(row => this.rowEquals(row, selectedRow))) {
                this.selection.push(selectedRow);
            }
        } else {
            const index = this.selection.findIndex(row => this.rowEquals(row, selectedRow));
            if (index != -1) {
                this.selection.splice(index, 1);
            }
        }
    }
    isSelectedRow(row) {
        const index = this.selection.findIndex(r => this.rowEquals(r, row));
        return index != -1;
    }
    selectAll(selected) {
        this.selection.splice(0, this.selection.length);
        if (selected) {
            for (let row of this.dataSource.data) {
                this.selection.push(row);
            }
        }
    }
    getRecordsPendingSave() {
        return this.dataSource.data.filter(({ dirty }) => dirty === true).length;
    }
    isSelectedAll() {
        return this.selection.length == this.dataSource.data.length;
    }
    columnHasFilter(column) {
        var has = false;
        for (var filter of this.displayedColumnsFilters) {
            if (filter == column + "-filter") {
                has = true;
            }
        }
        return has;
    }
    rowEquals(row1, row2) {
        for (const k in row1) {
            if (row1[k] !== row2[k]) {
                return false;
            }
        }
        for (const k in row2) {
            if (row1[k] != row2[k]) {
                return false;
            }
        }
        return true;
    }
    getSelection() {
        return this.selection.slice();
    }
}
export class BasicTablePageFilterModel {
    public attribute: string;
    public msModel: MultiselectModel;
    public msModelMV: MultiselectModelMV;
    public draggedOn: boolean;
    public dirty: boolean = false;
    // Filtros externos
    public keyList: string[];
    public selectedItem: string;
    constructor(
        attribute: string,
    ) {
        this.attribute = attribute;
        this.draggedOn = false;
        this.msModel = new MultiselectModel('cod', 'desc', true);
        this.msModelMV = new MultiselectModelMV('cod', 'desc', true);
        this.keyList = [];
        this.selectedItem = '';
    }
    public load(data: any[], isCheckbox: true) {
        var elements = ListUtils.getMasterComboFromObjectList(data, this.attribute).multiselectList;
        if (isCheckbox) {
            for (let element of elements) {
                if (element.cod === 0 || element.cod === "0") {
                    element.desc = "No";
                } else if (element.cod === 1 || element.cod === "1") {
                    element.desc = "Sí";
                }
            }
        }
        this.msModel.setElements(elements);
        // Para filtros externos
        this.keyList = ListUtils.sortStringList(this.keyList, 'asc');
        if (this.selectedItem && !this.keyList.includes(this.selectedItem)) this.selectedItem = null;
        if (!this.selectedItem && (this.keyList.length > 0)) this.selectedItem = this.keyList[0];
    }
}
export class BasicTableColumnFilterModel {
    public attribute: string;
    public value: string;
    constructor(
        attribute: string,
    ) {
        this.attribute = attribute;
        this.value = '';
    }
}
export class BasicTableUtility {
    id: string;
    tooltip: Function;
    faIcon: Function;
    innerHtml: string;
    innerHtmlFunction: Function;
    onContextMenu: Function;
    constructor(configuration: any) {
        this.id = configuration.id;
        this.tooltip = typeof (configuration.tooltip) == "function" ? configuration.tooltip : (() => configuration.tooltip);
        this.faIcon = typeof (configuration.faIcon) == "function" ? configuration.faIcon : (() => configuration.faIcon);
        this.innerHtml = configuration.innerHtml;
        this.onContextMenu = configuration.onContextMenu;
        this.innerHtmlFunction = function () { return null };
    }
}
