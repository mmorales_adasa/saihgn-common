import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuditoriaService } from './../../services/auditoria.service';
import { Router } from '@angular/router';
@Component({
  selector: 'arca-common-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() sections: [];
  profile: any;
  @Output() seleccionar = new EventEmitter<string>();

  constructor(public _auditoriaService: AuditoriaService,
    private _router: Router) { }
  ngOnInit() {
    this.profile = this._auditoriaService.getUserProfile();
  }
  getClassSelected(item) {
    if (item.selected) {
      return item.class + " mainMenuOptionHighlighted ";
    }
    return item.class;
  }
  getClassDisabled(item) {
    return item.class + " mainMenuOptionDisabled ";

  }
  clickOnMainMenuOption(option) {

    this.seleccionar.emit(option);
    //this._router.navigate([option]);
  }
}
