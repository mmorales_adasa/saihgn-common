import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuditoriaService } from  './../../services/auditoria.service';
import { Router } from '@angular/router';
@Component({
  selector: 'arca-common-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input()  title:   string; 
  @Input()  profile: any;
  @Input()  version: string;
  @Input()  description: string;
  @Output() logoutClick = new EventEmitter<string>();
  constructor(public _auditoriaService: AuditoriaService, private _router: Router) { }
  ngOnInit() {
    //this.profile = this._auditoriaService.getUserProfile();
  }
  logout() {
    this.logoutClick.emit();
}  
}
 