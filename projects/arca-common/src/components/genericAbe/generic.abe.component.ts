import { Component, OnInit, Input, OnDestroy, Inject, OnChanges, SimpleChanges, HostListener, ElementRef, } from '@angular/core';
import { GenericTableCommonService, OpenGenericAbeFormObject } from '../../services/generic.table.common.service';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material';
import { GenericAbe } from './generic.abe.model';
//import { AuditoriaConstants, AuditoriaService } from '../../services/auditoria.service';
import { AuditoriaService } from './../../services/auditoria.service';
import { CrudService } from './../../services/crud.service';
import { AuditoriaConstants } from './../../services/auditoria.service';
@Component({
    selector: 'app-generic-abe',
    templateUrl: './generic.abe.component.html',
    styleUrls: ['./generic.abe.component.scss'],
    providers: [CrudService]
})

export class GenericAbeComponent implements OnInit, OnDestroy {
    @Input() abeConfig: GenericAbe;
    openAeFormSubscription: Subscription;
    boxOpened = false;
    updateError = '';
    mode = '';
    method = '';
    snackBarMessage = '';
    accion = '';
    model: any;
    loading = false;
    asleep = true; // Útil para no mostrar nunca antes de la primera vez que se abra
    constructor(
        private _genericTableCommonService: GenericTableCommonService,
        private _crudService: CrudService,
        private _auditoriaService: AuditoriaService,
        private snackBar: MatSnackBar,
        public dialog: MatDialog,
        private eRef: ElementRef,
    ) { }
    ngOnInit() {
        this.openAeFormSubscription = this._genericTableCommonService.openGenericAbeFormNewCall.subscribe(async (data: OpenGenericAbeFormObject) => {
            if (this.abeConfig.id == data.id) {
                this.asleep = false;
                if (this.boxOpened) {
                    this.boxOpened = false;
                    await this.sleep(500); // 'transition' está configurado en el css como 0.3s
                }
                // Asignamos valores a campos
                this.mode = data.mode;
                this.model = data.model;
                for (var elementId in this.abeConfig.elementMap) {
                    this.abeConfig.elementMap[elementId].validationError = '';
                    this.abeConfig.elementMap[elementId].setValue((['E', 'D'].includes(this.mode)) ? data.model[elementId] : this.abeConfig.elementMap[elementId].defaultValue);
                }
                // Abrimos formulario
                if (['I', 'E'].includes(this.mode)) this.boxOpened = true;
                if ('D' == this.mode) this.openDeleteConfirm();
            }
        });
    }
    ngOnDestroy() {
        if (this.openAeFormSubscription) {
            this.openAeFormSubscription.unsubscribe();
        }
    }
    @HostListener('document:click', ['$event'])
    clickout(event) {
        // Consideramos cualquier click externo que no venga de un icono awesome,
        // ni de un datepicker
        if (!event.srcElement.classList.contains('fa')
            && (event.srcElement.className.indexOf('mat-calendar') == -1)
            && (event.srcElement.className.indexOf('mat-button') == -1)
            && (event.srcElement.className.indexOf('cdk-overlay-backdrop') == -1)
            && event.path[2].className.indexOf('mat-calendar') == -1) {
            if (!this.eRef.nativeElement.contains(event.target)) {
                this.boxOpened = false;
            }
        }
    }
    toggleBox() {
        this.boxOpened = !this.boxOpened;
    }
    save() {
        // Obtenemos validaciones
        this.updateError = '';
        var validationErrors = (this.mode == 'D') ? false : this.abeConfig.getValidationErrors();
        if (!validationErrors) {
            this.loading = true;
            this.loadOptions();
            var model = this.abeConfig.getModel()
            var params = { entity: this.abeConfig.entity, model: model };
            var service = null;
            this._crudService.setBaseUrl(this .abeConfig.crudServiceURL);            
            service = this._crudService[this.method](params).subscribe(
                (data: any[]) => {
                    this.loading = false;
                    if (data) {
                        var o = new OpenGenericAbeFormObject(this.abeConfig.id, this.mode, model);
                        this.boxOpened = false;
                        this._genericTableCommonService.afterUpdateFromAbeForm(o);
                        this.openSnackBar(this.snackBarMessage, true);
                    } else {
                        this.updateError = 'Se ha producido un error';
                        this.openSnackBar('Se ha producido un error', false);
                    }
                }
            );
            // Auditoría
            var obj2 = (this.mode == 'E') ? this.model : null;
        }
    }
    openDeleteConfirm() {
        const dialogRef = this.dialog.open(GenericAbeDeleteDialog, { width: '400px' });
        dialogRef.afterClosed().subscribe(result => { if (result) this.save(); });
    }
    cancel() {
        this.boxOpened = false;
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    openSnackBar(message, ok) {
        let config = new MatSnackBarConfig();
        config.duration = 1000;
        config.panelClass = ['snackBar', ok ? 'snackBar-ok' : 'snackBar-ko'];
        this.snackBar.open(message, undefined, config);
    }
    loadOptions() {
        if (this.mode == 'I') this.method = 'insert';
        if (this.mode == 'E') this.method = 'update';
        if (this.mode == 'D') this.method = 'delete';
        /*
        if (this.mode == 'I' && this.abeConfig.serviceInsertMethod) this.method = this.abeConfig.serviceInsertMethod;
        if (this.mode == 'E' && this.abeConfig.serviceUpdateMethod) this.method = this.abeConfig.serviceUpdateMethod;
        if (this.mode == 'D' && this.abeConfig.serviceDeleteMethod) this.method = this.abeConfig.serviceDeleteMethod;
        */
        if (this.mode == 'I') this.snackBarMessage = 'Registro añadido de forma correcta';
        if (this.mode == 'E') this.snackBarMessage = 'Registro actualizado de forma correcta';
        if (this.mode == 'D') this.snackBarMessage = 'Registro eliminado de forma correcta';
        if (this.mode == 'I') this.accion = AuditoriaConstants.INSERT;
        if (this.mode == 'E') this.accion = AuditoriaConstants.UPDATE;
        if (this.mode == 'D') this.accion = AuditoriaConstants.DELETE;
    }
    getDisabled(elementId) {
        return ((this.mode == 'I') && this.abeConfig.elementMap[elementId].insertDisabled)
            || ((this.mode == 'E') && this.abeConfig.elementMap[elementId].pk);
    }


}



export interface DialogData {
    animal: string;
    name: string;
}

@Component({
    templateUrl: 'generic.abe.delete.dialog.html',
})
export class GenericAbeDeleteDialog {
    constructor(public dialogRef: MatDialogRef<GenericAbeDeleteDialog>) { }
    confirm(): void { this.dialogRef.close(true); }
    cancel(): void { this.dialogRef.close(false); }
}
