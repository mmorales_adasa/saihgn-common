import moment from 'moment';
import { MultiselectModel } from '../multiselect/multiselect.model';


export class GenericAbe {
    id: string;
    entity: string;
    COD_PERMISO: string;
    label: string;
    rowArray: string[] = [];
    rowElementMap: any;
    elementMap: any;
    serviceUpdateMethod: string;
    serviceInsertMethod: string;
    serviceDeleteMethod: string;
    crudServiceURL: any;
    // Date validation/invalidation
    dateValidation: boolean = false;
    dateValidation_iniDateId: string;
    dateValidation_endDateId: string;
    dateValidation_elements: string[] = [];
    dateInvalidationIsActive: boolean = false;
    constructor(id: string, label: string, entity: string, COD_PERMISO: string, crudServiceURL: any = null) {
        this.id = id;
        this.label = label;
        this.entity = entity;
        this.COD_PERMISO = COD_PERMISO;
        this.rowElementMap = {};
        this.crudServiceURL = crudServiceURL;
        this.elementMap = {};
    }
    addFieldToRowFilterMap(row: number, id: string) {
        var srow = '' + row;
        if (!this.rowArray.includes(srow)) this.rowArray.push(srow);
        if (!this.rowElementMap.hasOwnProperty(srow)) this.rowElementMap[srow] = [];
        this.rowElementMap[srow].push(id);
    }
    addHiddenField(id: string, pk: boolean) {
        var filter = new GenericAeFormElement(id, pk, false, '', 12, 'TX', null, null, null);
        filter.hidden = true;
        this.elementMap[id] = filter;
    }
    addTextField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, maxLength: number, defaultValue: any) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'TX', null, maxLength, defaultValue);
        this.elementMap[id] = filter;
    }
    addAutocompleteField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, maxLength: number, defaultValue: any, options: [{COD: string, DESCR: string}]){
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'AC', null, maxLength, defaultValue);
        filter.autocompleteOptions = options;
        this.elementMap[id] = filter;
    }

    addMultiTextField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, maxLength: number, defaultValue: any, options: string[]) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'MT', null, maxLength, defaultValue);
        filter.autocompleteOptions = options;
        this.elementMap[id] = filter;
    }
    addMultiSelectField(row : number, id : string, pk : boolean, mandatory : boolean, label : string, bsWidth : number, searchable: boolean, data : any[], defaultValue : any){
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'MS', data, null, defaultValue);
        filter.msModel = new MultiselectModel("COD", "DESCR", searchable);
        filter.msModel.setElements(data);
        this.elementMap[id] = filter;
    }

    addCheckbox(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, defaultValue: any){
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'CK', null, null, defaultValue);
        this.elementMap[id] = filter;
    }
  
    addTextAreaField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, maxLength: number, defaultValue: any) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'TA', null, maxLength, defaultValue);
        this.elementMap[id] = filter;
    }
    addDateField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, defaultValue: any) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'DT', null, null, defaultValue);
        this.elementMap[id] = filter;
    }
    addSelectField(row: number, id: string, pk: boolean, mandatory: boolean, label: string, bsWidth: number, data: any[], defaultValue: any) {
        this.addFieldToRowFilterMap(row, id);
        var filter = new GenericAeFormElement(id, pk, mandatory, label, bsWidth, 'SS', data, null, defaultValue);
        this.elementMap[id] = filter;
    }
    setDisabledAtInsertion(id) {
        this.elementMap[id].insertDisabled = true;
    }
    // Validación de campos
    addIntegerValidation(elementId) {
        this.elementMap[elementId].addIntegerValidation();
    }
    addFloatValidation(elementId) {
        this.elementMap[elementId].addFloatValidation();
    }
    setDependency(parentId, childId) {
        this.elementMap[parentId].dependencies.push(childId);
    }

    getValidationErrors(): boolean {
        var result = false;
        for (var elementId in this.elementMap) {
            var validationError = this.elementMap[elementId].loadValidations();
            result = result || validationError;
        }
        return result;
    }
    // Obtención de modelo
    getModel(): any {
        var result = {};
        for (var elementId in this.elementMap) {
            result[elementId] = this.elementMap[elementId].getValue();
        }
        return result;
    }
}
export class GenericAeFormElement{

    // Types
    TYPE_SS = 'SS'; // Select
    TYPE_MS = 'MS'; // Multiple Select
    TYPE_DT = 'DT'; // Date
    TYPE_TX = 'TX'; // Input text
    TYPE_TA = 'TA'; // Textarea
    TYPE_MT = 'MT'; // Multiple Text
    TYPE_AC = 'AC'; // Autocomplete

    // Validations
    VALIDATE_MADATORY = 'NN';
    VALIDATE_MAXLENGTH = 'ML';
    VALIDATE_INTEGER = 'I';
    VALIDATE_FLOAT = 'FL';
    VALIDATE_AUTOCOMPLETE = 'AC';
    validations = [];

    hidden : boolean = false;

    id : string;
    label : string;
    bsWidth : number;
    type : string;
    superdata : any[];
    data : any[];
    pk : boolean;
    dependencies: string[] = [];
    msModel: MultiselectModel;
    autocompleteOptions: any[] = null;

    // Validaciones
    mandatory : boolean;
    maxLength : number;
    validationError = '';

    value : any;
    defaultValue : any;
    insertDisabled = false;


    constructor(id, pk, mandatory, label, bsWidth, type, superdata, maxLength, defaultValue : any){

        this.hidden = false;

        this.id = id;
        this.pk = pk;
        this.mandatory = mandatory;
        this.label = label;
        this.bsWidth = bsWidth;
        this.type = type;
        this.superdata = superdata;
        this.data = superdata;
        this.maxLength = maxLength;

        if(this.type == this.TYPE_SS){
            this.value = null;
        }

        if(this.mandatory) this.validations.push(this.VALIDATE_MADATORY);
        if(this.maxLength) this.validations.push(this.VALIDATE_MAXLENGTH);
        if (this.type == this.TYPE_AC) {
            this.validations.push(this.VALIDATE_AUTOCOMPLETE);
        }
        this.validationError = '';

        this.defaultValue = defaultValue;
        
    }

    getValue() : any {
        if (this.type == this.TYPE_DT) {
            if (!this.value) {
                return null;
            } else {
                return this.value.format('DD/MM/YYYY');
            }
        } else if (this.type == this.TYPE_MS) {
            return this.msModel.getSelectedElements();
        } else {
            return this.value;
        }
    }

    setValue(value) {
        if (value === null || value === "") {
            this.value = null;
        } else {
            if (this.type == this.TYPE_DT) {
                this.value = moment(value, "DD/MM/YYYY");
            } else if (this.type == this.TYPE_MS) {
                this.msModel.selectedElements = value;
            } else if (this.type == this.TYPE_MT) {
                this.value = value.slice(0, value.length);
            } else {
                this.value = value
            }
        }
    }

    // Validaciones

    addIntegerValidation(){
        this.validations.push(this.VALIDATE_INTEGER);
    }

    addFloatValidation(){
        this.validations.push(this.VALIDATE_FLOAT);
    }


    // Resultados de validaciones

    loadValidations() : boolean {
        var result = false;
        for (var validation of this.validations) {
            if (validation == this.VALIDATE_MADATORY) {
                result = this.loadMandatoryValidation();
            } else if (validation == this.VALIDATE_MAXLENGTH) {
                result = this.loadMaxLengthValidation();
            } else if (validation == this.VALIDATE_INTEGER) {
                result = this.loadIntegerValidation();
            } else if (validation == this.VALIDATE_FLOAT) {
                result = this.loadFloatValidation();
            } else if (validation == this.VALIDATE_AUTOCOMPLETE) {
                result = this.loadAutocompleteValidation();
            }
            if (result) {
                break;
            }
        }
        return result;
    }

    loadMandatoryValidation() : boolean {
        var notValid = (!this.getValue() && this.getValue() !== false) && !(this.getValue() === 0);
        if(!notValid){
            if (typeof this.getValue() === 'string' || this.getValue() instanceof String) notValid = !this.getValue().trim()
        }
        this.validationError = notValid ? 'Campo obligatorio' : '';
        return notValid;
    }

    loadMaxLengthValidation() : boolean {
        var result = this.getValue() 
            && (this.maxLength) 
            && (this.maxLength > 0)
            && (this.getValue().length > this.maxLength);
        this.validationError = result ? 'Longitud máxima (' + this.maxLength + ') superada' : '';
        return result;
    }

    loadIntegerValidation() : boolean {
        var value = this.getValue();
        var result = false;
        if(value) result = !Number.isInteger(Number(value));        
        this.validationError = result ? 'El campo debe ser un número entero' : '';
        return result;
    }

    loadFloatValidation() : boolean {
        var value = this.getValue();
        var result = false;
        if(value) result = Number.isNaN(Number.parseFloat(value));        
        this.validationError = result ? 'El campo debe ser un número float' : '';
        return result;
    }

    loadAutocompleteValidation() : boolean {
        const value = this.getValue();
        const result = !!value && !this.autocompleteOptions.some(option => option.COD == value);
        this.validationError = result ? 'El valor no es un valor permitido' : '';
        return result;
    }

    // Autocompletar

    filterOptions() : any {
        const value = this.getValue();
        if (!value) {
            return [];
        }
        const search = value.toLocaleUpperCase();
        if (search.length < 3) {
            // Si el valor es muy pequeño buscamos coincidencia exacta
            return this.autocompleteOptions.filter(option => option.COD.toLocaleUpperCase() == search);
        } else {
            // Si no, buscamos por "palabras"
            const words = search.split(/[ \t]+/);
            return this.autocompleteOptions.filter(option => {
                for (let word of words) {
                    if (option.DESCR.toLocaleUpperCase().indexOf(word) == -1) {
                        return false;
                    }
                }
                return true;
            });
        }
    }

}