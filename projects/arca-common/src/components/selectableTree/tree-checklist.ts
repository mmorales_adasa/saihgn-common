import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { forEach } from '@angular/router/src/utils/collection';
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  code : string;
}
@Component({
  selector: 'tree-checklist',
  templateUrl: 'tree-checklist.html',
  styleUrls: ['tree-checklist.css'],

})
export class TreeChecklist implements OnInit {
 
  @Input() data: any;
  @Input() conf: any;
  @Output() onChangeSelection = new EventEmitter<any>();
  private _transformer = (node: any, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      code: node.cod,
      attributes: node.attributes,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
  }
  ngOnInit() {
    this.dataSource.data = this.data;
  }
  clickNode(node){
    this.onChangeSelection.emit(node);
  }
  checkVariable(node) {    
    var tmp = !node.selected;
    node.selected = tmp;
    this.onChangeSelection.emit(node);
  }

  setNodeToValue(node, value) {    
    node.selected = value;
    this.onChangeSelection.emit(node);
  }

  checkStation(node){
    var checked = node.selected;    
    var index = this.data.findIndex(x => x.cod === node.code);    
    for(var i=0;i<this.data[index].children.length;i++){
        var cod = this.data[index].children[i].cod;
        var indexChild = this.treeControl.dataNodes.findIndex(x => x.code === cod);
        this.setNodeToValue(this.treeControl.dataNodes[indexChild], checked);
    }
  }
  uncheckAll() {
    if (this.conf.singleSelect) {
      

    }
  }
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
}
