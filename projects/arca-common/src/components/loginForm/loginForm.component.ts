import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuditoriaService, AuditoriaConstants } from './../../services/auditoria.service';
import { Router } from '@angular/router';
@Component({
  selector: 'arca-common-login-form',
  templateUrl: './loginForm.component.html',
  styleUrls: ['./loginForm.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() onSubmit = new EventEmitter<string>();
  @Input() codeApp = false;
  public userNok = false;
  public codUsuario: string;
  public password: string;
  public userLoading = false;
  public token = null;

  constructor(public _auditoriaService: AuditoriaService, private _router: Router) { }
  ngOnInit() {
     this._auditoriaService.removeProfile();
  }
  onKeydown(event) {
    if (event.key === "Enter") this.login();
  }
  login() {
    this.userLoading = true;
    this.userNok = false;
    var consultaModel = { COD_USUARIO: this.codUsuario, PASSWORD: this.password, COD_APP: this.codeApp };
    this._auditoriaService.login(consultaModel).subscribe(
      (data: any) => {
        /*
        if (!data['userOk']) {
          this.userNok = !data['userOk'];
          this.userLoading = false;
        } else {
          
              this._auditoriaService.setUserProfile({
                codUsuario: this.codUsuario,
                permisos: permisos,
                username: data['NOMBRE'],
                token: data["token"],
                codApp: this.codeApp
              });
              this.onSubmit.emit("ok");
            
        }*/
        this._auditoriaService.setUserProfile({
          code: data.code,
          name: data.name,
          token: data.token,
          permisos : []
        });        
        this.onSubmit.emit("ok");
      }
    );
  }
}
