import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'arca-common-welcome',
  templateUrl: './welcomePanel.component.html',
  styleUrls: ['./welcomePanel.component.scss']
})
export class WelcomePanel implements OnInit {
  @Input() title : any;
  @Input() description : any;
  constructor() { }
  ngOnInit() {
  }
  
}
