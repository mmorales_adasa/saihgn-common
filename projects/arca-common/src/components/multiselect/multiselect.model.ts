import { ListUtils } from '../../utils/list.utils';
export class MultiselectModel{
    cod : string;
    desc : string;
    settings : any = {};
    elements : any[] = [];
    selectedElements : any[] = [];
    constructor(cod, desc, allowSearch){
        this.cod = cod;
        this.desc = desc;
        this.settings = {
            singleSelection: false,
            enableCheckAll: true,
            idField: cod,
            textField: desc,
            selectAllText: 'Seleccionar todos',
            unSelectAllText: 'Eliminar todos',
            itemsShowLimit: 1,
            allowSearchFilter: allowSearch,
            searchPlaceholderText: 'Buscar',
            noDataAvailablePlaceholderText: 'No existen datos'
        };
    }
    setElements(elements : any[]){
        this.elements = elements;
        var newSelectedElements = [];
        var elementsKeyList = ListUtils.getMasterComboFromObjectList(this.elements, this.cod).keyList;
        for(var element of this.selectedElements){
            if(elementsKeyList.includes(element[this.cod]) || elementsKeyList.includes(element)) 
                newSelectedElements.push(element);
        }
        this.selectedElements = newSelectedElements;
    }
    getSelectedElements() : string[]{
        return ListUtils.getMasterComboFromObjectList(this.selectedElements, this.cod).keyList;
    }
    getSelectedSummary() : string {
        return ListUtils.getStringOfDescs(this.elements, this.getSelectedElements(), this.cod, this.desc);
    }
    getElementsKeyList(){
        return ListUtils.getMasterComboFromObjectList(this.elements, this.cod).keyList;
    }
    forceSelectAll(){
        this.selectedElements = this.getElementsKeyList();
    }
    forceDeselectAll(){
        this.selectedElements = [];
    }
    cleanSelection(){
        this.selectedElements = [];
    }
}