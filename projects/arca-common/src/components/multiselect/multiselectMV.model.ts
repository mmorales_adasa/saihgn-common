import { ListUtils } from '../../utils/list.utils';
export class MultiselectModelMV {
    cod: string;
    desc: string;
    settings: any = {};
    elements: any[] = [];
    selectedElements: any[] = [];
    selectedDescriptions: any[] = [];
    public ofsset: number = 100;
    position: number = 0;
    filters : any;
    searching: boolean = false;
    filterText: any;
    public loading = false;
    public firstTime = true;
    constructor(cod, desc, allowSearch) {
        this.cod = cod;
        this.desc = desc;
        this.settings = {
            text: "",
            //maxHeight: 200,
            singleSelection: false,
            showCheckbox: true,
            badgeShowLimit: 1,
            noDataLabel: 'No existen datos',
            enableCheckAll: true,
            selectAllText: 'Seleccionar todos',
            unSelectAllText: 'Eliminar todos',
            classes: "custom-class-example ",
            enableSearchFilter: true,
            lazyLoading: true,
            primaryKey: 'COD',
            labelKey: 'DESCR'
        };
    }
    setElements(elements: any[]) {
        this.selectedElements = elements;
    }
    setElementsWithDescr(elements: any[]) {
        this.selectedElements = elements;
    }
    getSelectedElements(): string[] {
        return ListUtils.getMasterComboFromObjectList(this.selectedElements, this.cod).keyList;
    }
    getSelectedSummary(): string {
        var resumen = '';
        var i = 1;
        for (var element of this.selectedElements) {
            resumen += element[this.desc];
            if (i < this.selectedElements.length) {
                resumen += ", ";
            }
            i++;
        }
        return resumen;
    }
    getElementsKeyList() {
        return ListUtils.getMasterComboFromObjectList(this.elements, this.cod).keyList;
    }
    forceSelectAll() {
        this.selectedElements = this.getElementsKeyList();
    }
    forceDeselectAll() {
        this.selectedElements = [];
    }
    cleanSelection() {
        this.selectedElements = [];
    }
    onSearch(event: any, element: any) {
        
        var self = this;
        this.loading = true;
        this.filterText = event ? event.target.value : "";
        self.position = 0;
        if (!this.searching) {
            this.searching = true;
            element.functionPagination({ limit: this.ofsset, offset: this.position, text: this.filterText, parentFilter : this.filters }).then(newList => {               
                this.elements = newList;
                self.position = newList.length;
                this.searching = false;
                this.loading = false;
            });
        }
    }
    reload(element, filters) {
        var self      = this;
        this.position = 0;
        this.filters =  filters;        
        element.data = [];
        element.functionPagination({ limit: this.ofsset, offset: this.position, text: null, parentFilter : this.filters }).then(newList => {
            //self.position = 0;
            self.position = newList.length;
            this.elements = newList;
            this.loading = false;
        });        
    }
    fetchMore(event: any, element) {
        var self = this;
        setTimeout(() => {
            if (this.firstTime || (!self.searching && !self.loading && event.endIndex >= self.position - 1)) {
                self.loading = true;
                element.functionPagination({ limit: this.ofsset, offset: this.position, text: this.filterText, parentFilter : this.filters }).then(newList => {
                    
                    if (element.data.length == 0) {
                        self.elements = self.elements.concat(newList);
                        self.loading = false;
                    }
                    else {
                        var tmp = [];                        
                        for (var item of newList) {
                            if (!element.data.find(row => row['COD'] === item['COD'])) {
                                tmp.push(item);
                            }
                            self.elements = self.elements.concat(tmp);
                        }
                    }
                    self.firstTime = false;
                    self.position += this.ofsset;
                    self.loading = false;
                });
            }
        }, 200);
    }
}