import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuditoriaService } from './../../services/auditoria.service';
import { Router } from '@angular/router';
@Component({
  selector: 'arca-common-navigationbar',
  templateUrl: './navigationBar.component.html',
  styleUrls: ['./navigationBar.component.scss']
})
export class NavigationBar implements OnInit {
  @Input() sections : any = [];
  @Input() root : any;
  profile: any;
  @Output() seleccionar = new EventEmitter<string>();
  constructor(public _auditoriaService: AuditoriaService,
    private _router: Router) { }
  ngOnInit() {    
    this.profile = this._auditoriaService.getUserProfile();    
  }
  getClassToogle(item) {
    if (item.selected) {
      return "toggle-selected";
    }
    else{
      return "toggle-unselected";
    }
  }
  selectOption(option) {
    for (var item of this.sections){
      item.selected =  (item.route === option.route);
    }    
    this.seleccionar.emit(option.route);
  }
  getSelectedOption() {
    for (var item of this.sections){
      if (item.selected){
        return item.route;
      }
    }
  }

}
