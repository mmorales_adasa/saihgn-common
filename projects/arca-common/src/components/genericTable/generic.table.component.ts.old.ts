import { Component, OnInit, OnDestroy, Input, ViewChild, ViewEncapsulation, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, SimpleChanges, OnChanges } from '@angular/core';
import { GenericTableModel } from './generic.table.model';
import { GenericTableCommonService, GenericTableExternalFilter, OpenGenericAbeFormObject, ClickOnTableCellObject, SaerchFromFilterObject } from '../../services/generic.table.common.service';
import { MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatPaginatorIntlCustom } from './generic.table.custom.paginator';
import { Subscription } from 'rxjs';
import { CheListsFixer } from '../../services/che.lists.fixer';
//import { AuditoriaService, AuditoriaConstants } from '../../services/auditoria.service';
import { AuditoriaService } from './../../services/auditoria.service';
import { AuditoriaConstants } from './../../services/auditoria.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { animate, state, style, transition, trigger } from '@angular/animations';
@Component({
    selector: 'app-generic-table2',
    styleUrls: ['generic.table.component.css'],
    templateUrl: './generic.table.component.html',
    providers: [        
        { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCustom }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
            state('expanded', style({ height: '*', visibility: 'visible', "background-color": '#f5faff' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
    encapsulation: ViewEncapsulation.None
})
export class GenericTableComponent implements OnInit, OnDestroy, OnChanges {
    subscription: Subscription;
    afterUpdateSubscription: Subscription;
    @Input() table: GenericTableModel;
    @Input() forceRender: number;
    @Input() configuration: any = {
        showexpandTable: false,
        collapsable: false,
        dragablecolumns: true
    };
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('chartcanvas') chartCanvas: ElementRef;
    @ViewChild('expandedTable') expandedTable: any;
    public expandedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
    constructor(
        private _genericTableCommonService: GenericTableCommonService,
        private _auditoriaService: AuditoriaService,
        private _cd: ChangeDetectorRef,
        private modalService: NgbModal,
    ) { }
    ngOnInit() {
        this.table._cd = this._cd;
        this.table.paginator = this.paginator;
        // Load table subscription
        this.subscription = this._genericTableCommonService.loadTableNewCall.subscribe((data: GenericTableExternalFilter) => {
            if (this.table.parentId == data.parent) {
                this.table.chartCanvas = this.chartCanvas; // Charts                                
                //this.table.searchData(data.filter, this._genericTableCommonService);
            }
        });
        // After update/add/delete element subscription
        this.afterUpdateSubscription = this._genericTableCommonService.afterUpdateFromAbeFormNewCall.subscribe((data: OpenGenericAbeFormObject) => {
            if (this.table.id == data.id) {
                if (data.mode == 'E') this.updateTableAfterEdition(data.model); // Edit
                if (data.mode == 'D') this.updateTableAfterDeletion(data.model); // Delete
            }
        });
    }
    ngOnChanges(changes: SimpleChanges) {
    }
    expandRow(event, row) {
        if (this.configuration.collapsable) {
            var temp = event.target.innerHTML;
            if (!temp.includes("mat-checkbox")) {
                row.expanded = !row.expanded;
            }
        }
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.afterUpdateSubscription.unsubscribe();
    }
    openEditForm(tableRow, index) {
        this.resetEdition();
        this.table.dataSource.data[index]['EDITING'] = true;
        var data = [].concat(this.table.superdata);
        for (var pk of this.table.pkAttributes) {
            data = data.filter(m => m[pk] == tableRow[pk]);
        }
        var model = data[0];
        this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.table.id, 'E', model));
    }
    openAddForm() {
        this.resetEdition();
        this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.table.id, 'I', {}));
    }
    openDeleteForm(tableRow, index) {
        this.resetEdition();
        this.table.dataSource.data[index]['EDITING'] = true;
        this._genericTableCommonService.openGenericAbeForm(new OpenGenericAbeFormObject(this.table.id, 'D', tableRow));
    }
    resetEdition() {
        this.table.dataSource.data.filter(m => m['EDITING']).forEach(function (element) {
            delete element['EDITING'];
        });
    }
    updateTableAfterEdition(model) {
        var pkAttributes = this.table.pkAttributes;
        this.setModelValues(model,
            this.table.dataSource.data.find(function (element) {
                var result = false;
                for (var pk of pkAttributes) {
                    result = (element[pk] == model[pk]);
                    if (!result) return;
                }
                return result;
            })
        );
    }
    updateTableAfterDeletion(model) {
        var pkAttributes = this.table.pkAttributes;
        var data = this.table.dataSource.data.filter(m => {
            var result = false;
            for (var pk of pkAttributes) {
                result = (m[pk] == model[pk]);
                if (!result) break;
            }
            return !result;
        });
        this.table.dataSource.data = data;
        this.table.numberOfResults--;
    }
    setModelValues(_elementFrom, elementTo) {
        var data = [_elementFrom];
        data = CheListsFixer.addSuperdataFields(data);
        data = CheListsFixer.fixBefore(data);
        data = CheListsFixer.fixAfter(data, []);
        var elementFrom = data[0];
        for (var key of Object.keys(elementFrom)) {
            elementTo[key] = elementFrom[key];
        }
    }
    exportTableAsExcel() {
        this.table.exportTableAsExcel();
        this._auditoriaService.audita(this.table.COD_PERMISO, AuditoriaConstants.EXPORT_XLS, null, null);
    }
    clickOnTableElement(elementCod, elementValue) {
        if (this.table.onClickColumns.includes(elementCod)) {
            var o = new ClickOnTableCellObject(this.table.id, elementCod, elementValue);
            this._genericTableCommonService.clickOnTableCell(o);
        }
    }
    throwTableUtility(id) {
        this._genericTableCommonService.simpleEvent(id);
    }
    throwTableRowUtility(column, tableRow, index) {
        this.resetEdition();
        this.table.dataSource.data[index]['EDITING'] = true;
        var id = this.table.tableRowUtilityObjects[column].id;
        this._genericTableCommonService.searchFromFilter(new SaerchFromFilterObject(id, tableRow));
    }
    tableRowUtilityContextMenu(event, row, column) {
        event.preventDefault();
        if (this.table.tableRowUtilityObjects[column].onContextMenu) {
            this.table.tableRowUtilityObjects[column].onContextMenu(event, row, column);
        }
    }
    onChangeStickyColumn() {
        if (this.table.reviewStickyColumnSelected) {
            for (var column of this.table.stickyOptions.expanded) {
                if (column != this.table.stickyOptions.compressed) {
                    this.table.hideColumns.push(column);
                }
            }
        }
        else {
            this.table.hideColumns = [];
        }
    }
    // COLUMN_TYPES
    getColumnType(_column) {
        var column = _column.replace('-filter', '');
        if (column.startsWith('ROWUTILITY')) return 'ROWUTILITY';
        else if (column == 'EDIT') return 'EDIT';
        else if (column == 'DELETE') return 'DELETE';
        else if (column == 'SELECT') return 'SELECT';
        else if (column == 'OBSERVACIONES_ICON') return 'OBS';
        else if (this.table.linkColumns.includes(column)) return 'LINK';
        else if (this.table.colorFlagColumns.includes(column)) return 'COLOR_FLAG';
        else if (column in this.table.checkboxColumns) return 'CHECKBOX';
        else return 'DATA';
    }
    isUtilityColumn(column) {
        var result = ['EDIT', 'DELETE', 'SELECT', 'ROWUTILITY', 'OBSERVACIONES_ICON'].includes(this.getColumnType(column));
        if (!result) result = column.endsWith('_ICON');
        return result;
    }
    // EXPANDED TABLE
    expandTable() {
        this.expandedDataSource.data = this.table.dataSource.data;
        //this.modalService.open(this.expandedTable, { size: 'modal-lglg', centered: true });
        this.modalService.open(this.expandedTable, { size: 'lg', centered: true });
    }
    getCellTitle(row, column) {
        const columnType = this.getColumnType(column);
        if (columnType == "CHECKBOX"
            && typeof (this.table.checkboxColumns[column].title == "function")) {
            return this.table.checkboxColumns[column].title(row[column]);
        } else if (columnType == "LINK" && typeof (row[column + "_TITLE"]) != "undefined") {
            return row[column + "_TITLE"];
        } else {
            return row[column];
        }
    }
    isCheckboxChecked(row, column) {
        return this.table.checkboxColumns[column].checked(row[column]);
    }
    isCheckboxDisabled(row, column) {
        const config = this.table.checkboxColumns[column];
        if (typeof (config.disabled) == "function") {
            return config.disabled(row[column]);
        } else if (typeof (config.change) == "function") {
            return false;
        } else {
            return true;
        }
    }
    async onCheckboxChange(event, row, column) {
        event.preventDefault();
        const config = this.table.checkboxColumns[column];
        if (typeof (config.change) == "function") {
            config.change(!this.isCheckboxChecked(row, column), row)
                .then(() => { this._cd.markForCheck(); })
                .catch(() => { });
        }
    }
    impresosDataChanged(event) {
    }
}
/*
@Component({
    selector: 'app-graficos-control-grafica',
    template: '',
})
export class DefaulTableSubComponent {
    @Input() grafica: any;
    constructor() { }
}
*/