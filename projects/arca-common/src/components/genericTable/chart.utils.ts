import { ElementRef} from '@angular/core';
import { Chart } from 'chart.js';
import { ChartConstants } from './charts.constants'

export class ChartUtils{



    public dataToChartObject(canvas : ElementRef, config : ChartConfig, data : any[]) : Chart{

        var context : CanvasRenderingContext2D = (<HTMLCanvasElement>canvas.nativeElement).getContext('2d');
        var result = { datasets: [], labels: [], type : config.type};

        if(ChartConstants.isPieOrDoughnutType(config.type)){
            
            result.datasets.push({label: [], data : [], backgroundColor: []});
            var index = 0;
            for (let row of data) {
                if(!row['SUBTOTAL']){
                    result.datasets[0].data.push(row[config.valuesElement]);
                    var color = this.generateColor(index);
                    result.datasets[0].backgroundColor.push(color.serie);
                    result.datasets[0].label.push(row[config.seriesElement]);
                    result.labels.push(row[config.seriesElement]); 
                    index++; 
                }         
            }

            return new Chart(context, this.load(result));
        }

        if(ChartConstants.isBarOrLineType(config.type)){
            for(var i in config.series){
                var color = this.generateColor(i);
                result.datasets.push({label: config.seriesDESC[i], data: [], backgroundColor: color.serie});
            }

            for (let row of data) {
                if(!row['SUBTOTAL']){

                    var labels = [];
                    for(let label of config.x){
                        labels.push(row[label]);
                    }
                    result.labels.push(labels);

                    for(var i in config.series){
                        result.datasets[i].data.push(row[config.series[i]]);

                        // Color
                        if(config.colors.length > 0) {
                            result.datasets[i].backgroundColor = config.colors[i];
                            result.datasets[i].borderColor = config.colors[i];
                        }

                        // Fill
                        if(config.fills.length > 0) result.datasets[i].fill = config.fills[i];

                        // Point sizes
                        if(config.pointSizes.length > 0) result.datasets[i].pointRadius = config.pointSizes[i];

                        // Border widths
                        if(config.borderWidths.length > 0) result.datasets[i].borderWidth = config.borderWidths[i];
                        


                    }
                }
            }

            return new Chart(context, this.load(result));
        }
    }




    public load(m){
        return {
            type: m.type,
            data: {
              labels: m.labels, 
              datasets: m.datasets
            },
            options: ChartConstants.getOptions(m.type)
        }
    }

    public dataToDatasetsChartObject(data : ChartRowInterface[], type : string) {
        var result = { datasets: [], labels: [], type : type};

        // bar, line...
        if(ChartConstants.isBarOrLineType(type)){
            var lastSerie = '';
            var lastSerieIndex = -1;
            for (let row of data) {
                if(!result.labels.includes(row.X_DESC)) result.labels.push(row.X_DESC);
                var serie = row.SERIE.toUpperCase();
                var serieDesc = row.SERIE_DESC;
                var color = this.generateColor(lastSerieIndex+1);

                // Nueva serie
                if(serie != lastSerie){
                    result.datasets.push({label : serieDesc, data : [], backgroundColor: color.serie});
                    lastSerie = serie;
                    lastSerieIndex++;
                }
                result.datasets[lastSerieIndex].data.push(row.VALOR);
            }
            return result;
        }

        else if(ChartConstants.isPieOrDoughnutType(type)){
            result.datasets.push({label: [], data : [], backgroundColor: []});
            var index = 0;
            for (let row of data) {
                result.datasets[0].data.push(row.VALOR);
                var color = this.generateColor(index);
                result.datasets[0].backgroundColor.push(color.serie);
                result.datasets[0].label.push(row.X_DESC);
                result.labels.push(row.X_DESC);     
                index++;      
            }
            return result;
        }
    }


    // Utils

    public registerPlugins(){
		// Define a plugin to provide data labels
		Chart.plugins.register({
			afterDatasetsDraw: ChartConstants.afterDatasetsDraw
		});
    }



    // Colors

    private generateColor(index) {
        var color = this.defaultColors[index] || this.getRandomColor();     
        //var color = this.getRandomColor();   
        return {serie  : 'rgba(' + color.concat(0.4).join(',') + ')',
                normal : 'rgba(' + color.concat(1).join(',') + ')'
        };
    }

    private getRandomColor() {
      return [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
    }

    private getRandomInt(min, max){
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private defaultColors = [
        [255, 99, 132],
        [54, 162, 235],
        [255, 206, 86],
        [0, 153, 76], // cuarto color
        [231, 233, 237],
        [75, 192, 192],
        [151, 187, 205],
        [220, 220, 220],
        [247, 70, 74],
        [70, 191, 189],
        [253, 180, 92],
        [148, 159, 177],
        [77, 83, 96]
        ];
      

}

export interface ChartRowInterface{
    SERIE : string;
    SERIE_DESC : string;
    X : string;
    X_DESC : string;
    VALOR : Number;
}

export class ChartConfig{
    type : string;
    seriesElement : string;
    valuesElement : string;
    x : string[];
    series : string[];
    seriesDESC : string[];

    // Other utils
    colors : string[];
    fills : boolean[];
    pointSizes : number[];
    borderWidths : number[];

    constructor(type : string, seriesElement : string, valuesElement : string,
            x : string[], series : string[], seriesDESC : string[]){
        this.type = type;
        this.seriesElement = seriesElement;
        this.valuesElement = valuesElement;
        this.x = x;
        this.series = series;
        this.seriesDESC = seriesDESC;

        // Other utils
        this.colors = [];
        this.fills = [];
        this.pointSizes = [];
        this.borderWidths = [];
    }
}

export const ChartConfigUtils = {
    generatePieChartConfig(type : string, seriesElement : string, valuesElement : string){
        return new ChartConfig(type, seriesElement, valuesElement, null, null, null);
    },
    generateBarChartConfig(type : string, x : string[], series : string[], seriesDESC : string[]){
        return new ChartConfig(type, null, null, x, series, seriesDESC);
    },
}







