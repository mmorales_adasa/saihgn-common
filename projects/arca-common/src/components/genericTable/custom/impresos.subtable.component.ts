import { Component, ApplicationRef, ViewContainerRef, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, NgZone, SimpleChanges, OnChanges } from '@angular/core';
import { GenericTableCommonService } from '../../../services/generic.table.common.service';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorIntlCustom } from './../generic.table.custom.paginator';
//import { AuditoriaService } from '../../../services/auditoria.service';
import { AuditoriaService } from  './../../../services/auditoria.service';

import { ImpresosService } from '../../../services/impresos.service';
import { UtilitiesService } from '../../../services/utilities.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { constants } from 'os';
import { constants } from '../../../config/constants';
@Component({
  selector: 'app-impresos-subtable',
  styleUrls: ['impresos.subtable.component.css'],
  templateUrl: './impresos.subtable.component.html',
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCustom }, UtilitiesService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpresosSubtableCmponent implements OnInit, OnDestroy, OnChanges {
  @Input() element: any;
  @Input() forceRender: number;
  @Output() onDataChange = new EventEmitter<boolean>();

  @ViewChild('modalConfirmation') modalConfirmation: any;
  @ViewChild('modalHistory') modalHistory: any;
  @ViewChild('modalBorrar') modalBorrar: any;
  @ViewChild('modalCrearImpreso') modalCrearImpreso: any;
  @ViewChild('modalAtributos') modalAtributos: any;
  @ViewChild('modalErrorDescription') modalErrorDescription: any;
  //@ViewChild('table_container', {read: ViewContainerRef}) table_container: ViewContainerRef;

  itemSelected: any;
  estadoSelected: any;
  tipoSelected: any;
  errorMsgCrear: any;
  errorMsgModal: any;
  observaciones: any;
  enviado: any;
  firmado: any;
  incluirInsitu: boolean = true;
  incluirParamsLabExternos: boolean = false;
  tiposImpreso: any = [];
  history: any = [];
  listStatus = []
  allowGenerar: boolean = false;
  allowGenerarModal: boolean = false;
  permisos : any;
  constructor(
    public appRef: ApplicationRef,
    private _genericTableCommonService: GenericTableCommonService,
    private _auditoriaService: AuditoriaService,
    public _impresosService: ImpresosService,
    public _utilitiesService: UtilitiesService,
    public _zone: NgZone,
    private modalService: NgbModal,
  ) {
  }
  ngOnInit() {
    this.tiposImpreso = this._impresosService.getTiposImpreso();    
    if (this._auditoriaService.getUserProfile()){
      this.permisos =  this._auditoriaService.getUserProfile().permisos; 
    }  
    this.allowGenerar = this._impresosService.comprobacionImprimible(this.element);
  }
  ngOnDestroy() {
  }
  selectEstado(estado) {
    this.estadoSelected = estado;
  }
  previsualizar() {
    var self = this;
    var params = {
      usuario: this._auditoriaService.getUserProfile().username,
      tipo: self.tipoSelected,
      conf: self.getConfImpreso(),
      analisis: { COD_ANALISIS: this.element.COD_ANALISIS, COD_TIPO_MATRIZ: this.element.COD_TIPO_MATRIZ, PER_MUE: this.element.PER_MUE }
    };
    this._impresosService.previsualizar(params).subscribe(
      data => {
        var nameFile = self._impresosService.getTipoImpreso(self.tipoSelected).prefix_file + "para_analisis_" + this.element.COD_ANALISIS + ".pdf";
        self.openInNewTab(data._body, nameFile);
      });
  }
  public openInNewTab(binary, nameFile) {
    var contentType = 'application/pdf';
    var blob = new Blob([binary], { type: contentType });
    const fileURL = URL.createObjectURL(blob);
    window.open(fileURL, '_blank');
  }
  public forceDownload(binary, nameFile) {
    var contentType = 'application/pdf';
    var blob = new Blob([binary], { type: contentType });
    var a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = nameFile;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
  validaCreacionImpreso() {
    if (this.tipoSelected == constants.TIPO_IMPRESO.INFORME_ANALISIS) {
      var position = this.element.LIST_IMPRESOS.findIndex(item => parseInt(item.COD_TIPO_IMPRESOS) === constants.TIPO_IMPRESO.INFORME_ANALISIS);
      if (position >= 0) {
        if (this.element.LIST_IMPRESOS[position].FIRMADO == 1) {
          this.allowGenerarModal = false;
          this.errorMsgCrear = "<b>ATENCIÓN! </b>El informe actual está FIRMADO, por favor genere un MODIFICADO ";
        }
        else {
          this.errorMsgCrear = "<b>ATENCIÓN! </b>Ya existe un impreso del mismo tipo. Se generará uno nuevo en el estado inicial pendiente de validar";
        }
      }
    }
    if (this.tipoSelected == constants.TIPO_IMPRESO.INFORME_ANALISIS_MODIFICADO) {
      var position = this.element.LIST_IMPRESOS.findIndex(item => parseInt(item.COD_TIPO_IMPRESOS) === constants.TIPO_IMPRESO.INFORME_ANALISIS);
      if (position >= 0) {
        if (this.element.LIST_IMPRESOS[position].FIRMADO != 1) {
          this.allowGenerarModal = false;
          this.errorMsgCrear = "<b>ATENCIÓN! </b>No puede crear un MODIFICADO ya que no existe un Informe de análisis previo FIRMADO ";
        }
      }
      else {
        this.allowGenerarModal = false;
        this.errorMsgCrear = "<b>ATENCIÓN! </b>No puede crear un MODIFICADO ya que no existe un Informe de análisis previo FIRMADO ";
      }
      // No hay errores -- avsos
      if (!this.errorMsgCrear) {
        var position = this.element.LIST_IMPRESOS.findIndex(item => item.COD_TIPO_IMPRESOS === this.tipoSelected);
        if (position >= 0) {
          this.errorMsgCrear = "<b>ATENCIÓN! </b>Ya existe un impreso del mismo tipo. Se generará uno nuevo en el estado inicial pendiente de validar";
        }
      }
    }
  }
  checkExisteImpreso() {
    var position = this.element.LIST_IMPRESOS.findIndex(item => item.COD_TIPO_IMPRESOS === this.tipoSelected);
    if (position >= 0) {
      return true;
    }
    return false;
  }
  saveCrear() {
    var self = this;
    if (self.checkExisteImpreso()) {
      self.itemSelected = this.element.LIST_IMPRESOS.find(item => item.COD_TIPO_IMPRESOS === this.tipoSelected);
      var p1 = {
        usuario: this._auditoriaService.getUserProfile().username,
        tipo: self.tipoSelected,
        codImpreso: self.itemSelected.COD_IMPRESO,
        conf: self.getConfImpreso(),
        existe: self.checkExisteImpreso(),
        analisis: { COD_ANALISIS: this.element.COD_ANALISIS, COD_TIPO_MATRIZ: this.element.COD_TIPO_MATRIZ, PER_MUE: this.element.PER_MUE }
      };
      this._impresosService.reGenerarImpreso(p1).subscribe(
        data => {
          self.itemSelected.ESTADO_IMPRESO = constants.ESTADO_IMPRESO.PENDIENTE_VALIDACION;
          self.itemSelected.ENVIADO = 0;
          self.itemSelected.FIRMADO = 0;
          self.emitChangeStatus(true);
          self.modalService.dismissAll();
        });
    }
    else {
      var p2 = {
        usuario: this._auditoriaService.getUserProfile().username,
        tipo: self.tipoSelected,
        conf: self.getConfImpreso(),
        existe: self.checkExisteImpreso(),
        analisis: { COD_ANALISIS: this.element.COD_ANALISIS, COD_TIPO_MATRIZ: this.element.COD_TIPO_MATRIZ, PER_MUE: this.element.PER_MUE }
      };
      self._impresosService.generarImpreso(p2).subscribe(
        data => {
          self._zone.run(() => {
            self.element.LIST_IMPRESOS.push({
              COD_IMPRESO: data.id,
              NOMBRE_FICHERO_PDF: data.nameFile,
              COD_TIPO_IMPRESOS: self.tipoSelected,
              ENVIADO: 0,
              FIRMADO: 0,
              ESTADO_IMPRESO: constants.ESTADO_IMPRESO.PENDIENTE_VALIDACION
            });
            self.emitChangeStatus(true);
            self.modalService.dismissAll();
          });
        });
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    
  }
  showHistory(item) {
    var self = this;
    this.itemSelected = item;
    var params = { codImpreso: item.COD_IMPRESO };
    this._impresosService.getHistorialImpreso(params).subscribe(
      data => {
        self.history = data;
        this.modalService.open(this.modalHistory, { size: 'lg', centered: true });
      })
  }
  download(item) {
    var self = this;
    this._impresosService.download(item.COD_IMPRESO).subscribe(
      data => {
        var nameFile = self._impresosService.getTipoImpreso(item.COD_TIPO_IMPRESOS).prefix_file + item.COD_IMPRESO + "_PARA_ANALISIS_" + item.COD_ANALISIS + ".pdf";
        self.forceDownload(data._body, nameFile);
      },
      error => {
        self.errorMsgModal = "El fichero " + item.NOMBRE_FICHERO_PDF + " no encontrado en el sistema de ficheros.";
        self.modalService.open(self.modalErrorDescription, { size: 'lg', centered: true });
      });
  }
  openCrearImpreso(tipo) {
    this.errorMsgCrear = null;
    this.tipoSelected = tipo;
    this.incluirInsitu = true;
    this.incluirParamsLabExternos = false;
    this.allowGenerarModal = true;
    this.validaCreacionImpreso();
    this.modalService.open(this.modalCrearImpreso, { size: 'lg', centered: true });
  }
  openValidar(item) {
    this.itemSelected = item;
    this.getListStatusForSelectedItem();
    this.observaciones = null;
    this.modalService.open(this.modalConfirmation, { size: 'lg', centered: true });
  }
  openBorrar(item) {
    this.itemSelected = item;
    this.modalService.open(this.modalBorrar, { size: 'lg', centered: true });
  }
  openEditar(item) {
    this.itemSelected = item;
    this.enviado = this.itemSelected.ENVIADO;
    this.firmado = this.itemSelected.FIRMADO;
    this.modalService.open(this.modalAtributos, { size: 'lg', centered: true });
  }
  saveBorrar() {
    var self = this;
    var params = {
      codImpreso: this.itemSelected.COD_IMPRESO
    };
    this._impresosService.deleteImpreso(params).subscribe(
      data => {
        self._zone.run(() => {
          var position = this.element.LIST_IMPRESOS.findIndex(item => item.COD_IMPRESO === self.itemSelected.COD_IMPRESO);
          self.element.LIST_IMPRESOS.splice(position, 1);
        });
        self.emitChangeStatus(true);
        self.modalService.dismissAll();
      });
  }
  saveEditar() {
    var self = this;    
    var params = {
      codImpreso: this.itemSelected.COD_IMPRESO,
      nombreFicheroPDF: this.itemSelected.NOMBRE_FICHERO_PDF,
      firmado: self.firmado,
      enviado: self.enviado

    };
    this._impresosService.updateImpreso(params).subscribe(
      data => {
        self.itemSelected.ENVIADO = self.enviado;
        self.itemSelected.FIRMADO = self.firmado;
        self.emitChangeStatus(true);
        self.modalService.dismissAll();
      });

  }
  saveValidar() {
    var self = this;
    if (this.estadoSelected == null) {
      alert('No hay seleccionado estado');
    }
    else {
      var params = {
        codUsuario: this._auditoriaService.getUserProfile().username,
        codEstado: this.estadoSelected,
        observacion: this.observaciones,
        codImpreso: this.itemSelected.COD_IMPRESO
      };

      this._impresosService.insertarEstado(params).subscribe(
        data => {
          self._zone.run(() => {
            self.itemSelected.ESTADO_IMPRESO = self.estadoSelected;
            self.emitChangeStatus(true);
          });
          this.modalService.dismissAll();
        });
    }
  }
  getStatusStyle(item) {
    return { 'color': 'black', 'background-color': item.color, 'border': '1px solid black', 'border-radius': '3px', 'font-weight': 'bold' };
  }
  getListStatusForSelectedItem() {
    
    this.listStatus = [];
    if (this._auditoriaService.getUserProfile().codUsuario === constants.ROOT) {
      this.listStatus = this._impresosService.getEstados();
    } else {      
      if (this.itemSelected.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.PENDIENTE_VALIDACION && this.permisos.includes("gt_a_rev1")) {
        this.listStatus.push(this._impresosService.getEstadoById(constants.ESTADO_IMPRESO.VALIDADO_RESP_TECNICO));
      }
      else if (this.itemSelected.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.VALIDADO_RESP_TECNICO && this.permisos.includes("gt_a_rev2")) {
        this.listStatus.push(this._impresosService.getEstadoById(constants.ESTADO_IMPRESO.VALIDADO_RESP_CALIDAD));
      }
      else if (this.itemSelected.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.VALIDADO_RESP_CALIDAD && this.permisos.includes("gt_a_rev3")) {
        this.listStatus.push(this._impresosService.getEstadoById(constants.ESTADO_IMPRESO.VALIDADO_DIREC_CALIDAD));
      }
    }
  }
  allowValidar(item){    
    if (this._auditoriaService.getUserProfile().codUsuario === constants.ROOT) {
      return true;
    } else {      
      if (item.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.PENDIENTE_VALIDACION && this.permisos.includes("gt_a_rev1")) {
        return true;
      }
      else if (item.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.VALIDADO_RESP_TECNICO && this.permisos.includes("gt_a_rev2")) {
        return true;
      }
      else if (item.ESTADO_IMPRESO == constants.ESTADO_IMPRESO.VALIDADO_RESP_CALIDAD && this.permisos.includes("gt_a_rev3")) {
        return true;
      }
    }
    return false;
  }
  emitChangeStatus(finished) {
    this.onDataChange.emit(finished);
  }

  isRoot() {
    return (this._auditoriaService.getUserProfile().codUsuario === constants.ROOT);
  }
  getConfImpreso() {
    return { incluirInsitu: this.incluirInsitu, incluirParamsLabExternos: this.incluirParamsLabExternos };
  }
}



