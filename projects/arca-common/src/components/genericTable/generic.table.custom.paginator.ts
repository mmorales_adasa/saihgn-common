import {MatPaginatorIntl} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {Injectable} from '@angular/core';

@Injectable()
export class MatPaginatorIntlCustom extends MatPaginatorIntl {
  
  
  deLabel : String;

  constructor(private translate: TranslateService) {
    super();
    this.translate.onLangChange.subscribe((e: Event) => { this.getAndInitTranslations(); });
    this.getAndInitTranslations();
  }

  getAndInitTranslations() {
    this.translate.get(['Registros por página', 'Página siguiente', 'Página anterior', 
        'Primera página', 'Última página', 'de']).subscribe(translation => {
      this.itemsPerPageLabel = translation['Registros por página'] + ':';
      this.nextPageLabel = translation['Página siguiente'];
      this.previousPageLabel = translation['Página anterior'];
      this.firstPageLabel = translation['Primera página'];
      this.lastPageLabel = translation['Última página'];
      this.deLabel = translation['de'];
      this.changes.next();
    });
  }
  
  getRangeLabel = function (page, pageSize, length) {
    if (length === 0 || pageSize === 0) {
      return '0 de ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' ' + this.deLabel + ' ' + length;
  };
  

}