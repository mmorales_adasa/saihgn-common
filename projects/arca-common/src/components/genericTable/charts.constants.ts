
import { Chart } from 'chart.js';
	
export const ChartConstants = {

    getOptions(type : string) { 
        var options = {
            scaleShowVerticalLines: true, 
            responsive: true ,
            legend: {
                display: true,
                position : 'bottom',
                labels: {
                    fontColor: '#222',
                    fontSize: 11,
                    boxWidth: 15
                },
            },
            elements: {
                arc: {
                    borderWidth: 1
                }
            },
            tooltips:{
                titleFontSize: 11,
                bodyFontSize: 11,
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                        fontSize: 10
                        }
                    }
                ],
                xAxes: [
                    {
                        ticks: {
                        fontSize: 10
                        }
                    }
                ],      
            }
        }

        if(this.isPieOrDoughnutType(type)) delete options.scales;
        if(this.isBarOrLineType(type)) options.legend.labels['generateLabels'] = ChartConstants.removeLegendBoxesBorder;

        return options;
        
    },


    // Utils

    isPieOrDoughnutType(type){
        return (('pie' == type) || ('doughnut' == type))
    },

    isBarOrLineType(type){
        return (('bar' == type) || ('line' == type))
    },

    // Other utils

    removeLegendBoxesBorder(chart) {
        var data = chart.data;
        return data.datasets.map(function(dataset, i) {
            return {
                text: dataset.label,
                fillStyle: dataset.backgroundColor,
                hidden: !chart.isDatasetVisible(i),
                lineCap: dataset.borderCapStyle,
                lineDash: dataset.borderDash,
                lineDashOffset: dataset.borderDashOffset,
                lineJoin: dataset.borderJoinStyle,
                lineWidth: 0,
                strokeStyle: dataset.borderColor,
                // Below is extra data used for toggling the datasets
                datasetIndex: i
            };
        }, this)
    },

    // Plugins

    afterDatasetsDraw: function(chart) {
        var type = chart.config.type;
        if(ChartConstants.isPieOrDoughnutType(type)){
            var ctx = chart.ctx;   
            chart.data.datasets.forEach(function(dataset, i) { 
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        if(!element.hidden) {
                            ctx.fillStyle = '#222';
                            var fontSize = 11;                        
                            var fontStyle = 'normal';
                            var fontFamily = 'inherit';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();
                            //var dataString = chart.data.labels[index].toString() + ': ' + dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 0;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        }
                    });
                }
            });
        }
    }


};


