import { HttpClient } from '@angular/common/http';
import { ElementRef, ChangeDetectorRef, Optional } from '@angular/core';
import { ListUtils } from '../../utils/list.utils';
import { MatTableDataSource, Sort } from '@angular/material';
//import { MsCalidadService } from '../../services/mscalidad.service';
import { MatPaginator } from '@angular/material';
import { Chart } from 'chart.js';
import { ChartUtils, ChartConfig } from './chart.utils';
import { GenericTableCommonService, GenericTableExternalFilter } from '../../services/generic.table.common.service';
//import { CheListsFixer } from './../../services/che.lists.fixer';
import { GenericTableExportService } from './generic.table.export.service';
import { MultiselectModel } from '../multiselect/multiselect.model';
import { MultiselectModelMV } from '../multiselect/multiselectMV.model';
export class GenericTableModel {
    public _genericTableCommonService: GenericTableCommonService;
    public _cd: ChangeDetectorRef;
    // Datos totales
    public parentId: string;
    public id: string;
    public COD_PERMISO: string;
    public superdata: any[] = [];
    //public dataSourceOriginal : MatTableDataSource<any> = new MatTableDataSource<any>();
    public title: string = null;
    // Tabla de datos
    public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
    public filterWidthClass: string;
    // BI elements config
    public attributes: string[];
    public metrics: string[];
    public displayedAttributes: string[];
    public displayedMetrics: string[];
    public stickyColumns: boolean[] = [];
    public hideColumns: any[] = [];
    public displayedPageFilters: string[];
    public displayedPageFiltersObjects: {};
    // Utilidades de la tabla
    public numberOfResults = 0;
    public currentSort: Sort;
    public currentTextFilter: string;
    public textFilterTimeout: number = 500;
    private filterByGeneralTextTimeout = null;
    private filterByColumnTextTimeout = {};
    // Show/hide options
    public tableIsLoaded: boolean;
    public tableIsLoading: boolean;
    public tableHasResults: boolean;
    // Export 
    public _exportService: GenericTableExportService;
    // DB Service
    //public _msCalidadService: MsCalidadService;
    //public _msCalidadService_function: string;
    // Displayed columns
    public displayedColumns: string[];
    public displayedColumnsFilters: string[];
    public displayedColumnsFiltersObjects: {};
    public displayedColumnsMovements: string[];
    public lastDisplayedAttributeColumn: string;
    // Show/hide options
    public showSubtotals: boolean;
    public showPageFilters: boolean;
    public showColumnFilters: boolean;
    // Sticky behaviour
    public showReviewStickyColumn = false;
    public reviewStickyColumnSelected = false;
    public stickyOptions = {
        expanded: [],
        compressed: null,
        confCompressed: null
    };
    public showMovementOptions: boolean;
    public showSubtotal = true;
    public showTotal = true;
    public showExternalTextFilter = true;
    public showShowSubtotals = true;
    public showShowPageFilters = true;
    public showShowColumnFilters = true;
    public showShowMovementOptions = true;
    // funciones externas
    public beforeTableIsBuiltFunction: any;
    public afterTableIsBuiltFunction: any;
    public setDisplayedMetricsFunction: any;
    // Multiselect
    public dropdownSettings = {
        singleSelection: false,
        enableCheckAll: true,
        idField: 'cod',
        textField: 'desc',
        selectAllText: 'Seleccionar todos',
        unSelectAllText: 'Eliminar todos',
        itemsShowLimit: 1,
        allowSearchFilter: false
    };
    // Drag&drop utils
    public draggedElement = { id: '', type: '' };
    public binDraggedOn = false;
    public tableColumnDraggedOn = null;
    public pagefilterDraggedOn = null;
    public pagefilterDraggedOn_side = null;
    // Paginator
    public showPaginator = true;
    public paginator: MatPaginator;
    public pageRows = 50;
    // Chart
    public showChart = false;
    public chart: Chart;
    public chartCanvas: ElementRef;
    public chartConfig: ChartConfig;
    public chartUtils: ChartUtils = new ChartUtils();

    // Aspect options
    public separeMetrics: boolean = false;
    public hideMetricSortArrows: boolean = false;
    public ellipsisColumns: string[] = [];
    // External filters
    public hasExternalFilters: boolean = false;
    public externalFilters: string[] = [];
    public externalFiltersObjects: {};
    // SubComponent
    public hasSubcomponent: boolean = false;
    public subcomponentId: string = '';
    public subcomponentObject: any = {};
    // Links and cplumnWithIcons
    public linkColumns = [];
    public colorFlagColumns = [];
    public checkboxColumns = {};
    // style
    public styleHeight = 'unset';
    public smallMetricsInHeader = false;
    public fixedWidthHeaders: string[] = [];
    // Remarcar cambios
    public highlightChanges = false;
    public highlightChangesColumns = [];
    // Remarcar cambios
    public highlightRowChanges = false;
    public highlightRowChangesColumns = [];
    public highlightColumnChanges = false;
    public highlightColumnChangesColumns = [];
    // Simple superheader
    public simpleSuperHeader = false;
    // Tablas EDITABLES
    public editable = false;
    public deletable = false;
    public insertable = false;
    public pkAttributes: string[] = [];
    // Prohibir la agrupación de filas
    public doNotGroup = false;
    // Loading ion refreshing
    public refreshing = false;
    // On table click
    public onClickColumns = [];
    // Multiple header
    public multipleHeaderObjectList = [];
    public hasMultipleHeader = false;
    public transposeFunction = null;
    // Hard separator
    public hardSeparatorColumns = [];
    public metricTinnyWidth = false;
    public metricSuperTinnyWidth = false;
    public totalColumns = [];
    // Utilidades
    tableUtilities: GenericTableUtility[] = [];
    tableRowUtilities: GenericTableUtility[] = [];
    tableRowUtilityObjects = {};
    // Seleccion de filas
    public selectable = false;
    selection: any[] = [];
    traslations: any;
    functionSearch: any;
    classFisxFields: any;
    // Constructor
    constructor(
        parentId: string,
        id: string,
        COD_PERMISO: string,
        msCalidadService: any,
        msCalidadService_function: string,
        attributes: string[],
        metrics: string[],
        filterAttributes: string[],
        displayedAttributes: string[],
        classFisxFields: any
    ) {
        this.parentId = parentId;
        this.id = id;
        this.COD_PERMISO = COD_PERMISO;
        //this._msCalidadService = msCalidadService;
        //this._msCalidadService_function = msCalidadService_function;
        this.filterWidthClass = 'col-md-2';
        this.attributes = attributes;
        this.displayedAttributes = displayedAttributes;
        this.metrics = metrics;
        this.displayedMetrics = [].concat(metrics);
        this.displayedPageFilters = filterAttributes;
        this.tableIsLoaded = false;
        this.tableIsLoading = false;
        this.tableHasResults = false;
        this.currentTextFilter = '';
        this.numberOfResults = 0;
        this._exportService = new GenericTableExportService();
        // Inicializamos otros valores
        this.currentSort = { active: this.displayedAttributes[0], direction: 'asc' };
        this.displayedColumnsFiltersObjects = {};
        this.lastDisplayedAttributeColumn = '';
        this.displayedPageFiltersObjects = {};
        this.classFisxFields = classFisxFields;
        for (var i in this.displayedPageFilters) this.displayedPageFiltersObjects[this.displayedPageFilters[i]] = new GenericTablePageFilterModel(this.displayedPageFilters[i]);
        // Show/hide options
        this.showPageFilters = true;
        this.showColumnFilters = true;
        this.showMovementOptions = true;
        this.showSubtotals = true;
        // funciones externas
        this.setDisplayedMetricsFunction = null;
    }
    // Simple super header
    setSimpleSuperHeader() {
        this.simpleSuperHeader = true;
        this.showSubtotals = false;
        this.showShowColumnFilters = false;
        this.showShowMovementOptions = false;
        this.showShowPageFilters = false;
        this.showPageFilters = false;
        this.showPaginator = false;
    }
    // External Filters
    public setExternalFilters(externalFilters: string[]) {
        this.externalFilters = externalFilters;
        this.externalFiltersObjects = {};
        for (var i in this.externalFilters) this.externalFiltersObjects[this.externalFilters[i]] = new GenericTablePageFilterModel(this.externalFilters[i]);;
        this.hasExternalFilters = (this.externalFilters.length > 0);
    }
    // SubComponentes
    public setSubcomponent(subcomponentId: string, subcomponentObject: any) {
        this.hasSubcomponent = true;
        this.subcomponentId = subcomponentId;
        this.subcomponentObject = subcomponentObject;
    }
    public startLoading() {
        this.tableIsLoading = true;
    }
   
    public setDataAndLoadTable(data: any[]) {
        this.paginator.pageIndex = 0;
        this.superdata = data;
        this.currentTextFilter = '';
        this.superdata = this.classFisxFields.addSuperdataFields(this.superdata);
        if (this.setDisplayedMetricsFunction) this.displayedMetrics = this.setDisplayedMetricsFunction(this.superdata);
        if (this.beforeTableIsBuiltFunction) this.superdata = this.beforeTableIsBuiltFunction(this.superdata);
        this.buildTable();
    }

    public buildTable() {
        var data = [].concat(this.superdata);
        this.tableIsLoading = true;
        this.refreshing = (this.tableIsLoaded ? true : false);
        // Cargamos displayed columns
        this.loadDisplayedColumns();
        // Filtros externos
        data = this.filterTableByExternalFilters(data);
        // Emitimos evento por si hay alguien que escuche, gráficas por ejemplo
        if (this._genericTableCommonService) {
            var filter = new GenericTableExternalFilter(this.parentId, {});
            filter.id = this.id;
            filter.data = data;
            this._genericTableCommonService.afterLoadTable(filter);
        }
        // Filtramos por texto general
        data = this.filterTableByExternalText(data);
        this.tableHasResults = (data.length > 0);
        // Filtramos por filtros "de página"
        data = this.filterTableByPageFilters(data);
        // Transponemos
        if (this.transposeFunction) {

            var tfr = this.transposeFunction(data, this.displayedAttributes);
            data = tfr.data;
            this.displayedMetrics = tfr.displayedMetrics;
            this.multipleHeaderObjectList = tfr.multipleHeaderObjectList;
            this.hasMultipleHeader = true;
            if (tfr.hardSeparatorMetrics) this.hardSeparatorColumns = tfr.hardSeparatorMetrics;
            this.loadDisplayedColumns();
        }
        // Filtramos por filtros de columna
        // Lo ponemos después de la transposición para que las columans de métricas ya estén creadas
        data = this.filterTableByColumnFilters(data);
        // Agrupar
        // Si la tabla es editable, no agrupamos para no perder PKs
        // Si la tabla tiene transposición, ya habremos agrupado en ella
        // Si se prohibe expresamente agrupar, no agrupamos. Así no se pierden columnas no visibles
        if (!this.editable && !this.transposeFunction && !this.doNotGroup) {
            data = ListUtils.groupObjectList(data, this.displayedAttributes, this.displayedMetrics);
        }
        // Funciones generales de preparación de tabla
        data = this.classFisxFields.fixBefore(data);
        // Ordenar
        data = this.sortTable(data);
        // Subtotales
        data = this.addSubtotals(data);
        // Funciones generales de preparación de tabla

        data = this.classFisxFields.fixAfter(data, this.displayedMetrics);

        // Función externa
        if (this.afterTableIsBuiltFunction) data = this.afterTableIsBuiltFunction(data);
        // Marcado de cambios
        if (this.highlightRowChanges) this.showHighlightRowChanges(data);
        if (this.highlightColumnChanges) this.showHighlightColumnChanges(data);
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.numberOfResults = ListUtils.getNumberOfResults(data);
        var __cd = this._cd;
        setTimeout(() => {
            __cd.markForCheck();
            this.tableIsLoaded = true;
            this.tableIsLoading = false;
            this.refreshing = false;
        }, 10)
        // Chart
        if (this.showChart) {
            this.chart = this.chartUtils.dataToChartObject(this.chartCanvas, this.chartConfig, data);
        }
    }
    private loadDisplayedColumns() {
        this.displayedColumns = [].concat(this.displayedAttributes).concat(this.displayedMetrics);
        this.displayedColumnsFilters = [];
        this.displayedColumnsMovements = [];
        var displayedColumnsFiltersObjects_aux = {};
        // Utilidades
        for (var rowUtility of this.tableRowUtilities) {
            var columnId = 'ROWUTILITY_' + rowUtility.id;
            this.displayedColumns = [columnId].concat(this.displayedColumns);
            this.tableRowUtilityObjects[columnId] = rowUtility;
        }
        if (this.editable) this.displayedColumns = ['EDIT'].concat(this.displayedColumns);
        if (this.deletable) this.displayedColumns = ['DELETE'].concat(this.displayedColumns);
        if (this.selectable) this.displayedColumns = ['SELECT'].concat(this.displayedColumns);
        // Añadimos ids para los filtros de columna
        for (var i in this.displayedColumns) {
            var id = this.displayedColumns[i];
            var filterId = id + '-filter';
            var movementId = id + '-movement';
            this.displayedColumnsFilters.push(filterId);
            this.displayedColumnsMovements.push(movementId);
            if (this.displayedColumnsFiltersObjects.hasOwnProperty(id)) displayedColumnsFiltersObjects_aux[id] = this.displayedColumnsFiltersObjects[id];
            else displayedColumnsFiltersObjects_aux[id] = new GenericTableColumnFilterModel(id);
        }
        this.displayedColumnsFiltersObjects = displayedColumnsFiltersObjects_aux;
        this.lastDisplayedAttributeColumn = this.displayedAttributes[this.displayedAttributes.length - 1];
        // External filters
        var externalFiltersObjects_aux = {};
        for (var i in this.externalFilters) {
            var id = this.externalFilters[i];
            if (this.externalFiltersObjects.hasOwnProperty(id)) externalFiltersObjects_aux[id] = this.externalFiltersObjects[id];
            else externalFiltersObjects_aux[id] = new GenericTablePageFilterModel(id);
        }
        this.externalFiltersObjects = externalFiltersObjects_aux;
        // Page filters
        var displayedPageFiltersObjects_aux = {};
        for (var i in this.displayedPageFilters) {
            var id = this.displayedPageFilters[i];
            if (this.displayedPageFiltersObjects.hasOwnProperty(id)) displayedPageFiltersObjects_aux[id] = this.displayedPageFiltersObjects[id];
            else displayedPageFiltersObjects_aux[id] = new GenericTablePageFilterModel(id);
        }
        this.displayedPageFiltersObjects = displayedPageFiltersObjects_aux;
        // Current sort
        if (!this.displayedColumns.includes(this.currentSort.active)) {
            this.currentSort.active = this.displayedColumns[0];
            this.currentSort.direction = 'asc';
        }
    }
    private filterTableByExternalText(data: any[]): any[] {
        if (this.currentTextFilter) {
            var dataSourceAux = new MatTableDataSource(data);
            dataSourceAux.filter = this.currentTextFilter.trim().toLowerCase();
            data = dataSourceAux.filteredData;
        }
        return data;
    }
    private filterTableByExternalFilters(data: any[]): any[] {
        // Page filters
        for (var i in this.externalFilters) {
            var pageFilter = this.externalFiltersObjects[this.externalFilters[i]];
            // Actualizamos filtro
            pageFilter.load(data, !!this.checkboxColumns[this.externalFilters[i]]);
            // Actualizamos tabla
            data = ListUtils.filterObjectList(data, pageFilter.attribute, [pageFilter.selectedItem]);
        }
        return data;
    }
    private filterTableByPageFilters(data: any[]): any[] {
        // Page filters
        for (var i in this.displayedPageFilters) {
            var pageFilter = this.displayedPageFiltersObjects[this.displayedPageFilters[i]];
            // Actualizamos filtro
            pageFilter.load(data, !!this.checkboxColumns[this.displayedPageFilters[i]]);
            // Actualizamos tabla
            var selectedElements = ListUtils.possibleObjectListToKeyList(pageFilter.msModel.selectedElements, pageFilter.msModel.cod);
            data = ListUtils.filterObjectList(data, pageFilter.attribute, selectedElements);
        }
        return data;
    }
    private filterTableByColumnFilters(data: any[]): any[] {
        // Column filters
        for (var i in this.displayedColumns) {
            var columnFilter = this.displayedColumnsFiltersObjects[this.displayedColumns[i]];
            // Actualizamos tabla
            if (columnFilter.value) {
                data = ListUtils.filterObjectListLike(data, columnFilter.attribute, columnFilter.value);
            }
        }
        return data;
    }
    private sortTable(data: any[]): any[] {
        data = ListUtils.sortObjectList(data, this.currentSort.active, this.currentSort.direction);
        return data;
    }
    sortData(sort: Sort) {
        this.currentSort = sort;
        this.buildTable();
    }
    setFirstSorting(firstSortingAttribute: string, ascDirection: boolean) {
        this.currentSort = {
            active: firstSortingAttribute,
            direction: ascDirection ? 'asc' : 'desc'
        };
    }
    addSubtotals(data: any[]): any[] {
        var lastDisplayedAttribute = this.displayedAttributes[this.displayedAttributes.length - 1];
        if (this.showSubtotals) {
            // Subtotal
            if (this.showSubtotal) {
                if ((this.displayedAttributes.length > 1) || (this.displayedMetrics.length == 0)) {
                    data = ListUtils.addSubtotalToObjectList(data, this.currentSort.active, lastDisplayedAttribute, this.displayedMetrics, 'SUBTOTAL_1');
                }
            }
            // Total
            if (this.showTotal) {
                if (data.length > 0) data = ListUtils.addTotalToObjectList(data, this.displayedMetrics, this.displayedAttributes[0], lastDisplayedAttribute, 'SUBTOTAL_2');
            }
        }
        return data;
    }
    showHideSubtotals() {
        this.buildTable();
    }
    filterByGeneralText(filterValue: string) {
        if (this.textFilterTimeout <= 0) {
            this.currentTextFilter = filterValue;
            this.buildTable();
        } else {
            this.filterByGeneralTextWithTimeout(filterValue);
        }
    }
    async filterByGeneralTextWithTimeout(filterValue: string) {
        const self = this;
        await this.waitWhileLoading();
        if (this.filterByGeneralTextTimeout !== null) {
            clearTimeout(this.filterByGeneralTextTimeout);
        }
        self.currentTextFilter = filterValue;
        this.filterByGeneralTextTimeout = setTimeout(
            function () {
                self.buildTable();
                self.filterByGeneralTextTimeout = null;
            },
            this.textFilterTimeout
        );
    }
    filterByColumnText(text: string, attribute: string) {
        if (this.textFilterTimeout <= 0) {
            this.displayedColumnsFiltersObjects[attribute].value = text;
            this.buildTable();
        } else {
            this.filterByColumnTextWithTimeout(text, attribute);
        }
    }
    async filterByColumnTextWithTimeout(text: string, attribute: string) {
        const self = this;
        await this.waitWhileLoading();
        if (typeof (this.filterByColumnTextTimeout[attribute]) != "undefined") {
            clearTimeout(this.filterByColumnTextTimeout[attribute]);
        }
        self.displayedColumnsFiltersObjects[attribute].value = text;
        this.filterByColumnTextTimeout[attribute] = setTimeout(
            function () {
                self.buildTable();
                delete self.filterByColumnTextTimeout[attribute];
            },
            this.textFilterTimeout
        );
    }
    async waitWhileLoading() {
        while (this.tableIsLoading) {
            await new Promise((resolve, reject) => {
                setTimeout(() => resolve(), 100);
            });
        }
    }
    applyFilter(idFilter) {
        this.paginator.pageIndex = 0;
        for (let key in this.displayedPageFiltersObjects) {
            this.displayedPageFiltersObjects[key].dirty = false;
        }
        this.buildTable();
    }
    removeFilter(idFilter) {
        this.displayedPageFilters = this.removeElementInArray(this.displayedPageFilters, idFilter);
        this.buildTable();
    }
    getStyleFilterApply(idFilter) {
        if (this.displayedPageFiltersObjects[idFilter].dirty) {
            return { 'color': 'green' };
        } else {
            return { 'color': '#dee2e6' };
        }
    }
    filterByPageFilter(selectAll: boolean, deselectAll: boolean, attribute: string) {
        //this.paginator.pageIndex = 0;
        
        this.displayedPageFiltersObjects[attribute].dirty = true;
        //if(selectAll) this.displayedPageFiltersObjects[attribute].selectedItems = this.displayedPageFiltersObjects[attribute].list;
        //else if(deselectAll) this.displayedPageFiltersObjects[attribute].selectedItems = [];
        if (selectAll) this.displayedPageFiltersObjects[attribute].msModel.forceSelectAll();
        else if (deselectAll) this.displayedPageFiltersObjects[attribute].msModel.forceDeselectAll();
        //this.buildTable();
    }
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    // Export
    public getColumnTranslation(translation) {
        if (this.traslations) {
            return this.traslations[translation];
        }
        else {            
            return "";
        }
    }
    public setColumnTranslations(translations) {
        this.traslations = translations;
    }
    public exportTableAsExcel() {
        var prefix = this.parentId + '.' + this.id + '.';
        this._exportService.exportAsGenericExcelFile(this.dataSource.data, this.displayedColumns, prefix, this.multipleHeaderObjectList, this.traslations);
    }
    // Drag & drop
    // Types are:
    // 'a' : available element
    // 'pf' : page filter
    // 't' : cabecera de tabla
    // 'tc' : table column
    // 'tr' : table row
    // 'del' : papelera
    public dragEnd(type, id) {
    }
    public dragStart(type, id) {
        this.draggedElement = { id: id, type: type };
    }
    drop_pf(targetId, pos) {
        this.pagefilterDraggedOn = null;
        if (this.attributes.includes(this.draggedElement.id)) {
            var index = this.displayedPageFilters.indexOf(targetId) + pos;
            this.displayedPageFilters = this.insertElementInArray(this.displayedPageFilters, this.draggedElement.id, (index == -1) ? 0 : index);
            this.buildTable();
        }
    }
    drop_tc(targetId) {
        this.tableColumnDraggedOn = null;
        if (this.attributes.includes(this.draggedElement.id)) {
            var index = this.displayedAttributes.indexOf(targetId);
            this.displayedAttributes = this.insertElementInArray(this.displayedAttributes, this.draggedElement.id, index);
            this.buildTable();
        }
        else if (this.metrics.includes(this.draggedElement.id)) {
            var index = this.displayedMetrics.indexOf(targetId);
            this.displayedMetrics = this.insertElementInArray(this.displayedMetrics, this.draggedElement.id, index);
            this.buildTable();
        }
    }
    drop_bin() {
        this.binDraggedOn = false;
        if (this.draggedElement.type == 'pf') {
            this.displayedPageFilters = this.removeElementInArray(this.displayedPageFilters, this.draggedElement.id);
            this.buildTable();
        } else if (this.draggedElement.type == 'tc') {
            if (this.displayedAttributes.includes(this.draggedElement.id) && (this.displayedColumns.length > 1))
                this.displayedAttributes = this.removeElementInArray(this.displayedAttributes, this.draggedElement.id);
            else if (this.displayedMetrics.includes(this.draggedElement.id) && (this.displayedColumns.length > 1))
                this.displayedMetrics = this.removeElementInArray(this.displayedMetrics, this.draggedElement.id);
            this.buildTable();
        }
    }
    private insertElementInArray(array: string[], elementId: string, index: number): string[] {
        var result = [];
        for (var i = 0; i < array.length; i++) {
            if (i == index) result.push(elementId);
            if (array[i] != elementId) result.push(array[i]);
        }
        if (index == array.length) result.push(elementId);
        return result;
    }
    private removeElementInArray(array: string[], elementId: string) {
        return this.insertElementInArray(array, elementId, -1);
    }
    // Remarcar cambios
    private showHighlightRowChanges(data: any[]) {
        var columns = this.highlightRowChangesColumns;
        for (var i = 1; i < data.length; i++) {
            for (var attr of columns) {
                if (data[i][attr] != data[i - 1][attr]) data[i - 1]['ROWCHANGE'] = true;
            }
        };
    }
    private showHighlightColumnChanges(data: any[]) {
        var highlightColumnChangesColumns = this.highlightColumnChangesColumns;
        data.forEach(function (element) {
            for (var column of highlightColumnChangesColumns) {
                element[column + '_COLUMNCHANGE'] = true;
            }
        });
    }
    // Selección
    selectRow(selected, selectedRow) {
        if (selected) {
            if (!this.selection.find(row => this.rowEquals(row, selectedRow))) {
                this.selection.push(selectedRow);
            }
        } else {
            const index = this.selection.findIndex(row => this.rowEquals(row, selectedRow));
            if (index != -1) {
                this.selection.splice(index, 1);
            }
        }
    }
    isSelectedRow(row) {
        const index = this.selection.findIndex(r => this.rowEquals(r, row));
        return index != -1;
    }
    selectAll(selected) {
        this.selection.splice(0, this.selection.length);
        if (selected) {
            for (let row of this.dataSource.data) {
                this.selection.push(row);
            }
        }
    }
    isSelectedAll() {
        return this.selection.length == this.dataSource.data.length;
    }

    columnHasFilter(column) {
        var has = false;
        for (var filter of this.displayedColumnsFilters) {
            if (filter == column + "-filter") {
                has = true;
            }
        }
        return has;
    }
    isHideSpecically(column) {
        if (this.hideColumns.find(item => item === column)) {
            return true;


        }
        if (column.includes("OBS@")) {
            return true;
        }
        return false;
    }
    isStickyColumn(column) {
        if (this.stickyColumns[column]) {
            return true;
        }
        return false;
    }
    getEmptyContentColumnSuperHeader(sh) {
        return sh.name === "NO-SH";
    }
    getEmptySuperHeaderColumn(column, columnIndex) {
        var headers = this.getSuperHeaders(column, columnIndex);
        for (var header of headers) {
            if (header.name != "NO-SH") {
                return false;
            }
        }
        return true;
    }
    getSuperHeaders(column, columnIndex) {
        var headers = [];
        var index = 0;
        for (var superHeaderObject of this.multipleHeaderObjectList) {
            index++;
            var j = 0;
            for (var dc of superHeaderObject.displayedColumns) {
                if (dc === column + "-sh_" + index) {
                    var name = "NO-SH";
                    if (index == 1 && this.smallMetricsInHeader && superHeaderObject[this.displayedColumns[columnIndex]] != superHeaderObject[this.displayedColumns[columnIndex - 1]]) {
                        if (dc.includes('@')) {
                            name = dc.split('@')[0];
                        }
                        else {
                            name = dc;
                        }
                    }
                    if (index != 1 || !this.smallMetricsInHeader) {
                        name = superHeaderObject[column];
                    }
                    if (name) {
                        headers.push({
                            position: index,
                            superHeaderObject: superHeaderObject,
                            index: index,
                            name: name
                        });
                    }
                }
                j++;
            }
        }
        return headers;
    }
    rowEquals(row1, row2) {
        for (const k in row1) {
            if (row1[k] !== row2[k]) {
                return false;
            }
        }
        for (const k in row2) {
            if (row1[k] != row2[k]) {
                return false;
            }
        }
        return true;
    }
    getSelection() {
        return this.selection.slice();
    }
    setActiveRow(pkValues: {}) {
        this.pkAttributes;
        // TODO Recorrer todas las filas. activando o no según la clave primaria
        for (let row of this.dataSource.data) {
            if (this.rowEqualsPK(row, pkValues)) {
                row.__ACTIVE__ = true;
            } else {
                delete row.__ACTIVE__;
            }
        }
    }
    private rowEqualsPK(row, pk) {
        for (let key of this.pkAttributes) {
            if (row[key] !== pk[key]) {
                return false;
            }
        }
        return true;
    }
}
export class GenericTablePageFilterModel {
    public attribute: string;
    public msModel: MultiselectModel;
    public msModelMV: MultiselectModelMV;
    public draggedOn: boolean;
    public dirty: boolean = false;
    // Filtros externos
    public keyList: string[];
    public selectedItem: string;
    constructor(
        attribute: string,
    ) {
        this.attribute = attribute;
        this.draggedOn = false;
        this.msModel = new MultiselectModel('cod', 'desc', true);
        this.msModelMV = new MultiselectModelMV('cod', 'desc', true);
        this.keyList = [];
        this.selectedItem = '';
    }
    public load(data: any[], isCheckbox: true) {
        var elements = ListUtils.getMasterComboFromObjectList(data, this.attribute).multiselectList;
        if (isCheckbox) {
            for (let element of elements) {
                if (element.cod === 0 || element.cod === "0") {
                    element.desc = "No";
                } else if (element.cod === 1 || element.cod === "1") {
                    element.desc = "Sí";
                }
            }
        }
        this.msModel.setElements(elements);
        // Para filtros externos
        this.keyList = ListUtils.sortStringList(this.keyList, 'asc');
        if (this.selectedItem && !this.keyList.includes(this.selectedItem)) this.selectedItem = null;
        if (!this.selectedItem && (this.keyList.length > 0)) this.selectedItem = this.keyList[0];
    }
}
export class GenericTableColumnFilterModel {
    public attribute: string;
    public value: string;
    constructor(
        attribute: string,
    ) {
        this.attribute = attribute;
        this.value = '';
    }
}
export class TransposeFunctionResult {
    public data: any[];
    public displayedMetrics: string[];
    public multipleHeaderObjectList: any[];
    public hardSeparatorMetrics: string[] = null;
    constructor(data, displayedMetrics, multipleHeaderObjectList) {
        this.data = data;
        this.displayedMetrics = displayedMetrics;
        this.multipleHeaderObjectList = multipleHeaderObjectList;
    }
}
export class GenericTableUtility {
    id: string;
    tooltip: Function;
    faIcon: Function;
    innerHtml: string;
    innerHtmlFunction: Function;
    onContextMenu: Function;
    constructor(id: string, tooltip: string | Function, faIcon: string | Function, innerHtml: string, onContextMenu: Function = null) {
        this.id = id;
        this.tooltip = typeof (tooltip) == "function" ? tooltip : (() => tooltip);
        this.faIcon = typeof (faIcon) == "function" ? faIcon : (() => faIcon);
        this.innerHtml = innerHtml;
        this.onContextMenu = onContextMenu;
        this.innerHtmlFunction = function () { return null };
    }
}
