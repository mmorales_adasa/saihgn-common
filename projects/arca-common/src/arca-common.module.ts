import { CommonModule, } from '@angular/common';
import { InjectionToken } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LoginFormComponent } from './components/loginForm/loginForm.component';
import { NavigationBar } from './components/navigationBar/navigationBar.component';
import { GenericAbeComponent, GenericAbeDeleteDialog } from './components/genericAbe/generic.abe.component';

import { GenericTableCommonService } from './services/generic.table.common.service';
import { WelcomePanel } from './components/welcomePanel/welcomePanel.component';
import { GenericTableComponent } from './components/genericTable/generic.table.component';
import { BasicTableComponent, BasicTableDeleteDialog } from './components/basicTable/basic.table.component';

import { ImpresosSubtableCmponent } from './components/genericTable/custom/impresos.subtable.component';
import { GraficosControlGraficaComponent } from './components/subcomponentGrafica/graficos.control.grafica.component';
import { GenericFilterComponent } from './components/genericFilter/generic.filter.component';
import { GenericFilterElement } from './components/genericFilter/element/generic.filter.element';
import { BasicCrudComponent  } from './components/basicCrud/basic.crud.component';
import { BasicFilterComponent } from './components/basicFilter/basic.filter.component';
import { BasicFilterElement } from './components/basicFilter/element/basic.filter.element';
import { TreeChecklist } from './components/selectableTree/tree-checklist';


import { AuditoriaService } from './services/auditoria.service';
import { ArcaServiceOptions } from './services/arca.options.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';

import { MomentModule } from 'ngx-moment';
import { ChartsModule } from 'ng2-charts';
import {
  MatInputModule, MatDatepickerModule, MatNativeDateModule,
  MatTableModule, MatSortModule, MatPaginatorModule,
  MatSnackBarModule, MatDialogModule, MatFormFieldModule, MatSelectModule,
  MatTooltipModule,
  MatCheckboxModule, MatButtonModule,
  MatButtonToggleModule, MatTabsModule,
  MatMenuModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatCardModule,
  MatChipsModule,
  MatExpansionModule,
  MatGridListModule,
  MatListModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatStepperModule,
  MatToolbarModule,
} from '@angular/material';


import { A11yModule } from '@angular/cdk/a11y';
import { BidiModule } from '@angular/cdk/bidi';
import { ObserversModule } from '@angular/cdk/observers';
import { OverlayModule } from '@angular/cdk/overlay';
import { PlatformModule } from '@angular/cdk/platform';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';

import { DatePipe } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BasicFilter } from './components/basicFilter/basic.filter.model';
export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};
// @dynamic
@NgModule({
  imports: [
    CommonModule,
    MomentModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    ScrollDispatchModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    AngularMultiSelectModule, NgMultiSelectDropDownModule,
    FormsModule, ReactiveFormsModule, MatTooltipModule, MatCheckboxModule, MatButtonModule, MatButtonToggleModule, MatTabsModule,
    MatTreeModule,
    MatMenuModule, MatIconModule,
    MatInputModule, MatDatepickerModule, MatNativeDateModule,
    MatSortModule, MatTableModule, MatPaginatorModule,
    MatSnackBarModule, MatDialogModule, ChartsModule, NgbModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatToolbarModule,
  ],
  declarations: [HeaderComponent, SidebarComponent, LoginFormComponent, NavigationBar, WelcomePanel, GenericAbeComponent, GenericAbeDeleteDialog, GenericTableComponent, 
    GenericFilterComponent, GenericFilterElement,
    BasicFilterComponent, BasicFilterElement, TreeChecklist,BasicCrudComponent,
    ImpresosSubtableCmponent, GraficosControlGraficaComponent, BasicTableComponent, BasicTableDeleteDialog],
  entryComponents: [GenericAbeDeleteDialog,  BasicTableDeleteDialog ],
  providers: [
    DatePipe, AuditoriaService, GenericTableCommonService
  ],
  exports: [HeaderComponent, SidebarComponent, NavigationBar, WelcomePanel, LoginFormComponent, GenericAbeComponent, GenericAbeDeleteDialog, GenericTableComponent,
    GenericFilterComponent, GenericFilterElement, BasicCrudComponent,
    BasicFilterComponent, BasicFilterElement, TreeChecklist,
    ImpresosSubtableCmponent, GraficosControlGraficaComponent, BasicTableComponent]
})
export class ArcaCommonModule {
  public static forRoot(options?: ArcaServiceOptions): ModuleWithProviders {
    return {
      ngModule: ArcaCommonModule, providers: [
        {
          provide: FOR_ROOT_OPTIONS_TOKEN,
          useValue: options
        },
        {
          provide: ArcaServiceOptions,
          useFactory: provideMyServiceOptions,
          deps: [FOR_ROOT_OPTIONS_TOKEN]
        }
      ]
    };
  }
}
export interface ModuleOptions {
  auditoriaEndPoint?: string;
  laboratorioEndPoint?: string;
  auditaActivated?: boolean;
}
export var FOR_ROOT_OPTIONS_TOKEN = new InjectionToken<ModuleOptions>("forRoot() MyService configuration.");
export function provideMyServiceOptions(options?: ModuleOptions): ArcaServiceOptions {
  var myServiceOptions = new ArcaServiceOptions();
  if (options) {
    if (typeof (options.auditoriaEndPoint) === "string") {
      myServiceOptions.auditoriaEndPoint = options.auditoriaEndPoint;
    }
    if (typeof (options.laboratorioEndPoint) === "string") {
      myServiceOptions.laboratorioEndPoint = options.laboratorioEndPoint;
    }
    if (typeof (options.auditaActivated) === "boolean") {
      myServiceOptions.auditaActivated = options.auditaActivated;
    }

  }
  return (myServiceOptions);
}